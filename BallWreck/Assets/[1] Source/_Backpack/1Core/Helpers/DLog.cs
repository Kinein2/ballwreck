﻿using UnityEngine;


public class DLog
	{


		
		public enum Level
		{
			None,
			Critical,
			Normal,
			Verbose,
			UltraVerbose
		}


		public static Level currentLevel = Level.Normal;
		public static UnityEngine.Object currentContext;
		public static string[] ignoreFilter;

		public static void Log(System.Object o, Level level = Level.Normal, UnityEngine.Object context = null, string filter = "")
		{
			_Log(o, level, context, filter, true);
		}

		public static void Log(System.Object o, UnityEngine.Object context)
		{
			_Log(o, Level.Normal, context, "", true);
		}

		public static void Log(System.Object o, Level level, UnityEngine.Object context)
		{
			_Log(o, level, context, "", true);
		}
		
		public static void Log(System.Object o, Level level, string filter)
		{
			_Log(o, level, null, filter, true);
		}



		

		public static void Error(System.Object o, Level level = Level.Normal, UnityEngine.Object context = null, string filter = "")
		{
			_Log(o, level, context, filter, false);
		}

		public static void Error(System.Object o, UnityEngine.Object context)
		{
			_Log(o, Level.Normal, context, "", false);
		}

		public static void Error(System.Object o, Level level, UnityEngine.Object context)
		{
			_Log(o, level, context, "", false);
		}

		public static void Error(System.Object o, Level level, string filter)
		{
			_Log(o, level, null, filter, false);
		}





		static void _Log(System.Object o, Level level, UnityEngine.Object context, string additionalFilter, bool log)
		{
			if ((int)level > (int)currentLevel)
				return;

			if (ignoreFilter != null)
			if (ignoreFilter.Length > 0)
			{
				for (int i = 0; i < ignoreFilter.Length; i++)
				{
					if (additionalFilter == ignoreFilter[i])
						return;

					if (level.ToString() == ignoreFilter[i])
						return;
				}
			}

			string levelName = level.ToString();
			string filterName = "";
			if (additionalFilter.Length > 0)
				filterName = "#" + additionalFilter + "# ";


			UnityEngine.Object obj = context;

			if (obj == null)
				if (o is UnityEngine.Object)
					obj = o as UnityEngine.Object;
				else
					obj = currentContext;

			string text;
			text = o.ToString();

			if (Application.isEditor == false && obj != null)
				text += " (" + obj.ToString() + ")";

			if (log || level != Level.Critical) // критические ошибки мы не фильтруем, это просто ошибка
				text = filterName + text + "\nCPAPI:{\"cmd\":\"Filter\" \"name\":\"" + levelName + "\"}";
			


			


			if (!log)
				log = level != Level.Critical; // ошибку выдает только в особых критичных случаях, иначе просто сообщение. Ведь ошибки ставят паузы

			if (obj == null)
			{
				if (log)
					Debug.Log(text);
				else Debug.LogError(text);
			}
			else
			{
				if (log) Debug.Log(text, obj);
				else Debug.LogError(text, obj);
			}
		}

		public static void Watch(System.Object text, string watchName, UnityEngine.Object context = null)
		{
			Debug.Log(text + "\nCPAPI:{\"cmd\":\"Watch\" \"name\":\"" + watchName + "\"}", context);
		}
	}

namespace DLogWrapper
{

	


}