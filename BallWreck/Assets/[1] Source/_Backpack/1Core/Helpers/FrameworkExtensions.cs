/*===============================================================
Product:    Battlecruiser
Developer:  Dimitry Pixeye - pixeye@hbrew.store
Company:    Homebrew - http://hbrew.store
Date:       12/09/2017 23:42
================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static partial class FrameworkExtensions
{
	private static System.Random _r = new System.Random();


	#region OLD

	public static float ClampAngle(this float angle, float min, float max)
	{
		if (min < 0 && max > 0 && (angle > max || angle < min))
		{
			angle -= 360;
			if (angle > max || angle < min)
			{
				return Mathf.Abs(Mathf.DeltaAngle(angle, min)) < Mathf.Abs(Mathf.DeltaAngle(angle, max)) ? min : max;
			}
		}
		else if (min > 0 && (angle > max || angle < min))
		{
			angle += 360;
			if (angle > max || angle < min)
			{
				return Mathf.Abs(Mathf.DeltaAngle(angle, min)) < Mathf.Abs(Mathf.DeltaAngle(angle, max)) ? min : max;
			}
		}

		if (angle < min) return min;
		else if (angle > max)
			return max;
		else
			return angle;
	}

	public static int ReturnNearestIndex(this Vector3[] nodes, Vector3 destination)
	{
		var nearestDistance = Mathf.Infinity;
		var index = 0;
		var length = nodes.Length;
		for (var i = 0; i < length; i++)
		{
			var distanceToNode = (destination + nodes[i]).sqrMagnitude;
			if (!(nearestDistance > distanceToNode)) continue;
			nearestDistance = distanceToNode;
			index = i;
		}

		return index;
	}
	
	public static string GetPath(this Transform current) {
		if (current.parent == null)
			return "/" + current.name;
		return current.parent.GetPath() + "/" + current.name;
	}

	public static T ReturnRandom<T>(this List<T> list, T[] itemsToExclude)
	{
		var val = list[UnityEngine.Random.Range(0, list.Count)];

		while (itemsToExclude.Contains(val))
			val = list[UnityEngine.Random.Range(0, list.Count)];

		return val;
	}

	public static T ReturnRandom<T>(this List<T> list)
	{
		var val = list[UnityEngine.Random.Range(0, list.Count)];
		return val;
	}

	public static T GetRandom<T>(this List<T> vals) where T : IRandom
	{
		var total = 0f;

		var probs = new float[vals.Count];


		for (var i = 0; i < probs.Length; i++)
		{
			probs[i] = vals[i].ReturnChance;
			total += probs[i];
		}


		var randomPoint = (float) _r.NextDouble() * total;


		for (var i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i])
				return vals[i];
			randomPoint -= probs[i];
		}

		return vals[0];
	}

	public static T GetRandom<T>(this T[] vals) where T : IRandom
	{
		if (vals == null || vals.Length == 0) return default(T);


		float total = 0f;

		float[] probs = new float[vals.Length];


		for (int i = 0; i < probs.Length; i++)
		{
			probs[i] = vals[i].ReturnChance;
			total += probs[i];
		}


		float randomPoint = (float) _r.NextDouble() * total;


		for (int i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i])
				return vals[i];
			
			randomPoint -= probs[i];
		}

		return vals[0];
	}

	public static float GetRandomArg(this float[] vals)
	{
		if (vals == null || vals.Length == 0) return -1;


		var total = 0f;

		var probs = new float[vals.Length];


		for (var i = 0; i < probs.Length; i++)
		{
			probs[i] = vals[i];
			total += probs[i];
		}


		var randomPoint = (float) _r.NextDouble() * total;


		for (var i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i])
				return probs[i];
			randomPoint -= probs[i];
		}

		return 0;
	}


	public static int GetRandom(this float[] vals)
	{
		if (vals == null || vals.Length == 0) return -1;


		var total = 0f;

		var probs = new float[vals.Length];


		for (var i = 0; i < probs.Length; i++)
		{
			probs[i] = vals[i];
			total += probs[i];
		}


		var randomPoint = (float) _r.NextDouble() * total;


		for (var i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i])
				return i;
			randomPoint -= probs[i];
		}

		return 0;
	}

	public static T Random<T>(this T[] vals)
	{
		return vals[UnityEngine.Random.Range(0, vals.Length)];
	}


	public static T GetRandom<T>(this T[] vals, out int index) where T : IRandom
	{
		index = -1;

		if (vals == null || vals.Length == 0) return default(T); 


		float total = 0f;

		float[] probs = new float[vals.Length];


		for (int i = 0; i < probs.Length; i++)
		{
			probs[i] = vals[i].ReturnChance;
			total += probs[i];
		}


		var randomPoint = (float) _r.NextDouble() * total;


		for (var i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i])
			{
				index = i;
				return vals[i];
			}

			randomPoint -= probs[i];
		}

		return vals[0];
	}

	public static T GetRandom<T>(this T[] vals, float[] probs) where T : IRandom
	{
		if (vals == null || vals.Length == 0) return default(T);


		var total = probs.Sum();


		var randomPoint = (float) _r.NextDouble() * total;


		for (var i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i])
				return vals[i];
			randomPoint -= probs[i];
		}

		return vals[0];
	}


	public static float GetRandom(this Vector2 v)
	{
		return UnityEngine.Random.Range(v.x, v.y);
	}

	public static float GetRandomChoice(this Vector2 v)
	{
		return UnityEngine.Random.value >= 0.5f ? v.x : v.y;
	}


	public static Vector3 MixRandom(this Vector3 me, Vector3 v1, Vector3 v2)
	{
		var v = new Vector3(
			UnityEngine.Random.Range(v1.x, v2.x),
			UnityEngine.Random.Range(v1.y, v2.y),
			UnityEngine.Random.Range(v1.z, v2.z)
		);
		return v;
	}

	


	public static T[] Increase<T>(this T[] values, int increment)
	{
		T[] array = new T[values.Length + increment];
		values.CopyTo(array, 0);
		return array;
	}
	
	public static T[] Decrease<T>(this T[] values, int decrement) // @Volatile: если = не действует то хана
	{
		T[] array = new T[values.Length - decrement];
		for (int i = 0; i < values.Length - decrement; i++)
			array[i] = values[i];
		return array;
	}

	public static T[] AddOrPopulate<T>(this T[] array, T val, int increaseN)
	{
		var length = array.Length;

		for (var i = 0; i < length; i++)
		{
			if (array[i] != null) continue;
			array[i] = val;
			return array;
		}

		array = array.Increase(increaseN);
		array[length] = val;
		return array;
	}
	
	public static T[] AddOrPopulate<T>(this T[] array, T val)
	{
		return AddOrPopulate<T>(array, val, 10);
	}

	public static T[] Add<T>(this T[] array, T val)
	{
		var length = array.Length;

		array = array.Increase(1);
		array[length] = val;
		return array;
	}
	
	public static T[] AddAt<T>(this T[] array, T val, int n)
	{
		T[] newArr = new T[array.Length + 1];
		
		int k = 0;
		for (int i = 0; i < array.Length; i++)
		{
			newArr[k] = array[i];
			k++;
			

			if (i == n)
			{
				newArr[k] = val;
				k++;
			}
		}

		return newArr;
	}

	public static T[] Remove<T>(this T[] source, object obj)
	{
		T[] dest = new T[source.Length - 1];
		var index = Array.FindIndex(source, o => o.Equals(obj));

		if (index > 0)
			Array.Copy(source, 0, dest, 0, index);

		if (index < source.Length - 1)
			Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

		return dest;
	}

	public static T[] RemoveAt<T>(this T[] source, int index)
	{
		T[] dest = new T[source.Length - 1];
		if (index > 0)
			Array.Copy(source, 0, dest, 0, index);

		if (index < source.Length - 1)
			Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

		return dest;
	}

	public static int IndexOf<T>(this T[] source, T obj)
	{
		for (int i = 0; i < source.Length; i++)
			if (source[i] != null)
			if (source[i].Equals(obj))
				return i;

		return -1;
	}

	public static T FirstOrDefault<T>(this T[] source)
	{
		if (source == null)
			return default(T);
		if (source.Length == 0)
			return default(T);

		return source[0];
	}

	#endregion

	public static float Fraction(this float f)
	{
		return f - (int)f;
	}

	public static int Whole(this float f)
	{
		return (int)f;
	}

	public static bool InBetween(this float f, float a, float b)
	{
		return (f >= a && f <= b) || (f >= b && f <= a);
	}

	public static float To180Deg(this float ang) // приводим к от -180 до 180
	{
		
		if (ang >= 180)
		{
			ang = ang - 360;
			return ang.To180Deg();
		}


		if (ang <= -180)
		{
			ang = ang + 360;
			return ang.To180Deg();
		}

		return ang;
	}

	
	public static float To360Deg(this float ang) // приводим к от 0 до 360
	{
		if (ang < 0)
		{
			ang = 360 - ang;
			return ang.To360Deg();
		}
		
		if (ang > 360)
		{
			ang = ang - 360;
			return ang.To360Deg();
		}

		return ang;
	}

    //drains.Find((x, best) => { return x.width > best.width; });
    public static int Find<T>(this T[] arr, Func<T, T, bool> funcX_Best)
    {
        int iBest = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            if (funcX_Best(arr[i], arr[iBest]))
            {
                iBest = i;
            }
        }

        return iBest;
    }

    public delegate void RefAction<T>(ref T arg1);

    //drains = drains.Run((ref DamageOverTime x) => { x.time = 69; });
    //drains = drains.Run(Run);
    public static T[] Run<T>(this T[] arr, RefAction<T> func)
    {
        for (int i = 0; i < arr.Length; i++)
            func(ref arr[i]);

        return arr;
    }

    public static void Run<T>(this T[] arr, Action<int> func)
    {
        for (int i = 0; i < arr.Length; i++)
            func(i);
    }
	
	
	public static T Instantiate<T>(this T unityObject, System.Action<T> beforeAwake = null) where T : UnityEngine.Object
	{
		//Find prefab gameObject
		var gameObject = unityObject as GameObject;
		var component = unityObject as Component;
 
		if (gameObject == null && component != null)
			gameObject = component.gameObject;
 
		//Save current prefab active state
		var isActive = false;
		if (gameObject != null)
		{
			isActive = gameObject.activeSelf;
			//Deactivate
			gameObject.SetActive(false);
		}
 
		//Instantiate
		var obj = UnityEngine.Object.Instantiate(unityObject) as T;
		if (obj == null)
			throw new Exception("Failed to instantiate Object " + unityObject);
 
		//This funciton will be executed before awake of any script inside
		if (beforeAwake != null)
			beforeAwake(obj);
 
		//Revert prefab active state
		if (gameObject != null)
			gameObject.SetActive(isActive);
 
		//Find instantiated GameObject
		gameObject = obj as GameObject;
		component = obj as Component;
 
		if (gameObject == null && component != null)
			gameObject = component.gameObject;
 
		//Set active state to prefab state
		if (gameObject != null)
			gameObject.SetActive(isActive);
 
		return obj;
	}

    


    #region TRANSFORMS

	public static Vector3 Set(this Vector3 obj, Vector2 v)
	{
		return new Vector3(v.x, v.y, obj.z);
	}
	
	public static Vector3 Set(this Vector3 obj, Vector2z v)
	{
		return new Vector3(v.x, obj.y, v.z);
	}
	
	public static Vector3 Set(this Vector3 obj, Vector2 v, float z)
	{
		return new Vector3(v.x, v.y, z);
	}

    public static Vector2 SetX(this Vector2 obj, float xVal)
	{
		return new Vector2(xVal, obj.y);
	}

	public static Vector2 SetY(this Vector2 obj, float yVal)
	{
		return new Vector2(obj.x, yVal);
	}

	public static Vector3 SetZ(this Vector2 obj, float zVal = 0.0f)
	{
		return new Vector3(obj.x, obj.y, zVal);
	}
	
	public static Vector3 SetX(this Vector3 obj, float x)
	{
		return new Vector3(x, obj.y, obj.z);
	}
	
	public static Vector3 SetY(this Vector3 obj, float y)
	{
		return new Vector3(obj.x, y, obj.z);
	}

	public static Vector3 SetZ(this Vector3 obj, float z)
	{
		//obj = new Vector3(obj.x, obj.y, z); sometimes
		return new Vector3(obj.x, obj.y, z);
	}
	
	public static Vector3 IncX(this Vector3 obj, float x)
	{
		return new Vector3(obj.x + x, obj.y, obj.z);
	}
	
	public static Vector3 IncY(this Vector3 obj, float y)
	{
		return new Vector3(obj.x, obj.y + y, obj.z);
	}

	public static Vector3 IncZ(this Vector3 obj, float z)
	{
		return new Vector3(obj.x, obj.y, obj.z + z);
	}

	public static Vector2 IncX(this Vector2 obj, float x)
	{
		return new Vector2(obj.x + x, obj.y);
	}
	
	public static Vector2 IncY(this Vector2 obj, float y)
	{
		return new Vector2(obj.x, obj.y + y);
	}
	
	public static Vector2 SetLength(this Vector2 v, float L)
	{
		float l = v.magnitude;
		float Labs = Mathf.Abs(L);
		if (l > 10e-6)
			return v * (Labs / l);
		else return v;
	}
	
	public static Vector2 IncLength(this Vector2 v, float dL)
	{
		float l = v.magnitude;

		if (l > 10e-6)
			return v * ((l + dL) / l);
		else return v;

	}
	
	public static Vector3 SetLength(this Vector3 v, float L)
	{
		float l = v.magnitude;
		float Labs = Mathf.Abs(L);
		if (l > 10e-6)
			return v * (Labs / l);
		else return v;
	}
	
	public static Vector3 IncLength(this Vector3 v, float dL)
	{
		float l = v.magnitude;

		if (l > 10e-6)
			return v * ((l + dL) / l);
		else return v;
	}
	
	public static Vector3 MultX(this Vector3 v, float val)
	{
		v = new Vector3(val * v.x, v.y, v.z);
		return v;
	}

	public static Vector3 MultY(this Vector3 v, float val)
	{
		v = new Vector3(v.x, val * v.y, v.z);
		return v;
	}

	public static Vector3 MultZ(this Vector3 v, float val)
	{
		v = new Vector3(v.x, v.y, val * v.z);
		return v;
	}

	public static Vector2 ToVector2(this Vector2z v)
	{
		return new Vector2(v.x, v.z);
	}
	
	public static Vector2 ToXZ(this Vector3 v)
	{
		return new Vector2(v.x, v.z);
	}

	
	
	/*
	public static void SetPosX(this Transform obj, float x)
	{
		Vector3 v = obj.position;
		v.x = x;
		obj.position = v;
	}
	
	public static void SetPosY(this Transform obj, float y)
	{
		Vector3 v = obj.position;
		v.y = y;
		obj.position = v;
	}
	
	public static void SetPosZ(this Transform obj, float z)
	{
		Vector3 v = obj.position;
		v.z = z;
		obj.position = v;
	}
	*/
	



	public static Transform FindDeep(this Transform obj, string id)
	{
		if (obj.name == id)
		{
			return obj;
		}

		var count = obj.childCount;
		for (var i = 0; i < count; ++i)
		{
			var posObj = obj.GetChild(i).FindDeep(id);
			if (posObj != null)
			{
				return posObj;
			}
		}

		return null;
	}
	

	public static List<T> GetAll<T>(this Transform obj)
	{
		var results = new List<T>();
		obj.GetComponentsInChildren(results);
		return results;
	}

	public static T AddOrGetComponent<T>(this GameObject go) where T : Component
	{
		T t = go.GetComponent<T>();
		if (t == null)
			return go.AddComponent<T>();

		return t;
	}

	#endregion

	#region EASE

//		public static float CalculateEase(float time, float duration, Ease ease)
//		{
//			var overshootOrAmplitude = 1.70158f;
//			switch (ease)
//			{
//				case Ease.InSine:
//					return (float) (-Math.Cos(time / (double) duration * 1.57079637050629f) + 1.0);
//				case Ease.OutSine:
//					return (float) Math.Sin(time / (double) duration * 1.57079637050629);
//				case Ease.InBack:
//					return (float) ((time /= duration) * (double) time *
//					                ((overshootOrAmplitude + 1.0) * time - overshootOrAmplitude));
//				case Ease.OutBack:
//					return (float) ((time = (float) (time / (double) duration - 1.0)) * (double) time *
//					                ((overshootOrAmplitude + 1.0) * time + overshootOrAmplitude) + 1.0);
//				case Ease.InOutBack:
//					if ((time /= duration * 0.5f) < 1.0)
//						return (float) (0.5 * (time * (double) time *
//						                       (((overshootOrAmplitude *= 1.525f) + 1.0) * time - overshootOrAmplitude)));
//					return (float) (0.5 * ((time -= 2f) * (double) time *
//					                       (((overshootOrAmplitude *= 1.525f) + 1.0) * time + overshootOrAmplitude) + 2.0));
//				case Ease.InCirc:
//					return (float) -(Math.Sqrt(1.0 - (time /= duration) * (double) time) - 1.0);
//				case Ease.OutCirc:
//					return (float) Math.Sqrt(1.0 - (time = (float) (time / (double) duration - 1.0)) * (double) time);
//				default:
//					return time / duration;
//			}
//		}

	#endregion

	#region COLORS

	public static Color SetAlpha(this Color c, float alpha)
	{
		return new Color(c.r, c.g, c.b, alpha);
	}

	public static string ToHex(this Color c)
	{
		byte rb = (byte)(c.r * 255.0f);
		byte gb = (byte)(c.g * 255.0f);
		byte bb = (byte)(c.b * 255.0f);
		byte ab = (byte)(c.a * 255.0f);

		return ByteToHexString(rb) + ByteToHexString(gb) + ByteToHexString(bb) + ByteToHexString(ab);
	}

	
	public static string ByteToHexString(byte b)
	{
		
		string HexAlphabet = "0123456789ABCDEF";

		string result = "";
		result += HexAlphabet[(int)(b >> 4)];
		result += HexAlphabet[(int)(b & 0xF)];

		return result.ToString();
	}

	#endregion
}