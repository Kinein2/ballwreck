﻿using UnityEngine;

[System.Serializable]
public class SingletonHost : MonoBehaviour 
{
	[SerializeField]
	public MonoBehaviour[] singletons;

	

	public string[] ignoreFilter;
	
	void Awake()
	{
		DLog.ignoreFilter = ignoreFilter;
	}

	void Update()
	{
		DLog.ignoreFilter = ignoreFilter;
	}
	
	
	static SingletonHost _instance;
	
	public static SingletonHost Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType<SingletonHost>();
				if (_instance == null)
					DLog.Error("[Singleton] Something went really wrong "        +
					           " - there is no singletonHost, find it and activate it (or create one)", DLog.Level.Critical, "Backpack");
			}

			return _instance;
		}
	}
}
