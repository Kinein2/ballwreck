﻿using UnityEngine;

public struct Vector2z 
{
    
    public float x, z;

    public Vector2z(float x, float z)
    {
        this.x = x;
        this.z = z;
    }

    public float sqrMagnitude
    {
        get
        {
            return x*x + z*z;        
        }
    }
    
    public float magnitude 
    {
        get
        {
            return Mathf.Sqrt(x*x + z*z);    
        }
    }

    public void Normalize()
    {
        float l = magnitude;
        x /= l;
        z /= l;
    }

    public Vector2z normalized
    {
        get
        {
            Vector2z v = new Vector2z(this.x, this.z);
            v.Normalize();
            return v;
        }
    }

    public override string ToString()
    {
        return string.Format("({0:F1}, {1:F1})", (object) this.x, (object) this.z);
    }

    public string ToString(string format)
    {
        return string.Format("({0}, {1})", (object) this.x.ToString(format), (object) this.z.ToString(format));
    }

    public static Vector2z Lerp(Vector2z a, Vector2z b, float t)
    {
        return a*(1-t) + b*t;
    }

    public static float Dot(Vector2z a, Vector2z b)
    {
        return a.x * b.x + a.z * b.z;
    }

    static float Cross(Vector2z a, Vector2z b)
    {
        return a.x * b.z - a.z * b.x;
    }

    public static Vector2z operator +(Vector2z a, Vector2z b)
    {
        return new Vector2z(a.x + b.x, a.z + b.z);
    }

    public static Vector2z operator -(Vector2z a, Vector2z b)
    {
        return new Vector2z(a.x - b.x, a.z - b.z);
    }

    public static Vector2z operator *(Vector2z a, Vector2z b)
    {
        return new Vector2z(a.x * b.x, a.z * b.z);
    }

    public static Vector2z operator /(Vector2z a, Vector2z b)
    {
        return new Vector2z(a.x / b.x, a.z / b.z);
    }

    public static Vector2z operator -(Vector2z a)
    {
        return new Vector2z(-a.x, -a.z);
    }

    public static Vector2z operator *(Vector2z a, float d)
    {
        return new Vector2z(a.x * d, a.z * d);
    }

    public static Vector2z operator *(float d, Vector2z a)
    {
        return new Vector2z(a.x * d, a.z * d);
    }

    public static Vector2z operator /(Vector2z a, float d)
    {
        return new Vector2z(a.x / d, a.z / d);
    }

    public static implicit operator Vector2z(Vector2 v)
    {
        return new Vector2z(v.x, v.y);
    }
    
    public static implicit operator Vector2z(Vector3 v)
    {
        return new Vector2z(v.x, v.z);
    }

    public static implicit operator Vector3(Vector2z v)
    {
        return new Vector3(v.x, 0.0f,  v.z);
    }

        
        
    private static readonly Vector2z zeroVector = new Vector2z(0.0f, 0.0f);
    private static readonly Vector2z oneVector = new Vector2z(1f, 1f);
    private static readonly Vector2z upVector = new Vector2z(0.0f, 1f);
    private static readonly Vector2z downVector = new Vector2z(0.0f, -1f);
    private static readonly Vector2z leftVector = new Vector2z(-1f, 0.0f);
    private static readonly Vector2z rightVector = new Vector2z(1f, 0.0f);
    private static readonly Vector2z positiveInfinityVector = new Vector2z(float.PositiveInfinity, float.PositiveInfinity);
    private static readonly Vector2z negativeInfinityVector = new Vector2z(float.NegativeInfinity, float.NegativeInfinity);

    public static Vector2z zero
    {
        get
        {
            return Vector2z.zeroVector;
        }
    }

    public static Vector2z one
    {
        get
        {
            return Vector2z.oneVector;
        }
    }

    public static Vector2z up
    {
        get
        {
            return Vector2z.upVector;
        }
    }

    public static Vector2z down
    {
        get
        {
            return Vector2z.downVector;
        }
    }

    public static Vector2z left
    {
        get
        {
            return Vector2z.leftVector;
        }
    }

    public static Vector2z right
    {
        get
        {
            return Vector2z.rightVector;
        }
    }

    public static Vector2z positiveInfinity
    {
        get
        {
            return Vector2z.positiveInfinityVector;
        }
    }

    public static Vector2z negativeInfinity
    {
        get
        {
            return Vector2z.negativeInfinityVector;
        }
    }
}