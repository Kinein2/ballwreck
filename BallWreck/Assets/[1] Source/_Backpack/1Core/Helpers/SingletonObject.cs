﻿using UnityEngine;

public class SingletonMono<T> : MonoBehaviour where T : MonoBehaviour
{
    static T _instance;

    public static T Instance
    {
        get
        {
            if (!_instance)
            {
                var objs = FindObjectsOfType(typeof(T));
                
                if (objs.Length > 1)
                {
                    DLog.Error("[Singleton] Something went really wrong " +
                                   " - there should never be more than 1 singleton!" +
                                   " Reopening the scene might fix it.", DLog.Level.Critical, "Backpack");
                    return _instance;
                }

                if (!_instance)
                {
                    if (objs.Length > 0)
                    {
                        _instance = objs[0] as T;
                    } else
                    {
                        var SH = SingletonHost.Instance;
                        _instance = SH.gameObject.AddComponent<T>();
                        SH.singletons = SH.singletons.Add(_instance);
                        DLog.Log(typeof(T).ToString() + " is added to host", DLog.Level.Verbose, SH, "Backpack");    
                    }
                    
                }
            }

            return _instance;
        }
    }

}