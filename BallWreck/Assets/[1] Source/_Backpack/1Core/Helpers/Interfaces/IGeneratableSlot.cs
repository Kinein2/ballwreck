﻿
public interface IGeneratableSlot
{
    
    
    bool UsesRef();
    
    IGeneratable GetRef();
    
    void SetRef(IGeneratable newRef);
    void SetData(IGeneratableSlot slot);
}


