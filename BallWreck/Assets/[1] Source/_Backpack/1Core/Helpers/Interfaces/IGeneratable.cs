﻿using UnityEngine;


public interface IGeneratable
{
	IGeneratable GetClone(string generatedName);
}


public class ScriptableObjectGeneratable : ScriptableObject, IGeneratable 
{
	public IGeneratable GetClone(string generatedName)
	{
		ScriptableObjectGeneratable sog = Instantiate(this);
		sog.name = this.name + " [" + generatedName + "]"; 	
		return sog;
	}	
}
