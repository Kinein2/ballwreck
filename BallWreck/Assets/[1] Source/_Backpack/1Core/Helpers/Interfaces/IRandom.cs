/*===============================================================
Product:    Battlecruiser
Developer:  Dimitry Pixeye - pixeye@hbrew.store
Company:    Homebrew - http://hbrew.store
Date:       30/06/2017 14:25
================================================================*/


public interface IRandom {


    float ReturnChance { get; }

}