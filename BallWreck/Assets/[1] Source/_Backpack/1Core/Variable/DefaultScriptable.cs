﻿using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class ScriptableWithDefault : ScriptableObjectGeneratable
{

	public abstract void SaveDefault();
	public abstract void SetToDefault(bool editorchange);

	public void _SetToDefault(bool editorchange)
	{
		SetToDefault(editorchange);
	}
	
	void OnEnable()
	{
		SceneManager.activeSceneChanged += SceneChanged;

#if UNITY_EDITOR
		EditorApplication.playModeStateChanged += OnChangePlayModeState;
#endif
		
		Enabling();
	}

	protected virtual void Enabling()
	{
		
	}



	public void SceneChanged(Scene scene1, Scene scene2)
	{
		if (scene1 != scene2)
			_SetToDefault(false);
	}

#if UNITY_EDITOR
	private void OnChangePlayModeState(PlayModeStateChange state)
	{ 
		
		DLog.Log(state, DLog.Level.UltraVerbose, "VARIABLE (BP)");
		if (state == PlayModeStateChange.EnteredPlayMode)
		{
			_SetToDefault(true);
		}

		if (state == PlayModeStateChange.ExitingPlayMode)
		{
			_SetToDefault(true);
		}
	}
#endif
}

