﻿using System;

[Serializable]
public abstract class VariableReference : IGeneratableSlot
{
	public UseMode useMode = UseMode.useConstant;

	public bool UseConstant { get { return useMode == UseMode.useConstant; } }

	public enum UseMode
	{
		useConstant, useVariable, monitorVariable
	}

	public VariableReference()
	{ }

	// IGeneratableSlot
	public bool UsesRef()
	{
		return useMode == UseMode.monitorVariable || useMode == UseMode.useVariable;
	}
	
	public abstract IGeneratable GetRef();
	public abstract void SetRef(IGeneratable newRef);
	public abstract void SetData(IGeneratableSlot slotData);
}

[Serializable]
public abstract class TemplateReference<T> : VariableReference
{

	public T constantValue;

	public delegate void changed();
	public changed onChange;
	
	
	
	// это надо задать вручную, т.к. нужна прямая ссылка на созданный вариабл
	//public TemplateVariable<T> variable;
	/*
	public TemplateReference()
	{

	}

	public TemplateReference(T value)
	{
		useMode = UseMode.useConstant;
		constantValue = value;
	}
	
	
	public override IGeneratable GetRef()
	{
		return variable;
	}
	
	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as ...;
	}
		
	
	*/
	
	
	public override void SetData(IGeneratableSlot slotData)
	{
		constantValue = (slotData as TemplateReference<T>).constantValue;
	}


	public TemplateReference(T r) 
	{
		constantValue = r;
	}

	public TemplateReference() 
	{

	}

	protected abstract T GetVariableValue();
	protected abstract void SetVariableValue(T val);
	
	public T Value
	{
		get { return UseConstant ? constantValue : GetVariableValue(); }
		set {
			if (UseConstant)
			{
				if (onChange != null)
					onChange();

				constantValue = value;
			}
			else {
				SetVariableValue(value);
			}
		}
	}
	
	
	public static implicit operator T(TemplateReference<T> val)
	{
		return val.Value;
	}

	
}
