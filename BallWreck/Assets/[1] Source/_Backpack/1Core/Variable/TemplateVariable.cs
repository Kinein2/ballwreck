﻿using UnityEngine;

public abstract class ScriptObj_Variable : ScriptableWithDefault
{
    
	public delegate void changed();
	public changed onChange;	
	
}

public abstract class TemplateVariable<T> : ScriptObj_Variable
{
	
	
	[SerializeField, HideInInspector]
	T _defaultValue;

	[SerializeField]
	protected T value;
	
	public bool noDefault;
	
	public T Value
	{
		get { return GetValue(); }
		set { SetValue(value); }
	}

    public virtual T GetValue()
    {
        return value;
    }

	public virtual void SetValue(T Value)
	{
		value = Value;

		if (onChange != null)
			onChange();
	}

	public void SetValue(TemplateVariable<T> Val)
	{
		Value = Val.Value;
	}

	public override void SaveDefault()
	{
		if (noDefault == false)
			_defaultValue = value;
	}
	public override void SetToDefault(bool editorchange)
	{
		if (noDefault == false)
			value = _defaultValue;
	}
	
}




	/*
	bool defaultSetted = false;

	[SerializeField]
	float _value;

	public bool resetToDefault = true;
	public float defaultValue = 00;

	
	public float value {
		get {
			CheckDefaultStuff();
			return _value;
		}

		set
		{
			CheckDefaultStuff();
			_value = value;
		}
	}

	void CheckDefaultStuff()
	{
		if (defaultSetted == false && resetToDefault)
			{
				if (Application.isPlaying)
				{
					defaultSetted = true;
					_value = defaultValue;
				}	
			}


		if (defaultSetted)
		{
			if (!Application.isPlaying)
				defaultSetted = false;
		}
	}
	*/