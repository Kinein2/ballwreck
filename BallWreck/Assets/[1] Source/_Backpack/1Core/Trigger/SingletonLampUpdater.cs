﻿using System.Collections.Generic;
using UnityEngine;

public class SingletonLampUpdater : SingletonMono<SingletonLampUpdater>
{
    [SerializeField]    
    List<Lamp> lamps = new List<Lamp>();
    
    
    void Start()
    {
        
    }

    void Update()
    {
        
#if UNITY_EDITOR
        
        float delta = Time.deltaTime;
        foreach (var l in lamps)
        {
            l.UpdateLights(delta);            
        }
        
#endif
    }


    void _AddLamp(Lamp l)
    {
        if (lamps.Contains(l) == false)
            lamps.Add(l);   
        
    }


    static public void AddLamp(Lamp l)
    {
#if UNITY_EDITOR
        Instance._AddLamp(l);
#endif
    }
    
    
}
