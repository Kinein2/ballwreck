﻿using UnityEngine;
using System;

#if UNITY_EDITOR
    using UnityEditor;
#endif


// Trigger - самый базовый и простой объект. На этот триггер подписываются (AddCallback), и при его активации (Raise) вызываются все подписанные функции.
public class Trigger : ScriptableObjectGeneratable
{
    public delegate void RaiseFunction();

    public MonoBehaviour[] subscribedMonos = null;
    public RaiseFunction[] subscribedCallbacks = null;
    

    public void AddCallback(MonoBehaviour mono, RaiseFunction raiseFunc)
    {
        if (subscribedMonos == null)
            subscribedMonos = new MonoBehaviour[0];
        if (subscribedCallbacks == null)
            subscribedCallbacks = new RaiseFunction[0];

        int index = subscribedCallbacks.IndexOf(raiseFunc);
        if (index != -1)
        {
            subscribedCallbacks = subscribedCallbacks.RemoveAt(index);
            subscribedMonos = subscribedMonos.RemoveAt(index);
        }

        subscribedMonos = subscribedMonos.Add(mono);
        subscribedCallbacks = subscribedCallbacks.Add(raiseFunc);
    }


    public void ClearAllCallbacks()
    {
        subscribedMonos = new MonoBehaviour[0];
        subscribedCallbacks = new RaiseFunction[0];
    }

    protected virtual void _Raise(MonoBehaviour mono)
    {
        if (subscribedMonos == null)
            subscribedMonos = new MonoBehaviour[0];
        if (subscribedCallbacks == null)
            subscribedCallbacks = new RaiseFunction[0];
        
        
        for (int i = 0; i < subscribedMonos.Length; i++)
        {
            if (subscribedMonos[i] == null)
                continue;
            
            if (subscribedMonos[i].enabled == false || subscribedMonos[i].gameObject.activeInHierarchy == false)
                continue;

            subscribedCallbacks[i]();
        }
    }

   
}



[Serializable]
public class Lamp
{
    public bool inSingleton = false;
    
    float colorHue, activatedPower;

    public void StartFlash()
    {
        activatedPower = 1;
        colorHue += 0.2f + UnityEngine.Random.value * 0.7f;
        if (colorHue > 1)
            colorHue -= 1;
        
        if (inSingleton == false)
            SingletonLampUpdater.AddLamp(this);
        
    }

    public void EndFlash()
    {
        activatedPower = 0;
    }

    public void UpdateLights(float deltaTime)
    {
        activatedPower -= deltaTime / 2.0f;
        if (activatedPower < 0)
            activatedPower = 0;
    }

    public bool isShining()
    {
        return activatedPower > 0.001f;
    }

    public Color GetColor()
    {
        if (isShining() == false)
            return neutral;
        Color max = Color.HSVToRGB(colorHue, 1, 1);
        return Color.Lerp(neutral, max, activatedPower);
    }

    public static Color neutral = new Color(120 / 255.0f, 120 / 255.0f, 120 / 255.0f, 108 / 255.0f);

}



// TriggerExtended - надстройка над Trigger. Добавляет к системе лампочку в редакторе и историю активаций 
public abstract class TriggerExtended : Trigger
{
    
#if UNITY_EDITOR
    Lamp triggerLamp = new Lamp(); // эта лампа будет показываться если мы выбрали в инспекторе триггер

    public void UpdateLights(float delta)
    {
        triggerLamp.UpdateLights(delta);
    }

    public Color GetLampColor()
    {
        return triggerLamp.GetColor();
    }

    [Multiline]
    public string developerDescription;

    [SerializeField]
    public bool recordHistory;
    
    
    string[] history = new string[0];

    public int GetHistoryCnt()
    {
        return history.Length;
    }
    
    public string GetHistoryString(int i)
    {
        return history[i];
    }

    public void ClearHistory()
    {
        history = new string[0];
    }

    protected void DrawTriggerField()
    {
        AuthorMono = EditorGUILayout.ObjectField(new GUIContent("Author"), AuthorMono, typeof(MonoBehaviour), true) as MonoBehaviour;   
    }

    public abstract void DrawTrigArgField();
    
#endif
    
    public MonoBehaviour AuthorMono { get; private set; }

    public GameObject AuthorGO
    {
        get
        {
            if (AuthorMono != null)
                return AuthorMono.gameObject;
            return null;
        }
    }

    protected bool CouldBeRaised(MonoBehaviour mono)
    {
        if (mono == null)
            return true;
        return !(mono.enabled == false || mono.gameObject.activeInHierarchy == false);
    }



    protected override void _Raise(MonoBehaviour mono)
    {
        AuthorMono = mono;
        
        #if UNITY_EDITOR            
            if (recordHistory)
                history = history.Add(StateToString());
        
            triggerLamp.StartFlash();
        #endif
        
        
        base._Raise(mono);
    }

    public virtual string StateToString()
    {
        if (AuthorGO != null)
            return Time.time + "   " + AuthorGO.name;
        return Time.time + "   " + "name=null";

    }
    
    protected void OnEnable()
    {
        
        subscribedMonos = null;
        subscribedCallbacks = null;
#if UNITY_EDITOR            
        triggerLamp.inSingleton = false;
        triggerLamp.EndFlash();
        history = new string[0];
#endif
    }

    protected void OnDisable()
    {
#if UNITY_EDITOR  
        triggerLamp.EndFlash();
#endif
    }
}







               






