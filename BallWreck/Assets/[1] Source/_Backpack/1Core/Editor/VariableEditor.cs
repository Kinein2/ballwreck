﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(ScriptObj_Variable), true)]
public class VariableEditor : Editor
{
	
	public override void OnInspectorGUI()
	{
		//Debug.Log("I'm working!");
		EditorGUI.BeginChangeCheck();
		serializedObject.Update();

		base.OnInspectorGUI();		
		if (EditorGUI.EndChangeCheck())
		{	
			serializedObject.ApplyModifiedProperties();
			
			if (Application.isPlaying == false)
			{
				var SOV = serializedObject.targetObject as ScriptObj_Variable;
				SOV.SaveDefault();
			}
		}
		
		
	}
	
}
