﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(TriggerExtended), true)]
public class TriggerExtendedEditor : Editor
{
    static Texture _knobTexture = null;

    MonoBehaviour authorMono;

    bool foldedSubscribers, foldedHistory = true;
    
    public static Texture KnobTexture
    {
        get
        {
            if (_knobTexture == null)
                _knobTexture = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Knob.psd").texture;
            return _knobTexture;
            
        }
    }
    
    
    public override void OnInspectorGUI()
    {
        var myTarget = target as TriggerExtended;

        EditorGUILayout.LabelField(new GUIContent("Name"), new GUIContent(target.name));
        EditorGUILayout.GetControlRect();



        foldedSubscribers = EditorGUILayout.Foldout(foldedSubscribers, "Recievers");
        if (foldedSubscribers)
        {
            GUI.enabled = false;
            EditorGUI.indentLevel += 1;
            if (myTarget.subscribedMonos != null)
            {
                for (int i = 0; i < myTarget.subscribedMonos.Length; i++)
                {
                    EditorGUILayout.ObjectField(new GUIContent("sub" + i.ToString()), myTarget.subscribedMonos[i], typeof(MonoBehaviour), true);
                    var funcName = myTarget.subscribedCallbacks[i].Method.Name;
                
                    EditorGUILayout.LabelField(new GUIContent("call"), new GUIContent(funcName));
                }
            }
            EditorGUI.indentLevel -= 1;   
        }
        
        
                 
        
        GUI.enabled = true;
        
        var position = EditorGUILayout.GetControlRect();

        position.height *= 2*0.9f;
        position.x += 0.5f*(position.xMax - position.xMin);
        position.x -= position.height*0.5f;
        
        DrawLamp(ref position, position.height, myTarget.GetLampColor());
        
        position = EditorGUILayout.GetControlRect();
        position = EditorGUILayout.GetControlRect();
        
        
        
        myTarget.DrawTrigArgField();
        
        GUI.enabled = true;
        
        EditorGUILayout.GetControlRect();
        EditorGUILayout.GetControlRect();
        myTarget.recordHistory = EditorGUILayout.Toggle("Record history", myTarget.recordHistory);
        
        foldedHistory = EditorGUILayout.Foldout(foldedHistory, "History");
        if (foldedHistory)
        {
            EditorGUI.indentLevel += 1;
            for (int i = 0; i < myTarget.GetHistoryCnt(); i++)
                EditorGUILayout.LabelField(new GUIContent(myTarget.GetHistoryString(i))); 
            EditorGUI.indentLevel -= 1;
        } 
        
    }

    public override bool RequiresConstantRepaint()
    {
        return true;
    }
    
    
    public static void DrawLamp(ref Rect position, float size, Color c)
    {
        Rect pos = position;
        //pos.height *= 0.8f;
        pos.height = size;
        pos.width = pos.height;
        pos.x -= 2; pos.y += 2;
        position.xMin = pos.xMax + 2;


        var T = KnobTexture;
            
        GUI.DrawTexture(pos, T, ScaleMode.ScaleToFit, true, 1, c, 0, 0);
    }
}