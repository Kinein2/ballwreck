﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerRaiser : MonoBehaviour 
{
	[SerializeField]
	SimpleTrigger[] triggers;

	public void Activate(int n)
	{
		triggers[n].Raise(this);
	}


}
