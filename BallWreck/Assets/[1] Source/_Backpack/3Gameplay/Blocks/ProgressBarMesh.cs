﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBarMesh : GeometryGenerator
{
    public float R0, R1;
    public FloatReference value;

    float valueOld;

    protected override void ConstructMesh()
    {
        //GenerateCircle(1.5f, 64);
        GenerateRingSector(R0, R1, -value.Value * 360, 0, 64);
        //GenerateRing(0.2f, 1.5f, 64);
    }

    private void LateUpdate()
    {
        if (Mathf.Abs(value.Value - valueOld) > 0.005f)
        {
            valueOld = value.Value;
            GenerateMesh();
        }
        
    }
}
