﻿using UnityEngine;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PrefabConnectionRecovery : MonoBehaviour
{
    [AssetsOnly]
    public GameObject[] possiblePrefabs;

    [Button]
    public void Reconnect()
    {
#if UNITY_EDITOR
        GameObject[] existed = new GameObject[transform.childCount];
        for (int i = 0; i < existed.Length; i++)
            existed[i] = transform.GetChild(i).gameObject;

        GameObject[] created = new GameObject[0];

        int nFailed = 0;
        GameObject failedDirectory = null;

        for (int i = 0; i < existed.Length; i++)
        {
            var t = existed[i].transform;

            int foundJ = -1, nameL = -1;
            for (int j = 0; j < possiblePrefabs.Length; j++)
            {
                if (possiblePrefabs[j].name.Length > nameL) // чтобы не было такого, что объект догМега попал к дог, до того как встретил догМега префаб
                if (t.name.IndexOf(possiblePrefabs[j].name) == 0)
                {
                    foundJ = j;
                    nameL = possiblePrefabs[j].name.Length;
                }
            }

            if (foundJ >= 0)
            {
                var go = (GameObject)PrefabUtility.InstantiatePrefab(possiblePrefabs[foundJ]); 
                go.transform.parent = transform;
                go.transform.name = t.name;
                go.transform.localPosition = t.localPosition;
                go.transform.localRotation = t.localRotation;
                go.transform.localScale = t.localScale;
                DestroyImmediate(t.gameObject);
            } else
            {
                if (nFailed == 0)
                {
                    failedDirectory = new GameObject("_NotFound");
                    failedDirectory.transform.parent = transform;
                }

                t.parent = failedDirectory.transform;
                nFailed++;
            }   
        }
#endif
    }
}
