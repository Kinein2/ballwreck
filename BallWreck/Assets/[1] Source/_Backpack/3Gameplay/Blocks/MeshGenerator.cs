using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class MeshGenerator : MonoBehaviour
{
    protected int maxVertices = 500;

    MeshFilter _meshFilter;

    Mesh _mesh;

    Vector3[] verts;
    int[] indices;

    int vertsN = 0, indicesN = 0;

    protected abstract void ConstructMesh();
    

    public void GenerateMesh() 
    {
        verts = new Vector3[maxVertices];
        indices = new int[maxVertices * 6];

        vertsN = 0;
        indicesN = 0;

        ConstructMesh();

        Vector3[] vs = new Vector3[vertsN];
        int[] inds = new int[indicesN];

        for (int i = 0; i < vs.Length; i++)
            vs[i] = verts[i];

        for (int i = 0; i < inds.Length; i++)
            inds[i] = indices[i];

        verts = vs;
        indices = inds;

        // обновляем меш
        _mesh.vertices = verts;
        _mesh.triangles = indices;
    }

    protected void AddTriangle(Vector3 a, Vector3 b, Vector3 c)
    {
        verts[vertsN] = a;
        verts[vertsN + 1] = b;
        verts[vertsN + 2] = c;

        indices[indicesN] = vertsN;
        indices[indicesN + 1] = vertsN + 1;
        indices[indicesN + 2] = vertsN + 2;

        vertsN += 3;
        indicesN += 3;
    }
    
    protected void AddTriangleFan(Vector3 vert)
    {
        verts[vertsN] = vert;

        if (vertsN >= 2)
        {
            indices[indicesN] = 0;
            indices[indicesN + 1] = vertsN - 1;
            indices[indicesN + 2] = vertsN;

            indicesN += 3;
        }
        

        vertsN += 1;
        
    }

    protected void AddQuad(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
    {
        verts[vertsN] = a;
        verts[vertsN + 1] = b;
        verts[vertsN + 2] = c;
        verts[vertsN + 3] = d;

        indices[indicesN] = vertsN;
        indices[indicesN + 1] = vertsN + 1;
        indices[indicesN + 2] = vertsN + 2;

        indices[indicesN + 3] = vertsN;
        indices[indicesN + 4] = vertsN + 2;
        indices[indicesN + 5] = vertsN + 3;

        vertsN += 4;
        indicesN += 6;
    }
    
    protected void AddQuadStrip(Vector3 a, Vector3 b)
    {
        verts[vertsN] = a;
        verts[vertsN + 1] = b;
    

        if (vertsN >= 2)
        {
            indices[indicesN] = vertsN - 2;
            indices[indicesN + 1] = vertsN - 1;
            indices[indicesN + 2] = vertsN;

            indices[indicesN + 3] = vertsN - 2;
            indices[indicesN + 4] = vertsN;
            indices[indicesN + 5] = vertsN + 1;

            indicesN += 6;
        }

        vertsN += 2;
    }

    
    
 
    void Start()
    {
        _meshFilter = GetComponent<MeshFilter>();
        _mesh = _meshFilter.mesh;
    }

    void Update()
    {
        
    }
}
