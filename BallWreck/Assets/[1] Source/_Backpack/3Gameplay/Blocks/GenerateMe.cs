﻿using UnityEngine;
using System.Reflection;

public class GenerateMe : MonoBehaviour
{
	public GenerateMe[] externalConnections = new GenerateMe[0];
	public bool external;


	IGeneratableSlot mySlot, resSlot;

	
	Transform context;
	string addr0;

	IGeneratable[] gens = new IGeneratable[0];
	IGeneratable[] clones = new IGeneratable[0];


	delegate void TransformIGen(IGeneratable igenIn, out IGeneratable igenOut);
	delegate void TransformIGenSlot(IGeneratableSlot igenSlotIn, out IGeneratableSlot igenSlotOut);

	TransformIGen transformIGen;
	TransformIGenSlot transformIGenSlot;
	
	

	void Awake () {
		DLog.Log("GenerateMe started for " + name, DLog.Level.UltraVerbose, gameObject);
		
		// выставляем, чтобы создавало клоны каждому IGen и IGenSlot
		
		if (external == false)
		{
			transformIGen = CreateCloneForIGen;
			transformIGenSlot = CreateCloneForIGenSlot;
			RunThroughAll();	
		}
		
		
		
	}

	void RunThroughThis(Transform t)
	{
		CheckComponents(t);
		Helper.RunActionToAllChildren(t, CheckComponents);
		
	}


	void RunThroughAll()
	{
		CheckComponents(transform);
		Helper.RunActionToAllChildren(transform, CheckComponents);

		for (int i = 0; i < externalConnections.Length; i++)
		{
			CheckComponents(externalConnections[i].transform);
			Helper.RunActionToAllChildren(externalConnections[i].transform, CheckComponents);
		}
	}

	
	
	public void AddAsExternalConnection(GenerateMe gm)
	{
		externalConnections = externalConnections.Add(gm);
		gm.externalConnections = gm.externalConnections.Add(this);
		gm.external = true;       
		
		transformIGen = CreateCloneForIGen;
		transformIGenSlot = CreateCloneForIGenSlot;
		RunThroughThis(gm.transform);
		
		// @Volatile: если куча externalConnections, то прошлые не добавят ничего
		
		gm.clones = new IGeneratable[clones.Length];
		for (int i = 0; i < clones.Length; i++)
			gm.clones[i] = clones[i]; 
		
		gm.gens = new IGeneratable[gens.Length];
		for (int i = 0; i < gens.Length; i++)
			gm.gens[i] = gens[i]; 
		
	}


	public IGeneratable GetClone(IGeneratable original)
	{
		int n = gens.IndexOf(original);
		if (n == -1) return null;
		return clones[n];
	}
	
	public void PropagateData(IGeneratableSlot igs)
	{
		mySlot = igs;
		
		transformIGen = null;
		transformIGenSlot = PropagateDataForIGenSlot;
		RunThroughAll();
	}
	
	public IGeneratableSlot FindTrend(IGeneratableSlot igs)
	{
		mySlot = igs;
		resSlot = null; 
		
		transformIGen = null;
		transformIGenSlot = FindTrendDataForIGenSlot;
		
		RunThroughAll();
       
		return resSlot;
	}

	void PropagateDataForIGenSlot(IGeneratableSlot igenSlotIn, out IGeneratableSlot igenSlotOut)
	{
		igenSlotOut = null;

		if (igenSlotIn == mySlot) return;
		if (igenSlotIn.UsesRef() == false) return;

		if (igenSlotIn.GetRef() == mySlot.GetRef())
			igenSlotIn.SetData(mySlot);
	}

	void FindTrendDataForIGenSlot(IGeneratableSlot igenSlotIn, out IGeneratableSlot igenSlotOut)
	{
		igenSlotOut = null;
		
		if (igenSlotIn == mySlot) return;
		if (igenSlotIn.UsesRef() == false) return;

		if (igenSlotIn.GetRef() == mySlot.GetRef())
			resSlot = igenSlotIn;
	}
	
	
	

	
	
	
	void CreateCloneForIGen(IGeneratable igenIn, out IGeneratable igenOut)
	{
		igenOut = null;


		bool needed;
		var clone = CloneIfNeeded(igenIn, out needed);
		if (needed)
			igenOut = clone;
	}

	void CreateCloneForIGenSlot(IGeneratableSlot igenSlotIn, out IGeneratableSlot igenSlotOut)
	{
		igenSlotOut = null;
		
		if (igenSlotIn.UsesRef() == false) return;

		var igen = igenSlotIn.GetRef();
		bool needed;
		var clone = CloneIfNeeded(igen, out needed);
		if (needed)
			igenSlotIn.SetRef(clone);
	}
	
	
	
	
	

	void CheckComponents(Transform t)
	{
		DLog.Log("Entering the " + name + "." + t.name, DLog.Level.UltraVerbose, t);
		
		var monos = t.GetComponents<MonoBehaviour>();

		context = t;
		
		      
		for (int i = 0; i < monos.Length; i++)
		{
			var type = monos[i].GetType();
			
			var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
			for (int j = 0; j < fields.Length; j++)
			{
				bool needed;
				System.Type igenT = null;
				
				addr0 = name + "." + t.name + "." + type.Name;
				
				if (fields[j].FieldType.IsArray == false)
				{
					
					igenT = fields[j].FieldType.GetInterface("IGeneratable");
					if (igenT != null)
					{
						var igen = fields[j].GetValue(monos[i]) as IGeneratable;
						IGeneratable igen2 = null;			
						
						if (transformIGen != null)						
							transformIGen(igen, out igen2);
						
						if (igen2 != null)
							fields[j].SetValue(monos[i], igen2);
					}
					else
					{
						igenT = fields[j].FieldType.GetInterface("IGeneratableSlot");
						if (igenT == null) continue;
						
						var igenSlot = fields[j].GetValue(monos[i]) as IGeneratableSlot;
						IGeneratableSlot igenSlot2 = null;
						
						if (igenSlot == null) continue;
						
						if (transformIGenSlot != null)
							transformIGenSlot(igenSlot, out igenSlot2);

						if (igenSlot2 != null)
							fields[j].SetValue(monos[i], igenSlot2);
					}
					
					
				}
				else
				{
					var elType = fields[j].FieldType.GetElementType();
					igenT = elType.GetInterface("IGeneratable");
					if (igenT != null)
					{
						var igens = fields[j].GetValue(monos[i]) as IGeneratable[];
						IGeneratable igen2 = null;
						
						if (igens == null) continue;
						
						string addrDefault = addr0;					 
						
						for (int k = 0; k < igens.Length; k++)
						{
							addr0 = addrDefault + " [" + k.ToString() + "]";
							
							if (transformIGen != null)
								transformIGen(igens[k], out igen2);
							
							if (igen2 != null)
								igens[k] = igen2;
						}
					}
					else
					{
						igenT = elType.GetInterface("IGeneratableSlot");
						if (igenT == null) continue;
						
						var igenSlots = fields[j].GetValue(monos[i]) as IGeneratableSlot[];
						IGeneratableSlot igenSlot2 = null;
						if (igenSlots == null) continue;

						for (int k = 0; k < igenSlots.Length; k++)
						{
							if (transformIGenSlot != null)
								transformIGenSlot(igenSlots[k], out igenSlot2);

							if (igenSlot2 != null)
								igenSlots[k] = igenSlot2;	
						}							
					}
				}
			}
		}
	}

	
	
	
	
	
	IGeneratable CloneIfNeeded(IGeneratable igen, out bool needed)
	{
		needed = false;
		if (igen == null) return null;

		var gen_so = igen as ScriptableObject;
		if (gen_so.name.Length == 0) return null;
		if (gen_so.name[0] != '_')
			return null;
				

		var index = gens.IndexOf(igen);
		IGeneratable clone = null;

		if (index == -1)
		{
			clone = igen.GetClone(name);
			clones = clones.Add(clone);
			gens = gens.Add(igen);
		}
		else
		{
			clone = clones[index];
		}
				
				
				
		if (index == -1)				
			DLog.Log(addr0 + " gets new clone!", DLog.Level.UltraVerbose, context);
		else DLog.Log(addr0 + " gets existing clone reference", DLog.Level.UltraVerbose, context);

		needed = true;
		return clone;
	}

	
}
