﻿using UnityEngine;

public class SetBorders : MonoBehaviour
{
    public LayerMask planeMask;
    
    Vector3 GetPosOnPlane(Vector3 start, Vector3 end)
    {
        RaycastHit hitInfo;
        bool hitted = Physics.Raycast(start, (end - start).normalized, out hitInfo, (end - start).magnitude, planeMask);
        Debug.DrawLine(start, end, Color.cyan);
        if (hitted)
        {
            return hitInfo.point;
        }

        return Vector3.zero;
    }
    

    void Awake()
    {
        var camera = Camera.main;

        var v1s = camera.ViewportToWorldPoint(new Vector3(0, 0, 1));
        var v1e = camera.ViewportToWorldPoint(new Vector3(0, 0, 100));
        var v2s = camera.ViewportToWorldPoint(new Vector3(1, 0, 1));
        var v2e = camera.ViewportToWorldPoint(new Vector3(1, 0, 100));
        var v3s = camera.ViewportToWorldPoint(new Vector3(1, 1, 1));
        var v3e = camera.ViewportToWorldPoint(new Vector3(1, 1, 100));
        var v4s = camera.ViewportToWorldPoint(new Vector3(0, 1, 1));
        var v4e = camera.ViewportToWorldPoint(new Vector3(0, 1, 100));
        
        
        
        
        
        Debug.DrawLine(v1s, v1e, Color.cyan, 1);
        Debug.DrawLine(v2s, v2e, Color.cyan, 1);
        Debug.DrawLine(v3s, v3e, Color.cyan, 1);
        Debug.DrawLine(v4s, v4e, Color.cyan, 1);

        var vs = new Vector3[4];
        
        vs[0] = GetPosOnPlane(v1s, v1e);
        vs[1] = GetPosOnPlane(v2s, v2e);
        vs[2] = GetPosOnPlane(v3s, v3e);
        vs[3] = GetPosOnPlane(v4s, v4e);
        
        Helper.DrawPointZ(vs[0], Color.red, 2);
        Helper.DrawPointZ(vs[1], Color.red, 2);
        Helper.DrawPointZ(vs[2], Color.red, 2);
        Helper.DrawPointZ(vs[3], Color.red, 2);

        Transform t;
        for (int i = 0; i < transform.childCount; i++)
        {
            var vn = i == 3 ? vs[0] : vs[i + 1];
            t = transform.GetChild(i);
            t.transform.position = (vn + vs[i]) * 0.5f;
            t.transform.rotation = Helper.RotateTo(vn - vs[i]);
            t.transform.rotation *= Quaternion.Euler(30, 0, 0); // @Volatile: зависит от камеры
            t.transform.localScale = new Vector3((vn - vs[i]).magnitude*2.0f, t.transform.localScale.y, t.transform.localScale.z);
            var p = t.transform.TransformPoint(0, -0.5f, 0);
            t.transform.position = p;
        }

        

        //var v2 = camera.ViewportToScreenPoint(new Vector3(0, 0, 0)); 



        //return Vector2.zero;
    }
    
}