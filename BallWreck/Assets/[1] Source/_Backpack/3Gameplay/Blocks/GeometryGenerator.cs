﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GeometryGenerator : MeshGenerator
{
    protected void GenerateCircle(float R, int sidesN) 
    {
        maxVertices = sidesN * 3 + 1;
        AddTriangleFan(Vector3.zero);
        
        for (int i = 0; i < sidesN; i++)
        {
            float ang = (float)i / (sidesN - 1);
            ang *= 360;

            AddTriangleFan(Helper.AngleVec(ang, R));

            
        }
    }

    protected void GenerateCircleSector(float R, float angStart, float angEnd, int sidesN)
    {
        maxVertices = sidesN * 3 + 1;

        AddTriangleFan(Vector3.zero);


        for (int i = 0; i < sidesN; i++)
        {
            float f = (float)i / (sidesN - 1);
            float ang = Mathf.Lerp(angStart, angEnd, f);
            
            AddTriangleFan(Helper.AngleVec(ang, R));
        }
    }

    protected void GenerateRing(float R0, float R1, int sidesN)
    {
        maxVertices = sidesN * 4 + 1;
        
        for (int i = 0; i < sidesN; i++)
        {
            float ang = (float)i / (sidesN - 1);
            ang *= 360;
            float angp = (float)(i-1) / (sidesN - 1);
            angp *= 360;
            AddQuad(Helper.AngleVec(angp, R0), Helper.AngleVec(angp, R1), Helper.AngleVec(ang, R1), Helper.AngleVec(ang, R0));
            //AddQuadStrip(Helper.AngleVec(ang, R0), Helper.AngleVec(ang, R1));
        }
    }

    protected void GenerateRingSector(float R0, float R1, float angStart, float angEnd, int sidesN)
    {
        maxVertices = sidesN * 4 + 1;
        
        for (int i = 0; i < sidesN; i++)
        {
            float f = (float)i / (sidesN - 1);
            float ang = Mathf.Lerp(angStart, angEnd, f);

            float fp = (float)(i-1) / (sidesN - 1);
            float angp = Mathf.Lerp(angStart, angEnd, fp);

            AddQuad(Helper.AngleVec(angp, R0), Helper.AngleVec(angp, R1), Helper.AngleVec(ang, R1), Helper.AngleVec(ang, R0));
        }
    }

    // @Unfinished
    //protected void GenerateCylinder(float R, float H, int sidesN)
    //protected void GenerateCylinderSector(float R, float H, float angStart, float angEnd, int sidesN)
    //protected void GenerateTube(float R0, float R1, float H, int sidesN)
    //protected void GenerateTubeSector(float R0, float R1, float H, float angStart, float angEnd, int sidesN)
   

}
