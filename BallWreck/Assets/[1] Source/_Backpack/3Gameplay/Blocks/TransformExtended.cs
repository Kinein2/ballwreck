﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformExtended : MonoBehaviour
{

	public UpdaterStyle updPosition, updRotation, updScale;
	public Vector3Reference position, rotation, scale;
	//public Vector3Reference localPosition, localTransform, localScale;
	//public Vector3Reference globalPosition, globalTransform, globalScale;

	public enum UpdaterStyle
	{
		DoNothing, SetGlobal, SetLocal, ReadGlobal, ReadLocal
	}

	
	void Update()
	{

		switch (updPosition)
		{
			case UpdaterStyle.SetGlobal:
				transform.position = position.Value;
				break;
			case UpdaterStyle.SetLocal:
				transform.localPosition = position.Value;
				break;
			case UpdaterStyle.ReadGlobal:
				position.Value = transform.position;
				break;
			case UpdaterStyle.ReadLocal:
				position.Value = transform.localPosition;
				break;
		}

		Quaternion q;
		switch (updRotation)
		{
			case UpdaterStyle.SetGlobal:
				q = transform.rotation;
				q.eulerAngles = rotation.Value;
				transform.rotation = q;
				break;
			case UpdaterStyle.SetLocal:
				q = transform.localRotation;
				q.eulerAngles = rotation.Value;
				transform.localRotation = q;
				break;
			case UpdaterStyle.ReadGlobal:
				rotation.Value = transform.rotation.eulerAngles;
				break;
			case UpdaterStyle.ReadLocal:
				rotation.Value = transform.localRotation.eulerAngles;
				break;
		}

		switch (updScale)
		{
			case UpdaterStyle.SetGlobal:
				//transform.lossyScale = position.Value; // @Incomplete:
				break;
			case UpdaterStyle.SetLocal:
				transform.localScale = scale.Value;
				break;
			case UpdaterStyle.ReadGlobal:
				scale.Value = transform.lossyScale;
				break;
			case UpdaterStyle.ReadLocal:
				scale.Value = transform.localScale;
				break;
		}
	}
}
