﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/*
[CustomEditor(typeof(UIDynamicLabel))]
public class UIDynamicLabelDrawer : Editor
{
	private GUIStyle popupStyle;

	public override void OnInspectorGUI()
	{
		if (popupStyle == null)
		{
			popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
			
			popupStyle.imagePosition = ImagePosition.ImageOnly;
		}

		var myTarget = (UIDynamicLabel)target;

		EditorGUI.BeginChangeCheck();
		serializedObject.Update();

		var property = serializedObject.FindProperty("labelType");
		EditorGUILayout.PropertyField(property);

		if (myTarget.labelType == UIDynamicLabel.LabelType.Simple)
		{
			property = serializedObject.FindProperty("result");
			EditorGUILayout.PropertyField(property);

			property = serializedObject.FindProperty("prefix");
			EditorGUILayout.PropertyField(property);

			property = serializedObject.FindProperty("postfix");
			EditorGUILayout.PropertyField(property);

			property = serializedObject.FindProperty("data");
			EditorGUILayout.PropertyField(property);
		} else if (myTarget.labelType == UIDynamicLabel.LabelType.Complex)
		{
			property = serializedObject.FindProperty("result");
			EditorGUILayout.PropertyField(property);

			property = serializedObject.FindProperty("formatString");
			EditorGUILayout.PropertyField(property);

			property = serializedObject.FindProperty("dataArray");
			EditorGUILayout.PropertyField(property, true);
		}

		property = serializedObject.FindProperty("updateEveryFrame");
		EditorGUILayout.PropertyField(property);



		Rect position = EditorGUILayout.GetControlRect();
		position = EditorGUI.PrefixLabel(position, new GUIContent("Text object"));

		Rect buttonRect = new Rect(position);
		buttonRect.yMin += popupStyle.margin.top;
		buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
		position.xMin = buttonRect.xMax;
		

		string[] popupOptions = { "TextUI", "TextMesh"};
		int selIndex = (int)myTarget.textType;

		selIndex = EditorGUI.Popup(buttonRect, selIndex, popupOptions, popupStyle);

		myTarget.textType = (UIDynamicLabel.TextType)selIndex;
		if (myTarget.textType == UIDynamicLabel.TextType.TextUI)
			property = serializedObject.FindProperty("textObj");
		else if (myTarget.textType == UIDynamicLabel.TextType.TextMesh)
			property = serializedObject.FindProperty("textMeshObj");


		EditorGUI.PropertyField(position, property, new GUIContent(""));

		position = EditorGUILayout.GetControlRect();
		if (GUI.Button(position, new GUIContent("Update string"))) 
			myTarget.UpdateText();
		
		if (EditorGUI.EndChangeCheck())
		{
			serializedObject.ApplyModifiedProperties();
			//myTarget.Update();
		}
	}
}

*/