﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;


[CustomPropertyDrawer(typeof(VariableReference), true)]
public class VarRefDrawer : PropertyDrawer
{
	/// <summary>
	/// Options to display in the popup to select constant or variable.
	/// </summary>
	private readonly string[] popupOptions =
		{ "Use Constant", "Set Variable", "Monitor Variable" };

	/// <summary> Cached style to use to draw the popup button. </summary>

	GUIStyle popupStyle, numberStyle, numberStyle2;
	object recycledEditor;
	MethodInfo doFloatFieldMethod;

	bool internalsInitted = false;

	GameObject owner = null;


	bool isVarRefGeneratable(GameObject owner, ScriptableObject variable)
	{
		if (variable == null)
			return false;
		
		if (variable.name[0] != '_')
			return false;

		bool parentDetected = false;
		
		Helper.RunActionToAllParents(owner.transform, t =>
		{
			var g = t.GetComponent<GenerateMe>();
			if (g != null)
				parentDetected = true;
		});

		return parentDetected;
	}

	void OnVarUpdate(SerializedProperty constantValue, SerializedProperty variable)
	{
		if (isVarRefGeneratable(owner, variable.objectReferenceValue as ScriptableObject))
		{
			// FindTrend;	
		}
		else
		{
				
		}                
	}

	void Propagate()
	{
		GenerateMe found = null;
		
		Helper.RunActionToAllParents(owner.transform, t =>
		{
			var g = t.GetComponent<GenerateMe>();
			if (g != null)
				found = g;
		});

		if (found == null) return;

		//found.PropagateData(propert);
	}

	void FindTrend()
	{
		
	}
	
	
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		// Get properties
		SerializedProperty useMode = property.FindPropertyRelative("useMode");
		SerializedProperty constantValue = property.FindPropertyRelative("constantValue");
		SerializedProperty variable = property.FindPropertyRelative("variable");

		
		if (variable == null) return;


		if (internalsInitted == false)
		{
			InitInternal();
			internalsInitted = true;
		}

		owner = (property.serializedObject.targetObject as MonoBehaviour).gameObject; // @Volatile: если мы в странных местах референсом пользуемся (не MonoBehaviour), то это всё навернется
		

		label = EditorGUI.BeginProperty(position, label, property);
		position = EditorGUI.PrefixLabel(position, label);

		EditorGUI.BeginChangeCheck();



		int selIndex = useMode.enumValueIndex;

		// Calculate rect for configuration button
		Rect buttonRect = new Rect(position);
		buttonRect.yMin += popupStyle.margin.top;
		buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
		position.xMin = buttonRect.xMax;

		string t = GetPropertyType(variable);


		// Store old indent level and set it to 0, the PrefixLabel takes care of it
		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;
		
		
		
		int result = EditorGUI.Popup(buttonRect, selIndex, popupOptions, popupStyle);

		if (selIndex == 0 && selIndex != result)
		{
			OnVarUpdate(constantValue, variable);
		}
		
		if (result == 2)
			if (variable.objectReferenceValue == null)
			{
				result = 1;
			}

		useMode.enumValueIndex = result;
		
		switch (result)
		{
			case 0:

				if (t == "QuaternionVariable")
				{
					var q = constantValue.quaternionValue;
					var v = q.eulerAngles;
					v = EditorGUI.Vector3Field(position, GUIContent.none, v);
					q.eulerAngles = v;
				}
				else if (t == "FloatVariable" || t == "NumberVariable") // т.к. мы меняем число, считаем Number как Float
				{
					Rect hotZone = position;
					hotZone.width = 5;

					constantValue.floatValue = MyFloatFieldInternal(position, hotZone, constantValue.floatValue, numberStyle);
					//EditorGUI.PropertyField(position, constantValue, new GUIContent(""));
				}
				else
				{
					EditorGUI.PropertyField(position, constantValue, GUIContent.none);
				}
				break;
			case 1:
				var val = variable.objectReferenceValue;
				EditorGUI.PropertyField(position, variable, GUIContent.none);
				if (val != variable.objectReferenceValue)
				{
					OnVarUpdate(constantValue, variable);
				}
				
				break;
			case 2:
				MonitorValue(position, constantValue, variable.objectReferenceValue);
				break;
		}

		if (EditorGUI.EndChangeCheck() || selIndex == 2)
			property.serializedObject.ApplyModifiedProperties();

		if (result == 2)
		{
			if (Application.isPlaying == false)
			{
				var SOV =  variable.objectReferenceValue as ScriptObj_Variable;
				SOV.SaveDefault();
				//Debug.Log("Saved");
			}
		}
		
		EditorGUI.indentLevel = indent;
		EditorGUI.EndProperty();
	}

	public static string GetPropertyType(SerializedProperty property)
	{
		var type = property.type;
		var match = Regex.Match(type, @"PPtr<\$(.*?)>");
		if (match.Success)
			type = match.Groups[1].Value;
		return type;
	}



	void InitInternal()
	{
		popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
		popupStyle.imagePosition = ImagePosition.ImageOnly;

		numberStyle = new GUIStyle(EditorStyles.numberField);
		numberStyle2 = new GUIStyle(EditorStyles.numberField);
		numberStyle2.normal.textColor = new Color(80 / 255.0f, 0 / 255.0f, 105 / 255.0f);
		// MyFloatFieldInternal
		Type editorGUIType = typeof(EditorGUI);

		Type RecycledTextEditorType = Assembly.GetAssembly(editorGUIType).GetType("UnityEditor.EditorGUI+RecycledTextEditor");
		Type[] argumentTypes = new Type[] { RecycledTextEditorType, typeof(Rect), typeof(Rect), typeof(int), typeof(float), typeof(string), typeof(GUIStyle), typeof(bool) };
		doFloatFieldMethod = editorGUIType.GetMethod("DoFloatField", BindingFlags.NonPublic | BindingFlags.Static, null, argumentTypes, null);

		FieldInfo fieldInfo = editorGUIType.GetField("s_RecycledEditor", BindingFlags.NonPublic | BindingFlags.Static);
		recycledEditor = fieldInfo.GetValue(null);


	}


	float MyFloatFieldInternal(Rect position, Rect dragHotZone, float value, GUIStyle style)
	{
		//[DefaultValue("EditorStyles.numberField")]
		int controlID = GUIUtility.GetControlID("EditorTextField".GetHashCode(), FocusType.Keyboard, position);
		object[] parameters = new object[] { recycledEditor, position, dragHotZone, controlID, value, "g7", style, true };

		return (float)doFloatFieldMethod.Invoke(null, parameters);
	}


	protected void MonitorValue(Rect position, SerializedProperty constProp, UnityEngine.Object refVal)
	{
		GUIStyle style = new GUIStyle(GUI.skin.textField);
		style.normal.textColor = new Color(80 / 255.0f, 0 / 255.0f, 105 / 255.0f);

		// @Volatile: у половины типов нет визуализации что это мониторинг

		Color colorBefore = EditorStyles.textField.normal.textColor;
		EditorStyles.textField.normal.textColor = style.normal.textColor;

		bool playing = Application.isPlaying;
		bool generatable = isVarRefGeneratable(owner, refVal as ScriptableObject);

		if (refVal.GetType() == typeof(FloatVariable) || refVal.GetType() == typeof(SmartFloatVar))
		{
			var FV = refVal as FloatVariable;
			float f;

			if (playing)
				f = FV.Value;
			else
			{
				if (generatable)
					f = constProp.floatValue;
				else f = FV.Value;
			}


			Rect hotZone = position;
			hotZone.width = 5;
			
			float fn = MyFloatFieldInternal(position, hotZone, f, numberStyle2);

			if (fn.Equals(f) == false)
			{
				if (playing)
					FV.Value = fn;
				else
				{
					if (generatable)
					{
						constProp.floatValue = fn;
						Propagate();
					}
					else
					{
						FV.Value = fn;
						constProp.floatValue = fn;	
					}
				}	
			}
			
			

		}
		else if (refVal.GetType() == typeof(IntegerVariable) || refVal.GetType() == typeof(SmartIntegerVar))
		{
			var IV = refVal as IntegerVariable;
			int i;

			if (playing)
				i = IV.Value;
			else i = constProp.intValue; 
			
			i = EditorGUI.IntField(position, i, style);

			if (playing)
				IV.Value = i;
			else constProp.intValue = i;
		}
		else if (refVal.GetType() == typeof(StringVariable))
		{
			var SV = refVal as StringVariable;
			string s = SV.Value;
			s = EditorGUI.TextField(position, s, style);
			SV.Value = s;
		}
		else if (refVal.GetType() == typeof(ColorVariable))
		{
			var CV = refVal as ColorVariable;
			Color c = CV.Value;

			c = EditorGUI.ColorField(position, "monitor:", c);

			CV.Value = c;
		}
		else if (refVal.GetType() == typeof(Vector2Variable))
		{
			var V2V = refVal as Vector2Variable;
			Vector2 v2;

			if (playing)
				v2 = V2V.Value;
			else v2 = constProp.vector2Value;

			v2 = EditorGUI.Vector2Field(position, "", v2);

			if (playing)
				V2V.Value = v2;
			else constProp.vector2Value = v2;

		}
		else if (refVal.GetType() == typeof(Vector3Variable))
		{
			var V3V = refVal as Vector3Variable;
			Vector2 v3 = V3V.Value;

			v3 = EditorGUI.Vector3Field(position, "", v3);
			//f = EditorGUI
			V3V.Value = v3;
		}
		else if (refVal.GetType() == typeof(QuaternionVariable))
		{
			var QV = refVal as QuaternionVariable;
			Quaternion q = QV.Value;
			var v = q.eulerAngles;
			v = EditorGUI.Vector3Field(position, "", v);
			q.eulerAngles = v;
			QV.Value = q;
		}

		EditorStyles.textField.normal.textColor = colorBefore;
	}

	//public override bool RequiresConstantRepaint() { return true; }
}