﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UIProgressBar))]
public class UIProgressBarDrawer : Editor
{
	private GUIStyle popupStyle;

	public override void OnInspectorGUI()
	{
		if (popupStyle == null)
		{
			popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
			
			popupStyle.imagePosition = ImagePosition.ImageOnly;
		}

		var myTarget = (UIProgressBar)target;

		EditorGUI.BeginChangeCheck();
		serializedObject.Update();

		var property = serializedObject.FindProperty("current");
		EditorGUILayout.PropertyField(property, new GUIContent("Current"));


		if (myTarget.UseSeperateMinMax())
		{
			property = serializedObject.FindProperty("min");
			EditorGUILayout.PropertyField(property, new GUIContent("Min"));

			property = serializedObject.FindProperty("max");
			EditorGUILayout.PropertyField(property, new GUIContent("Max"));
		} else
		{
			if (myTarget.current.variable is SmartFloatVar)
			{
				var SFV = myTarget.current.variable as SmartFloatVar;
			
				EditorGUILayout.LabelField("Min", SFV.minValue.Value.ToString());
				EditorGUILayout.LabelField("Max", SFV.maxValue.Value.ToString());
			} else
			{
				var SIV = myTarget.current.variable as SmartIntegerVar;
			
				EditorGUILayout.LabelField("Min", SIV.minValue.Value.ToString());
				EditorGUILayout.LabelField("Max", SIV.maxValue.Value.ToString());
			}
			
		}

		//property = serializedObject.FindProperty("imageType");
		//EditorGUILayout.PropertyField(property);
		
		property = serializedObject.FindProperty("centered");
		EditorGUILayout.PropertyField(property, new GUIContent("Centered"));
		
		property = serializedObject.FindProperty("dynamic");
		EditorGUILayout.PropertyField(property, new GUIContent("Dynamic max"));
		
		if (EditorGUI.EndChangeCheck())
		{
			serializedObject.ApplyModifiedProperties();
			//myTarget.Update();
		}
	}
}
