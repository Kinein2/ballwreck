﻿﻿using UnityEngine;


// ProjectionClicker - по нажатию клавиши выдает сигнал с проекцией на плоскость
public class ProjectionClicker2 : MonoBehaviour
{
    public enum Axis
    {
        XY, XZ
    }

    public Axis axis = Axis.XY;
    
    
    public LayerMask planeMask;

    public float distance0, distanceMax;
    
    
            
    [Optional]
    public Vector2Trigger onBegan, onEnded;

    public Vector2InstantVariable cursorTarget;
    
    Camera _camera;
    
    void Start()
    {
        _camera = Camera.main; // @Volatile: считает, что камера мэйн не меняется в игре 
        
        cursorTarget.instant.SetSource(GetCursorPos);
    }


    Vector2 GetCursorPos()
    {
        var v1 = GetCursorWorldPos(distance0);
        var v2 = GetCursorWorldPos(distanceMax);

        RaycastHit hitInfo;
        bool hitted = Physics.Raycast(v1, v2 - v1, out hitInfo, distanceMax - distance0, planeMask);
        
        if (hitted)
        {
            Debug.DrawLine(v1, hitInfo.point, Color.red);
            Debug.DrawLine(hitInfo.point, v2, Color.cyan);
            if (axis == Axis.XY)
                return hitInfo.point;
            else return hitInfo.point.ToXZ();
        } else Debug.DrawLine(v1, v2, Color.cyan);

        return Vector2.zero;
    }
    

    Vector3 GetCursorWorldPos(float distance)
    {
        var ts = InputHelper.GetTouches();
        if (ts != null)
            if (ts.Count > 0)
            {
                for (int i = 0; i < ts.Count; i++)               
                {
                    Vector3 v = _camera.ScreenToWorldPoint((Vector3)ts[i].position + distance * Vector3.forward);
                    return v;
                }
            } 
        
        return  Vector3.zero;
    }
    
    void Update()
    {
        var ts = InputHelper.GetTouches();
        if (ts != null)
            if (ts.Count > 0)
            {
                for (int i = 0; i < ts.Count; i++)          
                    if (ts[i].phase == TouchPhase.Began || ts[i].phase == TouchPhase.Ended)    
                {
                    
                    if (onBegan)
                    if (ts[i].phase == TouchPhase.Began)
                        onBegan.Raise(this, GetCursorPos());
                    
                    if (onEnded)
                    if (ts[i].phase == TouchPhase.Ended)
                        onEnded.Raise(this, GetCursorPos());

                }
            }
    }
}