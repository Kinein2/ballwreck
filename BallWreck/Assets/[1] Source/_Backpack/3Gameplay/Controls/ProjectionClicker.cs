﻿using UnityEngine;


// ProjectionClicker - по нажатию клавиши выдает сигнал с проекцией на определенную Z-длину
public class ProjectionClicker : MonoBehaviour
{
    
    
    public float distance;
            
    [Optional]
    public Vector2Trigger onBegan, onEnded;

    public Vector2InstantVariable cursorTarget;
    
    Camera _camera;
    
    void Start()
    {
        _camera = Camera.main; // @Volatile: считает, что камера мэйн не меняется в игре 
        
        cursorTarget.instant.SetSource(GetCursorPos);
    }
    
    
    // @Volatile: нет обработки нескольких пальцев
    Vector2 GetCursorPos()
    {
        var ts = InputHelper.GetTouches();
        if (ts != null)
            if (ts.Count > 0)
            {
                for (int i = 0; i < ts.Count; i++)               
                {
                    Vector3 v = _camera.ScreenToWorldPoint((Vector3)ts[i].position + distance * Vector3.forward);
                    return v;
                }
            } 
        
        return  Vector2.zero;
    }
    
    void Update()
    {
        var ts = InputHelper.GetTouches();
        if (ts != null)
            if (ts.Count > 0)
            {
                for (int i = 0; i < ts.Count; i++)          
                    if (ts[i].phase == TouchPhase.Began || ts[i].phase == TouchPhase.Ended)    
                {
                    Vector3 v = _camera.ScreenToWorldPoint((Vector3)ts[i].position + distance * Vector3.forward);
                    Debug.DrawLine(transform.position, v);
                    
                    if (onBegan)
                    if (ts[i].phase == TouchPhase.Began)
                        onBegan.Raise(this, v.ToXZ());
                    
                    if (onEnded)
                    if (ts[i].phase == TouchPhase.Ended)
                        onEnded.Raise(this, v.ToXZ());

                }
            }
    }
}