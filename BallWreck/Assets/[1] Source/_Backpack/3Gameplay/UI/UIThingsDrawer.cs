﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIThingsDrawer : MonoBehaviour
{

    public GameObject frontObject, backObject;

    public IntegerReference amount, width, max;

    public enum PlacementStyle
    {
        Ortho, Radial, Custom
    }

    public PlacementStyle placementStyle = PlacementStyle.Ortho;

    public Vector2 stepDist;

    //bool inCanvas;
    int _amount0;
    Vector2 stepDist0;

    Transform _fronts, _backs;

    void Start()
    {
        GrandInit();

    }

    void GrandInit()
    {
		if (placementStyle == PlacementStyle.Custom)
			return;

        var go = new GameObject();
        go.transform.SetParent(transform, false);
        go.transform.localPosition = Vector3.zero;

        var go2 = new GameObject();
        go2.transform.SetParent(transform, false);
        go2.transform.localPosition = Vector3.zero;

        go.name = "Backs"; go2.name = "Fronts";
        _backs = go.transform;
        _fronts = go2.transform;

        for (int i = 0; i < max; i++)
        {
            if (backObject != null)
            {
                var bo = Instantiate(backObject);
                bo.transform.SetParent(_backs, false);
                bo.transform.localPosition = GetPos(i);
                bo.gameObject.SetActive(true);
            }

            if (frontObject != null)
            {
                var fo = Instantiate(frontObject);
                fo.transform.SetParent(_fronts, false);
                fo.transform.localPosition = GetPos(i);
            }
        }

        stepDist0 = stepDist;

        UpdateObjects();
    }

    void GrandClear()
    {
		if (placementStyle == PlacementStyle.Custom)
			return;
        Destroy(_fronts.gameObject);
        Destroy(_backs.gameObject);
        _fronts = null; _backs = null;
    }

    Vector2 GetPos(int i)
    {
        int j;
        if (width == 0)
            j = 0;
        else
        {
            j = i / width;
            i = i % width;
        }

        if (placementStyle == PlacementStyle.Ortho)
        {
            return new Vector2(i * stepDist.x, j * stepDist.y);
        }

        if (placementStyle == PlacementStyle.Radial)
        {
            return Helper.AngleVec(i/(float)max*360, stepDist.x);
        }

		return Vector2.zero;
    }

    void UpdateObjects()
    {
        if (frontObject != null)
            for (int i = 0; i < max; i++)
            {
                var f = _fronts.GetChild(i);
                f.gameObject.SetActive(i < amount);
            }

        _amount0 = amount;
    }

    void Update()
    {

        if (amount != _amount0)
        {
            UpdateObjects();

        }

        if ((stepDist - stepDist0).magnitude > 0.0001f)
        {
            GrandClear();
            GrandInit();
        }
    }
}
