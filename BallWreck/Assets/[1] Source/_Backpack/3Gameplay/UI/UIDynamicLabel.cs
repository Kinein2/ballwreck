﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UIDynamicLabel : MonoBehaviour {
	
	public enum LabelType { Simple, Complex }

	public LabelType labelType = LabelType.Simple;

	

	[ShowIf("labelType", LabelType.Simple)]	
	public ScriptObj_Variable data;
	
	public StringReference result;
	
	[ShowIf("labelType", LabelType.Simple)]
	public StringReference prefix, postfix;
	
	

	[ShowIf("labelType", LabelType.Complex)]
	public string formatString;

	[ShowIf("labelType", LabelType.Complex)]
	public ScriptObj_Variable[] dataArray;


	public int precisionPoint = -1;
	[HideIf("precisionPoint", -1)] 
	public bool addZeros;

	
	public bool updateEveryFrame = true;
	
	public bool bigNumberReduction;
	[ShowIf("bigNumberReduction")] 
	public float[] steps = new float[0];
	[ShowIf("bigNumberReduction")] 
	public string[] stepPrefixes = new string[0];

	[ShowIf("bigNumberReduction")] 
	public float tolerance = 1;
	

    [Optional]
	public GameObject target;
	
	Text _textObj;
	TextMesh _textMeshObj;
	TextMeshProUGUI _textMeshProObj;
	
	void Start ()
	{
		if (target == null)
		{
			_textObj = GetComponent<Text>();
			_textMeshObj = GetComponent<TextMesh>();
			_textMeshProObj = GetComponent<TextMeshProUGUI>();				
		}
		else
		{
			_textObj = target.GetComponent<Text>();
			_textMeshObj = target.GetComponent<TextMesh>();
			_textMeshProObj = target.GetComponent<TextMeshProUGUI>();
		}

		
	}
	
	string GetStringFromVariable(ScriptObj_Variable VR)
	{
		if (VR is BooleanVariable)
			return ((BooleanVariable)VR).Value.ToString();

		if (VR is IntegerVariable)
			return ((IntegerVariable)VR).Value.ToString();

		if (VR is FloatVariable)
		{
			if (precisionPoint == -1)
				return ((FloatVariable) VR).Value.ToString();
			else return ClampFloat(((FloatVariable) VR).Value);

		}

		if (VR is StringVariable)
			return ((StringVariable)VR).Value.ToString();

		if (VR is ColorVariable)
			return ((ColorVariable)VR).Value.ToString();

		if (VR is Vector2Variable)
			return ((Vector2Variable)VR).Value.ToString();

		if (VR is Vector3Variable)
			return ((Vector3Variable)VR).Value.ToString();

		if (VR is QuaternionVariable)
			return ((QuaternionVariable)VR).Value.ToString();

		return "";
	}

	void NumberReduction(float number, out float resNumber, out string postfix)
	{
		int n = -1;
		for (int i = 0; i < steps.Length; i++)
		{
			if (number / tolerance > steps[i])
			{
				number /= steps[i];
				n = i;
			}
		}

		postfix = "";
		
		if (n != -1)
			postfix = stepPrefixes[n];
		
		resNumber = number;
	}

	string ClampFloat(float number)
	{
		string res = number.Whole().ToString();

		string zeroLine = "";
		
		for (int i = 0; i < precisionPoint; i++)
		{
			
			
			number = number.Fraction();
			number *= 10;

			int n = number.Whole();
			
			if (n == 0 && addZeros == false)
				zeroLine += n.ToString();
			else
			{	
				if (i == 0)
					res += ".";
				res += zeroLine + number.ToString();
				zeroLine = "";
			}
		}

		return res;
	}
	
	System.Object GetObjectFromVariable(ScriptObj_Variable VR)
	{
		if (VR is BooleanVariable)
			return ((BooleanVariable) VR).Value;

		if (VR is IntegerVariable)
			return ((IntegerVariable) VR).Value;

		if (VR is FloatVariable)
		{
			float f = ((FloatVariable) VR).Value;
			if (bigNumberReduction)
			{
				float resNumber;
				string postfix;
				
				NumberReduction(f, out resNumber, out postfix);
				if (precisionPoint == -1)
					return resNumber.ToString() + postfix;
				else return ClampFloat(resNumber) + postfix;
			}
			else
			{
				if (precisionPoint == -1)
					return ((FloatVariable) VR).Value;
				else return ClampFloat(((FloatVariable) VR).Value);
			}

		
		}
			

		if (VR is StringVariable)
			return ((StringVariable) VR).Value;

		if (VR is ColorVariable)
			return ((ColorVariable) VR).Value;

		if (VR is Vector2Variable)
			return ((Vector2Variable) VR).Value;

		if (VR is Vector3Variable)
			return ((Vector3Variable) VR).Value;

		if (VR is QuaternionVariable)
			return ((QuaternionVariable) VR).Value;

		return "";
	}
	
	
	void Update () {
		if (updateEveryFrame)
		{
			UpdateText();
		}
	}

	public void UpdateText()
	{
		UpdateResultString();
		UpdateTextGraphics();
	}

	void UpdateResultString()
	{
		if (labelType == LabelType.Simple)
		{
			string s = GetStringFromVariable(data);
			SetString(s);
		} else if (labelType == LabelType.Complex)
		{
			
			System.Object[] sArr = new System.Object[dataArray.Length];
			for (int i = 0; i < dataArray.Length; i++)
			{
				sArr[i] = GetObjectFromVariable(dataArray[i]);
			}

			result.Value = string.Format(formatString, sArr);
		}
	}

	void UpdateTextGraphics()
	{
		if (Application.isPlaying == false) {
			Start();
		}

		if (_textObj)
			_textObj.text = result;
		if (_textMeshObj)
			_textMeshObj.text = result;
		if (_textMeshProObj)
			_textMeshProObj.text = result;
		
		
	}
	

	void SetString(string s)
	{
		if (labelType == LabelType.Simple)
			result.Value = prefix + s + postfix;
		else if (labelType == LabelType.Complex)
		{
			result.Value = string.Format(formatString, s);
		}
	}

	// специально для GoodEvent'а
	public void GetStringFromBool(bool VR)
	{
		SetString(VR.ToString());
	}

	public void GetStringFromInt(int VR)
	{
		SetString(VR.ToString());
	}

	public void GetStringFromFloat(float VR)
	{
		SetString(VR.ToString());
	}

	public void GetStringFromString(string VR)
	{
		SetString(VR.ToString());
	}

	public void GetStringFromColor(Color VR)
	{
		SetString(VR.ToString());
	}

	public void GetStringFromVector2(Vector2 VR)
	{
		SetString(VR.ToString());
	}

	public void GetStringFromVector3(Vector3 VR)
	{
		SetString(VR.ToString());
	}

	public void GetStringFromQuaternion(Quaternion VR)
	{
		SetString(VR.ToString());
	}
}
