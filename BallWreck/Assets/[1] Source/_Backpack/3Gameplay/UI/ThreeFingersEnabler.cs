﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreeFingersEnabler : MonoBehaviour
{
    public bool pauseInMenu, saveStartEnd;
    
    bool active;
    float t0;
    

    // Update is called once per frame
    void Update()
    {
        bool change = Input.GetMouseButtonDown(1);
        
        var ts = InputHelper.GetTouches();
        
        if (ts.Count > 2)
            if (ts[2].phase == TouchPhase.Began)
                change = true;        

        if (change)
        {
            active = !active;
            transform.GetChild(0).gameObject.SetActive(active);

            if (pauseInMenu)
            {
                if (active)
                {
                    t0 = Time.timeScale;
                    Time.timeScale = 0;
                }
                else Time.timeScale = t0;
            }

            if (saveStartEnd)
            {
                if (active == false)
                {
                    SingletonPlayerprefs.Instance.SaveData();
                }
                else
                {
                    SingletonPlayerprefs.Instance.LoadData();
                }     
            }
            
            
                
        }
    }
}
