﻿using UnityEngine;

public abstract class UITableSlot : MonoBehaviour
{
    public abstract void Compile(int slotIndex, GameObject source);
}



// @Unfinished: может включать выключать картинки каким нибудь булеаном хз, хотя это не в тему
public class UISlotVar : UITableSlot
{
    [Optional] 
    public IntegerVariable index;

    [Optional]
    public StringVariable sourceName;
    
    public override void Compile(int slotIndex, GameObject source)
    {
        if (index != null)
            index.Value = slotIndex + 1;
        
        if (source == null)
            return;

        var gm = source.GetComponent<GenerateMe>();
        if (gm == null)
            return;

        gameObject.SetActive(false);
        var gmMe = gameObject.AddComponent<GenerateMe>();
        gm.AddAsExternalConnection(gmMe);
        gameObject.SetActive(true);
        
        if (index != null)
            index.Value = slotIndex + 1;

        if (sourceName != null)
            sourceName.Value = source.name;
    }
}