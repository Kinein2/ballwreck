﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UINumberTweaker : MonoBehaviour
{
    public float tickAddition;
    
    public SimpleTrigger plusAction, minusAction;
    public NumberVariable number;
    

    public TextMeshProUGUI numberText, labelText;

    void Start()
    {
        plusAction.AddCallback(this, OnPlusPressed);
        minusAction.AddCallback(this, OnMinusPressed);

        labelText.text = number.name;
        numberText.text = (number.GetInt()).ToString();
    }

    void OnPlusPressed()
    {
        number.Set(number.GetFloat()+tickAddition);
        numberText.text = (number.GetInt()).ToString();
    }
    
    void OnMinusPressed()
    {
        number.Set(number.GetFloat()-tickAddition);
        numberText.text = (number.GetInt()).ToString();
    }
    
}
