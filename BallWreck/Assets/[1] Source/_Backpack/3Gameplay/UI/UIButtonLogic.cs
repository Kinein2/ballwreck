﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonLogic : MonoBehaviour
{

    public enum PressedStyle
    {
        PushDown, PushRadial
    }

    public PressedStyle style = PressedStyle.PushDown;
    
    public Image buttonTop, buttonSide;

    public float pressDepth;

    public SimpleTrigger pressedTrig, outtedTrig, outtedGeneralTrig;
    public SimpleTrigger actionTrig;

    public bool ignoreIfSwiped;
    public float maxSwiped;
    
    bool pressed;
    RectTransform topRT, sidesRT;
    Vector3 oldPos;
    
    void Start()
    {
        pressedTrig.AddCallback(this, OnPressed);
        outtedTrig.AddCallback(this, OnOutted);
        outtedGeneralTrig.AddCallback(this, OnOuttedMissed);
        
        if (buttonTop)
            topRT = buttonTop.GetComponent<RectTransform>();
        if (buttonSide)
            sidesRT = buttonSide.GetComponent<RectTransform>();
    }

    void OnPressed()
    {
        pressed = true;
        oldPos = transform.position;

        PressVisual(true);
    }

    
    
    void OnOutted()
    {
        if (pressed)
        {
            actionTrig.Raise(this);
            pressed = false;
            PressVisual(false);
        }
    }

    void OnOuttedMissed()
    {
        pressed = false;       

    }

    void PressVisual(bool enable)
    {
       
        int k = 1;
        if (enable == false)
            k = -1;
        
        if (style == PressedStyle.PushDown)
        {
            if (buttonTop)
                topRT.position = topRT.position.IncY(-k*pressDepth * 0.5f);
            if (buttonSide)
                sidesRT.sizeDelta = sidesRT.sizeDelta.IncY(-k*pressDepth*0.25f);    
        }
        else if (style == PressedStyle.PushRadial)
        {
            if (buttonTop)
            {
                topRT.sizeDelta = topRT.sizeDelta.IncX(-k*pressDepth);
                topRT.sizeDelta = topRT.sizeDelta.IncY(-k*pressDepth);
            }
                
        }
    }

    void Update()
    {
        if (ignoreIfSwiped)
            if ((transform.position - oldPos).magnitude > maxSwiped)
        
                if (pressed)
                {
                    PressVisual(false);
                    pressed = false;
                } 
    }
}
