﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIClickerHost : MonoBehaviour
{
	[SerializeField]
	SimpleTrigger onClick, onRaise, onClickMissed, onRaiseMissed;
	
	GraphicRaycaster GR;
	UIClickerReciever touched = null;

	void Start () 
	{
		
		GR = transform.parent.GetComponent<GraphicRaycaster>();
		if (GR == null)
		{
			GR = transform.parent.parent.GetComponent<GraphicRaycaster>();
		}
	}

	bool clicked;
	
	void Update () 
	{
		touched = null;
		
		var ts = InputHelper.GetTouches();

		if (ts.Count == 0) return;
		clicked = false;
		for (int i = 0; i < ts.Count; i++)
		if (ts[i].phase == TouchPhase.Began || ts[i].phase == TouchPhase.Ended)

			//if (Input.GetMouseButtonDown(0))
			{
				// должно быть у хоста
				DLog.Log("Clicked", DLog.Level.UltraVerbose, this, "INPUT (BP)");
				PointerEventData ped = new PointerEventData(null);
				ped.position = ts[i].position;
				List<RaycastResult> results = new List<RaycastResult>();
				
				GR.Raycast(ped, results);
				
				foreach (var r in results)
				{
				//	Debug.Log(r.gameObject);
					bool b = RaycastedObj(ts[i].phase, r.gameObject);
					clicked = clicked || b;
				}

				if (ts[i].phase == TouchPhase.Began)
				if (onClick != null)
					onClick.Raise(this);
				
				if (ts[i].phase == TouchPhase.Began)
				if (onClickMissed && clicked == false)
					onClickMissed.Raise(this);
				
				if (ts[i].phase == TouchPhase.Ended)
				if (onRaiseMissed && clicked == false)
					onRaiseMissed.Raise(this);
				//onClick.Raise(this);
			}
	}

	

	bool RaycastedObj(TouchPhase phase, GameObject go)
	{
		var CR = go.GetComponent<UIClickerReciever>();
		if (CR == null) return false;

		DLog.Log("Catched - " + CR.gameObject.name, DLog.Level.Verbose, CR, "INPUT (BP)");

		bool b = CR.React(phase);

		if (phase == TouchPhase.Moved)
			return false;

		return b;
	}

}

