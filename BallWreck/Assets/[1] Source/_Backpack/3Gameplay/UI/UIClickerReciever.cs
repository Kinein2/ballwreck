﻿using UnityEngine;

using System;

public class UIClickerReciever : MonoBehaviour
{

	[SerializeField]
	SimpleTrigger reactOnPress, reactOnOut;	
	
	public bool React(TouchPhase phase)
	{
		if (phase == TouchPhase.Began)
			if (reactOnPress != null)
			{
				reactOnPress.Raise(this);
				return true;
			}
				

		if (phase == TouchPhase.Ended)
			if (reactOnOut != null)
			{
				reactOnOut.Raise(this);
				return true;
			}

		return false;

	}

}
