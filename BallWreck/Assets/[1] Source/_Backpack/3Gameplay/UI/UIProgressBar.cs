﻿using UnityEngine;
using UnityEngine.UI;

public class UIProgressBar : MonoBehaviour
{
	
	public NumberReference current;
	public NumberReference min;
	public NumberReference max;

	public GameObject target;

	float imageWidth0 = -1;

	public bool centered, dynamic = false;

	Image _imageUI;
	SpriteRenderer _imageSR;
	
	float percentageBefore;

	private void Start()
	{
		if (target == null)
			target = gameObject;
		
		_imageUI = target.GetComponent<Image>();
		_imageSR = target.GetComponent<SpriteRenderer>();
		
		if (_imageUI != null)
			imageWidth0 = _imageUI.rectTransform.rect.width;
		
		if (_imageSR != null)
			imageWidth0 = _imageSR.transform.localScale.x;

		percentageBefore = GetPercentage();
	}

                                                                                                                                     
	public void Update()
	{

		if (dynamic && UseSeperateMinMax())
		{
			if (current.Value < min.Value)
				min.Value = current.Value;

			if (current.Value > max.Value)
				max.Value = current.Value;
		}

		float percentage = GetPercentage();
		UpdateImage(percentage);
	}

	public float GetPercentage()
	{
		float percentage = 0;
		bool seperate = UseSeperateMinMax();

		if (seperate)
		{
			percentage = current.Value - min.Value;
			percentage /= (max.Value - min.Value);
		}
		else
		{
			if (current.variable is SmartFloatVar)
			{
				SmartFloatVar cv = current.variable as SmartFloatVar;
				percentage = cv.GetPercentage();
			} else if (current.variable is SmartIntegerVar)
			{
				SmartIntegerVar cv = current.variable as SmartIntegerVar;
				percentage = cv.GetPercentage();
			}
		}

		return percentage;
	}

	

	void UpdateImage(float percentage)
	{
		
		if (_imageUI != null)
		{
			if (_imageUI.type == Image.Type.Filled)
				_imageUI.fillAmount = Mathf.Clamp01(percentage);
			else
				_imageUI.rectTransform.sizeDelta = new Vector2 (imageWidth0*percentage, _imageUI.rectTransform.sizeDelta.y);
			
			
		} else 
		{
			
			
			_imageSR.transform.localScale = new Vector2 (imageWidth0*percentage, _imageSR.transform.localScale.y);
			if (centered)
			{
				Vector3 v = _imageSR.transform.localPosition;
				v.x -= imageWidth0*percentageBefore*0.5f;
				v.x += imageWidth0*percentage*0.5f;
				transform.localPosition = v;
				percentageBefore = percentage;	
			}
			
			
		}
		
	}

	public bool UseSeperateMinMax()
	{
		if (current.UseConstant == true)
			return true;

		bool useSeperateMinMax = true;
		
		if (current.variable is SmartFloatVar)
		{
			useSeperateMinMax = false;
		} else if (current.variable is SmartIntegerVar)
		{
			useSeperateMinMax = false;
		}

		return useSeperateMinMax;
	}
}