﻿using UnityEngine;
using Sirenix.OdinInspector;

public class UITable : MonoBehaviour
{
    public enum PlacementStyle
    {
        Vertical, Horizontal, Direction, Grid
    }

    public PlacementStyle placementStyle;
    [ShowIf("isPlacementSimple")]
    public float deltaStep;

    [HideIf("isPlacementSimple")] 
    public Vector2 deltaV;

    public bool isPlacementSimple()
    {
        return placementStyle == PlacementStyle.Horizontal || placementStyle == PlacementStyle.Vertical;    
    }

    [ShowIf("placementStyle", PlacementStyle.Grid)]
    public int width;
    
    
    public int slotsAmount;
    
    
    [AssetsOnly] 
    public UITableSlot slotPrefab;
    
    [SceneObjectsOnly]
    public GameObject[] sources = new GameObject[0];


    public bool sort;
    
    [ShowIf("sort")]
    public bool ascending;

    [ShowIf("sort")] 
    public NumberVariable sortParameter;
    
    void Awake()
    {
        if (sort)
            Sort();
        
        var RT = GetComponent<RectTransform>();
        
        Vector2 pos = RT.anchoredPosition;
        Vector2 pos0 = pos;
        
        for (int i = 0; i < slotsAmount; i++)
        {
            var slot = Instantiate(slotPrefab, transform);
            var rt = slot.GetComponent<RectTransform>();
            rt.anchoredPosition = pos;
            
            if (placementStyle == PlacementStyle.Vertical)
                pos.y -= deltaStep;
            else if (placementStyle == PlacementStyle.Horizontal) 
                pos.x += deltaStep;
            else if (placementStyle == PlacementStyle.Direction)
            {
                pos.x += deltaV.x;
                pos.y -= deltaV.y;
            }
                
            else 
            {
                // тут целая эпопея
                int x = i % width;
                int y = i / width;
                pos = pos0 + x * deltaV.x * Vector2.right + y * deltaV.y * Vector2.up;
            }
            
            
            if (sources.Length > i)
                slot.Compile(i, sources[i]);
            else slot.Compile(i, null);
        }
        
    }

    void Sort()
    {
        // @Unotimized: самый простой но и самый неэффективный метод сортировки. Зато делается он всего раз при запуске
        int len = sources.Length;
        bool[] used = new bool[len];
        int[] bestID = new int[len];

        int n = 0;

        for (int i = 0; i < len; i++)
        {
            float bestValue = !ascending ? -1 : 10000000000;
            int bestJ = -1;
            for (int j = 0; j < len; j++)
            {
                if (sources[j] == null) continue;
                
                var gm = sources[j].GetComponent<GenerateMe>();
                if (gm == null) continue;

                var clone = gm.GetClone(sortParameter);
                float value = (clone as NumberVariable).GetFloat();

                if (used[j] == false)
                {
                    if (!ascending)
                    {
                        if (value >= bestValue)
                        {
                            bestValue = value;
                            bestJ = j;
                        }
                        
                    } else
                    {
                        if (value <= bestValue)
                        {
                            bestValue = value;
                            bestJ = j;
                        }   
                    }
                    
                }
                    
            }

            used[bestJ] = true;
            bestID[i] = bestJ;
            n++;
        }

        GameObject[] newArr = new GameObject[n];
        for (int i = 0; i < n; i++)
        {
            newArr[i] = sources[bestID[i]];
        }

        
        if (newArr.Length < sources.Length)
            DLog.Error("Warning! Array is smaller after sort!");
        
        sources = newArr;

    }
    
}