﻿using UnityEngine;
using Sirenix.OdinInspector;

public class SingletonPlayerprefs : SingletonMono<SingletonPlayerprefs>
{
    public bool loadOnStart;
    
    public FloatVariable[] floatToPersist;
    public IntegerVariable[] intToPersist;
    public BooleanVariable[] boolToPersist;

    void Start()
    {
        if (loadOnStart)
            LoadData();
    }
    
    [Button]
    void ClearData()
    {
        PlayerPrefs.DeleteAll();

        for (int i = 0; i < floatToPersist.Length; i++)
            floatToPersist[i].Value = 0;

        for (int i = 0; i < intToPersist.Length; i++)
            intToPersist[i].Value = 0;
        
        for (int i = 0; i < boolToPersist.Length; i++)
            boolToPersist[i].Value = false;

    }
    [Button]
    public void LoadData()
    {
        LoadFloats(floatToPersist);
        LoadInts(intToPersist);
        LoadBools(boolToPersist);
    }
    [Button]
    public void SaveData()
    {
        SaveFloats(floatToPersist);
        SaveInts(intToPersist);
        SaveBools(boolToPersist);

        PlayerPrefs.Save();
    }

    void SaveFloats(FloatVariable[] fv)
    {
        for (int i = 0; i < fv.Length; i++)
        {
            PlayerPrefs.SetFloat(fv[i].name + "_floatPrefs", fv[i].Value);
        }
    }

    void SaveInts(IntegerVariable[] iv)
    {
        for (int i = 0; i < iv.Length; i++)
        {
            PlayerPrefs.SetInt(iv[i].name + "_intPrefs", iv[i].Value);
        }
    }

    void SaveBools(BooleanVariable[] bv)
    {
        for (int i = 0; i < bv.Length; i++)
        {
            PlayerPrefs.SetInt(bv[i].name + "_boolPrefs", bv[i].Value ? 1 : 0);
        }
    }

    
    void LoadFloats(FloatVariable[] fv)
    {
        for (int i = 0; i < fv.Length; i++)
        {
            if (PlayerPrefs.HasKey(fv[i].name + "_floatPrefs"))
                fv[i].Value = PlayerPrefs.GetFloat(fv[i].name + "_floatPrefs");
        }
    }

    void LoadInts(IntegerVariable[] iv)
    {
        for (int i = 0; i < iv.Length; i++)
        {
            if (PlayerPrefs.HasKey(iv[i].name + "_intPrefs"))
                iv[i].Value = PlayerPrefs.GetInt(iv[i].name + "_intPrefs");
        }
    }
    
    void LoadBools(BooleanVariable[] bv)
    {
        for (int i = 0; i < bv.Length; i++)
        {
            if (PlayerPrefs.HasKey(bv[i].name + "_boolPrefs"))
                bv[i].Value = PlayerPrefs.GetInt(bv[i].name + "_boolPrefs") == 1;
        }
    }

   
}

