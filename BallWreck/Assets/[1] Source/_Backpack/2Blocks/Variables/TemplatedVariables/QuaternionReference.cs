﻿using System;
using UnityEngine;

[Serializable]
public class QuaternionReference : TemplateReference<Quaternion>
{
	public QuaternionVariable variable;

	protected override Quaternion GetVariableValue()
	{
		return variable.Value;
	}

	protected override void SetVariableValue(Quaternion val)
	{
		variable.Value = val;
	}


	public override IGeneratable GetRef()
	{
		return variable;
	}
	
	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as QuaternionVariable;
	}
	
}
