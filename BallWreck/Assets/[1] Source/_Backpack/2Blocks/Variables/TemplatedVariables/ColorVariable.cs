﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Color Variable")]
public class ColorVariable : TemplateVariable<Color>
{

}
