﻿using System;

[Serializable]
public class StringReference : TemplateReference<string>
{
	public StringVariable variable;

	protected override string GetVariableValue()
	{
		return variable.Value;
	}

	protected override void SetVariableValue(string val)
	{
		variable.Value = val;
	}


	public override IGeneratable GetRef()
	{
		return variable;
	}
	
	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as StringVariable;
	}
}
