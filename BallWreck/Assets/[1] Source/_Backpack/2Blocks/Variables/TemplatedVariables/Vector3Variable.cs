﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Vector3 Variable")]
public class Vector3Variable : TemplateVariable<Vector3>
{
    
}
