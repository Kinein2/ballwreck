﻿using System;
using UnityEngine;

[Serializable]
public class Vector3Reference : TemplateReference<Vector3>
{
	public Vector3Variable variable;
	
	protected override Vector3 GetVariableValue()
	{
		return variable.Value;
	}

	protected override void SetVariableValue(Vector3 val)
	{
		variable.Value = val;
	}

    public override IGeneratable GetRef()
    {
        return variable;
    }
	
    public override void SetRef(IGeneratable newRef)
    {
        variable = newRef as Vector3Variable;
    }

    public float x
    {
        get
        { return Value.x; }

        set
        { Value = Value.SetX(value); }
    }

    public float y
    {
        get
        { return Value.y; }

        set
        { Value = Value.SetY(value); }
    }

    public float z
    {
        get
        { return Value.z; }

        set
        { Value = Value.SetZ(value); }
    }

    public float magnitude
    {
        get { return Value.magnitude; }
    }

    public Vector2 normalized
    {
        get { return Value.normalized; }
    }

    public void Normalize()
    {
        Value.Normalize();
    }
}
