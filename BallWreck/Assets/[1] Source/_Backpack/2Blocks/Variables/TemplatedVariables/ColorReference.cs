﻿using System;
using UnityEngine;

[Serializable]
public class ColorReference : TemplateReference<Color>
{
	public ColorVariable variable;
	
	protected override Color GetVariableValue()
	{
		return variable.Value;
	}

	protected override void SetVariableValue(Color val)
	{
		variable.Value = val;
	}
	
	
	public override IGeneratable GetRef()
	{
		return variable;
	}
	
	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as ColorVariable;
	}

	
}
