﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Boolean Variable")]
public class BooleanVariable : TemplateVariable<bool>
{
	
}
