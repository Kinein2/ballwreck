﻿using System;

[Serializable]
public class BooleanReference : TemplateReference<bool>
{
	public BooleanVariable variable;
	
	protected override bool GetVariableValue()
	{
		return variable.Value;
	}

	protected override void SetVariableValue(bool val)
	{
		variable.Value = val;
	}

	public override IGeneratable GetRef()
	{
		return variable;
	}
	
	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as BooleanVariable;
	}	
}
