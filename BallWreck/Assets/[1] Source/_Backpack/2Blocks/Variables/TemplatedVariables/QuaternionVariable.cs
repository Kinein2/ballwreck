﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Quaternion Variable")]
public class QuaternionVariable : TemplateVariable<Quaternion>
{
    
}
