﻿using System;
using UnityEngine;

[Serializable]
public class Vector2Reference : TemplateReference<Vector2>
{
	public Vector2Variable variable;
	
	protected override Vector2 GetVariableValue()
	{
		return variable.Value;
	}

	protected override void SetVariableValue(Vector2 val)
	{
		variable.Value = val;
	}

	public override IGeneratable GetRef()
	{
		return variable;
	}
	
	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as Vector2Variable;
		variable.Value = constantValue;
	}
	
    public float x
    {
        get
        { return Value.x; }

        set
        { Value = Value.SetX(value); }
    }

    public float y
    {
        get
        { return Value.y; }

        set
        { Value = Value.SetY(value); }
    }


    public float magnitude
    {
        get { return Value.magnitude; }
    }

    public Vector2 normalized
    {
        get { return Value.normalized; }
    }

    public void Normalize()
    {
        Value.Normalize();
    }

}
