﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Vector2 Variable")]
public class Vector2Variable : TemplateVariable<Vector2>
{
    
}
