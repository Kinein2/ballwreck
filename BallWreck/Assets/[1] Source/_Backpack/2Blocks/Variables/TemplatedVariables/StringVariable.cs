﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/String Variable")]
public class StringVariable : TemplateVariable<string>
{
	
}
