﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Approximators/Float Approx Variable")]
public class ApproximatorFloat : FloatVariable {
	public FloatReference a, b, t;
	public AnimationCurve ac;

	protected override void Enabling()
	{
		//t.SetOnChange(UpdateApproximator);
		t.onChange -= UpdateApproximator;
		t.onChange += UpdateApproximator;

		if (t.variable != null)
		{
			t.variable.onChange -= UpdateApproximator;
			t.variable.onChange += UpdateApproximator;
		}
	}

	void UpdateApproximator()
	{
		float tc = ac.Evaluate(t.Value);
		Value = Mathf.Lerp(a, b, tc);
	}
}
