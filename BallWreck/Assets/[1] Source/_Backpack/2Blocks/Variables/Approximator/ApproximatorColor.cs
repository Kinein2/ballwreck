﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Approximators/Color Approx Variable")]
public class ApproximatorColor : ColorVariable {
	public ColorReference a, b;
	public FloatReference t;
	public AnimationCurve ac;

	protected override void Enabling()
	{
		//t.SetOnChange(UpdateApproximator);
		t.onChange -= UpdateApproximator;
		t.onChange += UpdateApproximator;

		if (t.variable != null)
		{
			t.variable.onChange -= UpdateApproximator;
			t.variable.onChange += UpdateApproximator;
		}
	}

	void UpdateApproximator()
	{
		float tc = ac.Evaluate(t.Value);
		Value = Color.Lerp(a, b, tc);
	}
}
