﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Smart IntegerVar")]
public class SmartIntegerVar : IntegerVariable {

	public IntegerReference maxValue, minValue;
	public SimpleTrigger onMaxHit, onMinHit;

	public override void SetValue(int _Value)
	{
		if (_Value > maxValue)
		{
			if (onMaxHit)
			{
				onMaxHit.Raise(null);
			}
			
			value = maxValue;
		}
		else if (_Value <= minValue)
		{
			if (onMinHit)
			{
				onMinHit.Raise(null);
			}
			
			value = minValue;
		} else base.SetValue(_Value);

		if (onChange != null)
			onChange();
	}

	public float GetPercentage()
	{
		return ((float)(value - minValue)) / (maxValue - minValue);
	}
}
