﻿using System;

[Serializable]
public class NumberReference : VariableReference 
{

	public float constantValue;

	public NumberVariable variable;
	
	public NumberReference()
	{ }

	public NumberReference(float value)
	{
		useMode = UseMode.useConstant;
		constantValue = value;
	}

	protected float GetVariableValue()
	{
		return variable.GetFloat();
	}

	protected void SetVariableValue(float val)
	{
		variable.Set(val);
	}
	
	public float Value
	{
		get { return UseConstant ? constantValue : GetVariableValue(); }
		set {
			if (UseConstant)
				constantValue = value;
			else {
				SetVariableValue(value);
			}
		}
	}

	public static implicit operator float(NumberReference reference)
	{
		return reference.Value;
	}

	public static implicit operator int(NumberReference reference)
	{
		return (int)reference.Value;
	}

	public override IGeneratable GetRef()
	{
		return variable;
	}

	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as NumberVariable;
	}

	public override void SetData(IGeneratableSlot igs)
	{
		constantValue = (igs as NumberReference).constantValue;
	}
}
