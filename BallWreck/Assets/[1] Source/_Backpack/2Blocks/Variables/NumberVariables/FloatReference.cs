﻿using System;

[Serializable]
public class FloatReference : TemplateReference<float>
{
	public FloatVariable variable;

    
    
    protected override float GetVariableValue()
	{
		return variable.Value;
	}

	protected override void SetVariableValue(float val)
	{
		variable.Value = val;
	}


	public override IGeneratable GetRef()
	{
		return variable;
	}

	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as FloatVariable;
	}
}
