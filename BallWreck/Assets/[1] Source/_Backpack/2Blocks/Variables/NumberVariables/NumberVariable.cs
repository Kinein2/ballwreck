﻿

public abstract class NumberVariable : ScriptObj_Variable
{
	public abstract float GetFloat();
	public abstract int GetInt();

	public abstract void Set(float f);
	public abstract void Set(int i);
}
