﻿using System;

[Serializable]
public class IntegerReference : TemplateReference<int>
{
	public IntegerVariable variable;
	
	protected override int GetVariableValue()
	{
		return variable.Value;
	}

	protected override void SetVariableValue(int val)
	{
		variable.Value = val;
	}

	
	public static implicit operator IntegerReference(int f)
	{
		var FR = new IntegerReference();
		FR.Value = f;
		return FR;
	}
	
	
	public override IGeneratable GetRef()
	{
		return variable;
	}

	public override void SetRef(IGeneratable newRef)
	{
		variable = newRef as IntegerVariable;
	}
}
