﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Smart FloatVar")]
public class SmartFloatVar : FloatVariable {

	public FloatReference maxValue, minValue;
	public SimpleTrigger onMaxHit, onMinHit;

	public override void SetValue(float _Value)
	{
		if (_Value > maxValue)
		{
			if (onMaxHit)
			{
				onMaxHit.Raise(null);
			} else value = maxValue;
		}
		else if (_Value < minValue)
		{
			if (onMinHit)
			{
				onMinHit.Raise(null);
			} else value = minValue;
		} else base.SetValue(_Value);

		if (onChange != null)
			onChange();
	}

	public float GetPercentage()
	{
		return (value - minValue) / (maxValue - minValue);
	}
	
}
