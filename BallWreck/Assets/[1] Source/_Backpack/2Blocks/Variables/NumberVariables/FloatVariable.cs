﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Float Variable")]
public class FloatVariable : NumberVariable
{
	[SerializeField, HideInInspector]
	float _defaultValue;

	[SerializeField]
	protected float value;

	public bool noDefault;
	
	public float Value
	{
		get { return GetValue(); }
		set { SetValue(value); }
	}

    protected virtual float GetValue()
    {
        return value;
    }

	public virtual void SetValue(float val)
	{
		value = val;
		if (onChange != null)
			onChange();
	}

	public override void SaveDefault()
	{
		if (noDefault == false)
			_defaultValue = value;		
	}

	public override void SetToDefault(bool editorchange)
	{
		if (noDefault == false)
			value = _defaultValue;
	}


	public override float GetFloat()
	{ return Value; }

	public override int GetInt()
	{ return (int)Value; }

	public override void Set(float f)
	{ Value = f; }

	public override void Set(int i)
	{ Value = i; }

	
}
