﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Integer Variable")]
public class IntegerVariable : NumberVariable
{
	[SerializeField, HideInInspector]
	int _defaultValue;

	[SerializeField]
	protected int value;

	public bool noDefault;
	
	public int Value
	{
		get { return value; }
		set { SetValue(value); }
	}

	public virtual void SetValue(int val)
	{
		value = val;
		if (onChange != null)
			onChange();
	}

	public override void SaveDefault()
	{
		if (noDefault == false)
			_defaultValue = value;	
	}

	public override void SetToDefault(bool editorchange)
	{
		if (noDefault == false)
			Value = _defaultValue;
	}

	
	public override float GetFloat()
	{ return Value; }

	public override int GetInt()
	{ return Value; }

	public override void Set(float f)
	{ Value = (int)f; }

	public override void Set(int i)
	{ Value = i; }

}
