﻿
public class InstantTemplate<T>
{
	public delegate T ModifierFunc(T inputValue);
	public delegate T SourceFunc();

	ModifierFunc[] _mfs = new ModifierFunc[0];
	SourceFunc source;

	public void AddModFunction(ModifierFunc f)
	{
		_mfs = _mfs.Add(f);
	}

	public void RemoveModFunction(ModifierFunc f)
	{
		_mfs = _mfs.Remove(f);
	}

	public void SetSource(SourceFunc s)
	{
		source = s;
	}

	public T GetValue()
	{
		T val = default(T);
		if (source != null)
			val = source();
		
		
		for (int i = 0; i < _mfs.Length; i++)
		{
			val = _mfs[i](val);
		}

		return val;
	}

	public bool isUsed()
	{
		return source != null || _mfs.Length > 0;
	}
}
