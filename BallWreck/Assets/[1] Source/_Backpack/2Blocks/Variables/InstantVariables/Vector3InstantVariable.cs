﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Instant/Vector3")]
public class Vector3InstantVariable : Vector3Variable
{
	public InstantTemplate<Vector3> instant = new InstantTemplate<Vector3>();

	public override Vector3 GetValue()
	{
		if (instant.isUsed())
			return instant.GetValue();
		return base.GetValue();
	}
}