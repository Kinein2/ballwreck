﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Instant/Color")]
public class ColorInstantVariable : ColorVariable
{
	public InstantTemplate<Color> instant = new InstantTemplate<Color>();

	public override Color GetValue()
	{
		if (instant.isUsed())
		{
			value = instant.GetValue();
		}
		
		return base.GetValue();
	}
	
}