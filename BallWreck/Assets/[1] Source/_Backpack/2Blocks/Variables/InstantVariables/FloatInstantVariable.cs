﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Instant/Float")]
public class FloatInstantVariable : FloatVariable
{
	public InstantTemplate<float> instant = new InstantTemplate<float>();

    

	protected override float GetValue()
	{
		if (instant.isUsed())
			return instant.GetValue();
		return base.GetValue();
	}
}