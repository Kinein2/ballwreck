﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Instant/Vector2")]
public class Vector2InstantVariable : Vector2Variable
{
	public InstantTemplate<Vector2> instant = new InstantTemplate<Vector2>();

	public override Vector2 GetValue()
	{
		if (instant.isUsed())
		{
			value = instant.GetValue();
		}
		
		return base.GetValue();
	}
	
}