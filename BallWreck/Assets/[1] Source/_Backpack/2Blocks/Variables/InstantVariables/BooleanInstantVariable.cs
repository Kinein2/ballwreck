﻿using UnityEngine;

[CreateAssetMenu(menuName = "BackpackVariable/Instant/Boolean")]
public class BooleanInstantVariable : BooleanVariable
{
	public InstantTemplate<bool> instant = new InstantTemplate<bool>();

	public override bool GetValue()
	{
		if (instant.isUsed())
			return instant.GetValue();
		return base.GetValue();
	}
}