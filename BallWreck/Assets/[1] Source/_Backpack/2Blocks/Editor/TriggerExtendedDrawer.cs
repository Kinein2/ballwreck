﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

 // @Incomplete: не покажет тип, если это кастомный класс триггера (не наследуемый от скриптабл обжекта)

[CustomPropertyDrawer(typeof(TriggerExtended), false)]
public class TriggerExtendedDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        OnGUIDrawer(position, property, label);
    }


    public static void OnGUIDrawer(Rect position, SerializedProperty property, GUIContent label)
    {
        label = EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, label);
        
        EditorGUI.BeginChangeCheck();

        // Store old indent level and set it to 0, the PrefixLabel takes care of it
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Color c = Lamp.neutral;
        
        var trig = property.objectReferenceValue as TriggerExtended;
        var mono = (property.serializedObject.targetObject as MonoBehaviour);
       
        if (trig != null)
            if (mono.enabled == true && mono.gameObject.activeInHierarchy == true)
            {
                if (trig.subscribedMonos != null && trig.subscribedMonos.IndexOf(mono) == -1)
                {
                    if (trig.AuthorMono == mono)
                        c = trig.GetLampColor();
                }
                else c = trig.GetLampColor();    
            }
            

        
        TriggerExtendedEditor.DrawLamp(ref position, position.height * 0.8f,  c);
        
        var propType = GetPropertyObjectType(property);
        property.objectReferenceValue = EditorGUI.ObjectField(position, "", property.objectReferenceValue, propType, false);

        
        



        if (EditorGUI.EndChangeCheck())
        {
            property.serializedObject.ApplyModifiedProperties();
        }

       

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
    
    public static string GetPropertyType(SerializedProperty property)
    {
        var type = property.type;
        var match = Regex.Match(type, @"PPtr<\$(.*?)>");
        if (match.Success)
            type = match.Groups[1].Value;
        return type;
    }

    public static Type GetPropertyObjectType(SerializedProperty property)
    {
        return System.Type.GetType(GetPropertyType(property) + ", BackpackBlocks");                                   
    }    

}