﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(SingletonTimer.Timer), true)]
public class SingletonTimerTimerDrawer : PropertyDrawer {
	
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, null, property);
		var time = property.FindPropertyRelative("time");
		var timer = property.FindPropertyRelative("timer");

		var t = time.floatValue;
		var tr = timer.floatValue;

		float f = tr/t;
		int fracPart = (int)(tr.Fraction()*100);

		string text = tr.Whole().ToString() + "." + fracPart.ToString() + " / " + t.ToString();

		var enbl = property.FindPropertyRelative("enabled");
		var loop = property.FindPropertyRelative("loop");
		var fnshd = property.FindPropertyRelative("finished");
		var ownerName = property.FindPropertyRelative("ownerName");

		var defaultColor = GUI.color;

		if (enbl.boolValue == false)
			GUI.color = Color.black;
		else if (fnshd.boolValue)
			GUI.color = Color.grey;
		else if (loop.boolValue)
			GUI.color = Color.magenta*0.8f;
		
		var npos = position;

		if (SingletonTimer.Timer.showName)
			npos = EditorGUI.PrefixLabel(position, new GUIContent(ownerName.stringValue));

		EditorGUI.ProgressBar(npos, f, text);
		GUI.color = defaultColor;

		EditorGUI.EndProperty();

		// Debug.Log(timer);
	}
}
