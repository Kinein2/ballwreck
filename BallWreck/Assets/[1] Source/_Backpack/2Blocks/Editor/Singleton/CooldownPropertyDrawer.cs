﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Cooldown), true)]
public class CooldownPropertyEditor : PropertyDrawer {

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, null, property);	
		var time = property.FindPropertyRelative("time");

		if (Application.isPlaying)
		{
			var rdy = property.FindPropertyRelative("_ready");	
			var timer = property.FindPropertyRelative("_timer");	
			
			
			SingletonTimer.Timer.showName = false;
			var nrect = EditorGUI.PrefixLabel(position, new GUIContent(property.name));

			
			if (rdy.boolValue == false && timer != null)
				EditorGUI.PropertyField(nrect, timer);
			else
			{
				EditorGUI.ProgressBar(nrect, 0, "0.00 / " + time.floatValue.ToString());
			}

			SingletonTimer.Timer.showName = true;
		} else
		{
			var nrect = EditorGUI.PrefixLabel(position, new GUIContent(property.name));
			
			time.floatValue = EditorGUI.FloatField(nrect, time.floatValue);
		}

		EditorGUI.EndProperty();
		//float f = EditorGUILayout.FloatField(6);
		
		// Debug.Log(timer);
	}

}
