﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

using Object = UnityEngine.Object;


public class InspectorLockToggle
{
   
    
    private const string ITEM_NAME = "Tools/BackpackHotkeys/Toggle Inspector Debug &k";

    [MenuItem( ITEM_NAME )]
    private static void Toggle()
    {
        var window = Resources.FindObjectsOfTypeAll<EditorWindow>();
        var inspectorWindow = ArrayUtility.Find( window, c => c.GetType().Name == "InspectorWindow" );

        if ( inspectorWindow == null ) return;

        var inspectorType = inspectorWindow.GetType();
        var tracker = ActiveEditorTracker.sharedTracker;
        var isNormal = tracker.inspectorMode == InspectorMode.Normal;
        var methodName = isNormal ? "SetDebug" : "SetNormal";

        var attr = BindingFlags.NonPublic | BindingFlags.Instance;
        var methodInfo = inspectorType.GetMethod( methodName, attr );
        methodInfo.Invoke( inspectorWindow, null );
        tracker.ForceRebuild();
    }

    [MenuItem( ITEM_NAME, true )]
    private static bool CanToggle()
    {
        var window = Resources.FindObjectsOfTypeAll<EditorWindow>();
        var inspectorWindow = ArrayUtility.Find( window, c => c.GetType().Name == "InspectorWindow" );

        return inspectorWindow != null;
    }
    
    private static EditorWindow _mouseOverWindow;
    /*
    [MenuItem("Stuff/Select Inspector under mouse cursor (use hotkey) #&q")]
    static void SelectLockableInspector()
    {
        if (EditorWindow.mouseOverWindow.GetType().Name == "InspectorWindow")
        {
            _mouseOverWindow = EditorWindow.mouseOverWindow;
            Type type = Assembly.GetAssembly(typeof(Editor)).GetType("UnityEditor.InspectorWindow");
            Object[] findObjectsOfTypeAll = Resources.FindObjectsOfTypeAll(type);
            int indexOf = findObjectsOfTypeAll.ToList().IndexOf(_mouseOverWindow);
            EditorPrefs.SetInt("LockableInspectorIndex", indexOf);
        }
    }
    */
 
    //  % (ctrl on Windows, cmd on macOS), # (shift), & (alt)
    
    [MenuItem("Tools/BackpackHotkeys/Toggle Lock &q")]
    static void ToggleInspectorLock()
    {
        DLog.Log("Toggle lock hotkey registered!", DLog.Level.UltraVerbose, "Backpack");
        
        if (EditorWindow.mouseOverWindow.GetType().Name == "InspectorWindow")
        {
            _mouseOverWindow = EditorWindow.mouseOverWindow;
            Type type = Assembly.GetAssembly(typeof(Editor)).GetType("UnityEditor.InspectorWindow");
            Object[] findObjectsOfTypeAll = Resources.FindObjectsOfTypeAll(type);
            int indexOf = findObjectsOfTypeAll.ToList().IndexOf(_mouseOverWindow);
            EditorPrefs.SetInt("LockableInspectorIndex", indexOf);
            
        }
        
        if (_mouseOverWindow == null)
        {
            if (!EditorPrefs.HasKey("LockableInspectorIndex"))
                EditorPrefs.SetInt("LockableInspectorIndex", 0);
            int i = EditorPrefs.GetInt("LockableInspectorIndex");
 
            Type type = Assembly.GetAssembly(typeof(Editor)).GetType("UnityEditor.InspectorWindow");
            Object[] findObjectsOfTypeAll = Resources.FindObjectsOfTypeAll(type);
            _mouseOverWindow = (EditorWindow)findObjectsOfTypeAll[i];
        }
 
        if (_mouseOverWindow != null && _mouseOverWindow.GetType().Name == "InspectorWindow")
        {
            Type type = Assembly.GetAssembly(typeof(Editor)).GetType("UnityEditor.InspectorWindow");
            PropertyInfo propertyInfo = type.GetProperty("isLocked");
            bool value = (bool)propertyInfo.GetValue(_mouseOverWindow, null);
            propertyInfo.SetValue(_mouseOverWindow, !value, null);
            _mouseOverWindow.Repaint();
            
        }
        
        
    }
 
    [MenuItem("Tools/BackpackHotkeys/Clear Console #&c")]
    static void ClearConsole()
    {
        DLog.Log("Clear console hotkey registered!", DLog.Level.Verbose, "Backpack");
        
        Type type = Assembly.GetAssembly(typeof(Editor)).GetType("UnityEditorInternal.LogEntries");
        type.GetMethod("Clear").Invoke(null,null);
    }

}