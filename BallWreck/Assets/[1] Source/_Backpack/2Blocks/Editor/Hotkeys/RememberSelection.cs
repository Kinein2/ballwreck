﻿using System.IO;

using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "Hotkeys")]
public class RememberSelection : ScriptableObject
{
    
    public Object[] SavedObjs = new Object[10];
    public string[] savedPaths = new string[10];
                                               
    
    
    static RememberSelection _instance = null;
    public static RememberSelection Instance
    {
        get
        {

            if (!_instance)
            {
                _instance = Resources.Load<RememberSelection>("HotkeysSO");
                //_instance = Resources.FindObjectsOfTypeAll<RememberSelection>().FirstOrDefault();
            }
                
            return _instance;
        }
    }
    
    
                                                                                            
    
    
    
    
    bool Restore(int n)
    {
        if (savedPaths[n] != null && savedPaths[n].Length > 0)
        {
            SavedObjs[n] = GameObject.Find(savedPaths[n]);
            
            return true;
        }
                 
        return false;
    }
    
    public void SaveLogic(int n)
    {
        var objs = Selection.objects;
        if (objs.Length != 1) return;


        SavedObjs[n] = objs[0];
        savedPaths[n] = "";
        if (objs[0] is GameObject)
        {
            var go = objs[0] as GameObject;
            if (AssetDatabase.GetAssetPath(SavedObjs[n]).Length == 0)
                savedPaths[n] = go.transform.GetPath(); 
        }
        
        
        
        EditorUtility.SetDirty(this);
        //AssetDatabase.SaveAssets();
    }

    public void SelectLogic(int n)
    {
        if (SavedObjs[n] == null)
        {
            if (Restore(n) == false)
                return;
        }
                             
        string path = AssetDatabase.GetAssetPath(SavedObjs[n]);
        
       if (Selection.objects != null)
       if (Selection.objects.Length == 1)
           if (Selection.objects[0] == SavedObjs[n])
               if (path.Length > 0)
           {
               string a = GetActiveFolderPath();
               string b = Path.GetDirectoryName(path);
               string bnew = "";
               for (int i = 0; i < b.Length; i++)
               {
                   if (b[i] == '\\')
                       bnew += '/';
                   else bnew += b[i];
               }
               if (a == bnew)
                   OpenLogic(path, SavedObjs[n]);           
           }
       
        
        
       

        if (path.Length > 0)
            SetPathInProject(path);

        Selection.objects = new Object[1];
        Selection.objects = Selection.objects.Add(SavedObjs[n]);
        
    }

    public void OpenLogic(string path, Object obj)
    {
        AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath(path, obj.GetType()));
    }

    string GetActiveFolderPath()
    {
        System.Type projectBrowserType = System.Type.GetType("UnityEditor.ProjectBrowser,UnityEditor");
        if (projectBrowserType != null)
        {
            FieldInfo lastProjectBrowser = projectBrowserType.GetField("s_LastInteractedProjectBrowser", BindingFlags.Static | BindingFlags.Public);
            if (lastProjectBrowser != null)
            {
                object lastProjectBrowserInstance = lastProjectBrowser.GetValue(null);
                FieldInfo projectBrowserViewMode = projectBrowserType.GetField("m_ViewMode", BindingFlags.Instance | BindingFlags.NonPublic);
                if (projectBrowserViewMode != null)
                {
                    // 0 - one column, 1 - two column
                    MethodInfo showFolderContents = projectBrowserType.GetMethod("GetActiveFolderPath", BindingFlags.NonPublic | BindingFlags.Instance);
                    return showFolderContents.Invoke(lastProjectBrowserInstance, null) as string;
                }
                else
                    Debug.LogError("Can't find m_ViewMode field!");
            }
            else
                Debug.LogError("Can't find s_LastInteractedProjectBrowser field!");
        }
        else
            Debug.LogError("Can't find UnityEditor.ProjectBrowser type!");

        return "";
    }
    
    void SetPathInProject(string assetPath)
    {
        System.Type projectBrowserType = System.Type.GetType("UnityEditor.ProjectBrowser,UnityEditor");
        if (projectBrowserType != null)
        {
            FieldInfo lastProjectBrowser = projectBrowserType.GetField("s_LastInteractedProjectBrowser", BindingFlags.Static | BindingFlags.Public);
            if (lastProjectBrowser != null)
            {
                object lastProjectBrowserInstance = lastProjectBrowser.GetValue(null);
                FieldInfo projectBrowserViewMode = projectBrowserType.GetField("m_ViewMode", BindingFlags.Instance | BindingFlags.NonPublic);
                if (projectBrowserViewMode != null)
                {
                    // 0 - one column, 1 - two column
                    int viewMode = (int)projectBrowserViewMode.GetValue(lastProjectBrowserInstance);
                    if (viewMode == 1)
                    {
                         
                        MethodInfo showFolderContents = projectBrowserType.GetMethod("ShowFolderContents", BindingFlags.NonPublic | BindingFlags.Instance);
                        if (showFolderContents != null)
                        {
                            Object sceneFolder = AssetDatabase.LoadAssetAtPath(Path.GetDirectoryName(assetPath), typeof(Object));
                            showFolderContents.Invoke(lastProjectBrowserInstance, new object[] { sceneFolder.GetInstanceID(),  true});
                        }
                        else
                            Debug.LogError("Can't find ShowFolderContents method!");
                    }
                }
                else
                    Debug.LogError("Can't find m_ViewMode field!");
            }
            else
                Debug.LogError("Can't find s_LastInteractedProjectBrowser field!");
        }
        else
            Debug.LogError("Can't find UnityEditor.ProjectBrowser type!");
    }
    
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select1 _1")]
    static void Select1()
    {
        Instance.SelectLogic(0);             
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Save1 #1")]
    static void Save1()
    {
        Instance.SaveLogic(0);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select2 _2")]
    static void Select2()
    {
        Instance.SelectLogic(1);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Save2 #2")]
    static void Save2()
    {
        Instance.SaveLogic(1);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select3 _3")]
    static void Select3()
    {
        Instance.SelectLogic(2);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Save3 #3")]
    static void Save3()
    {
        Instance.SaveLogic(2);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select4 _4")]
    static void Select4()
    {
        Instance.SelectLogic(3);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Save4 #4")]
    static void Save4()
    {
        Instance.SaveLogic(3);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select5 _5")]
    static void Select5()
    {
        Instance.SelectLogic(4);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Save5 #5")]
    static void Save5()
    {
        Instance.SaveLogic(4);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select6 _6")]
    static void Select6()
    {
        Instance.SelectLogic(5);
    }
                                                                       
    [MenuItem("Tools/BackpackHotkeys/Remember/Save6 #6")]
    static void Save6()
    {
        Instance.SaveLogic(5);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select7 _7")]
    static void Select7()
    {
        Instance.SelectLogic(6);
    }
                                                                       
    [MenuItem("Tools/BackpackHotkeys/Remember/Save7 #7")]
    static void Save7()
    {
        Instance.SaveLogic(6);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select8 _8")]
    static void Select8()
    {
        Instance.SelectLogic(7);
    }
                                                                       
    [MenuItem("Tools/BackpackHotkeys/Remember/Save8 #8")]
    static void Save8()
    {
        Instance.SaveLogic(7);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select9 _9")]
    static void Select9()
    {
        Instance.SelectLogic(8);
    }
                                                                       
    [MenuItem("Tools/BackpackHotkeys/Remember/Save9 #9")]
    static void Save9()
    {
        Instance.SaveLogic(8);
    }
    
    [MenuItem("Tools/BackpackHotkeys/Remember/Select10 _0")]
    static void Select10()
    {
        Instance.SelectLogic(9);
    }
                                                                       
    [MenuItem("Tools/BackpackHotkeys/Remember/Save10 #0")]
    static void Save10()
    {
        Instance.SaveLogic(9);
    }
    
}