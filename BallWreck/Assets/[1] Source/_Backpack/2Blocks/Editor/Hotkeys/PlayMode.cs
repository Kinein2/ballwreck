﻿using UnityEditor;

public class PlayMode {
    [MenuItem("Tools/BackpackHotkeys/Run _F5")] 
    static void PlayGame() 
    {
        if (EditorApplication.isPaused == false)
            EditorApplication.ExecuteMenuItem("Edit/Play");
        else EditorApplication.Step();
            
    }
        
    [MenuItem("Tools/BackpackHotkeys/PauseHotkey _F4")] 
    static void PauseGame() {
        EditorApplication.ExecuteMenuItem("Edit/Pause");
    }
}