
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/*
public class CameraSwitcher : MonoBehaviour
{
 
   static Quaternion sPerspectiveRotation = Quaternion.Euler(0, 0, 0);

  
   static bool sShouldTween = true;


   
   static private void StorePerspective()
   {
       if (SceneView.lastActiveSceneView.orthographic == false)
       {
           sPerspectiveRotation = SceneView.lastActiveSceneView.rotation;
       }
   }

  
   static private void ApplyOrthoRotation(Quaternion newRotation)
   {
       StorePerspective();

       SceneView.lastActiveSceneView.orthographic = true;

       if (sShouldTween)
       {
           SceneView.lastActiveSceneView.LookAt(SceneView.lastActiveSceneView.pivot, newRotation);
       }
       else
       {
           SceneView.lastActiveSceneView.LookAtDirect(SceneView.lastActiveSceneView.pivot, newRotation);
       }

       SceneView.lastActiveSceneView.Repaint();
   }


   [MenuItem("Camera/Top #1")]
   static void TopCamera()
   {
       ApplyOrthoRotation(Quaternion.Euler(90, 0, 0));
   }


   [MenuItem("Camera/Bottom #2")]
   static void BottomCamera()
   {
       ApplyOrthoRotation(Quaternion.Euler(-90, 0, 0));
   }


   [MenuItem("Camera/Left #3")]
   static void LeftCamera()
   {
       ApplyOrthoRotation(Quaternion.Euler(0, 90, 0));
   }


   [MenuItem("Camera/Right #4")]
   static void RightCamera()
   {
       ApplyOrthoRotation(Quaternion.Euler(0, -90, 0));
   }


   [MenuItem("Camera/Front #5")]
   static void FrontCamera()
   {
       ApplyOrthoRotation(Quaternion.Euler(0, 0, 0));
   }


   static void BackCamera()
   {
       ApplyOrthoRotation(Quaternion.Euler(0, 180, 0));
   }
             
    [MenuItem("Camera/Back #6")]
    static void FUU()
    {

        var objs = Selection.objects;
        int n = 686;
    }

   [MenuItem("Camera/Persp Camera #7")]
   static void PerspCamera()
   {
       if (SceneView.lastActiveSceneView.camera.orthographic == true)
       {
           if (sShouldTween)
           {
               SceneView.lastActiveSceneView.LookAt(SceneView.lastActiveSceneView.pivot, sPerspectiveRotation);
           }
           else
           {
               SceneView.lastActiveSceneView.LookAtDirect(SceneView.lastActiveSceneView.pivot, sPerspectiveRotation);
           }

           SceneView.lastActiveSceneView.orthographic = false;

           SceneView.lastActiveSceneView.Repaint();
       }
   }
}
 */