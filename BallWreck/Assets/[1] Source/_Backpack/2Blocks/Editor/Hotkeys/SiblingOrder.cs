﻿// Done by Kogane, Edited by kinein
// TODO:
// CTRL Z
// MOVING OF SEVERAL OBJECTS

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public static class SiblingOrder
{
	const string ITEM_NAME_UP = "Tools/BackpackHotkeys/Sibiling Up %#&O";
	const string ITEM_NAME_DOWN = "Tools/BackpackHotkeys/Sibiling Down %#&L";
	const string ITEM_NAME_UPLEVEL = "Tools/BackpackHotkeys/Sibiling Up level %#&J";
	const string ITEM_NAME_DOWNLEVEL = "Tools/BackpackHotkeys/Sibiling Child of next %#&K";

	[MenuItem(ITEM_NAME_UP)]
	static void Up()
	{
		var t = Selection.activeTransform;
		if (t.GetSiblingIndex() > 0)
			t.SetSiblingIndex(t.GetSiblingIndex() - 1);
		else UpLevel();
	}

	[MenuItem(ITEM_NAME_DOWN)]
	static void Down()
	{
		var t = Selection.activeTransform;
		t.SetSiblingIndex(t.GetSiblingIndex() + 1);
	}

	[MenuItem(ITEM_NAME_UPLEVEL)]
	static void UpLevel()
	{
		var t = Selection.activeTransform;


		if (t.transform.parent != null)
		{
			int sibl = t.parent.GetSiblingIndex();
			t.parent = t.parent.parent;
			t.SetSiblingIndex(sibl);
		}

	}

	static Transform FindRootSibling(int sibling)
	{
		var allT = Resources.FindObjectsOfTypeAll<Transform>();
		var activeScene = SceneManager.GetActiveScene();
		foreach (var t in allT)
		{
			if (t.gameObject.scene != activeScene) continue;
			if (t.parent != null) continue;

			if (t.GetSiblingIndex() == sibling)
				return t;
		}

		return null;
	}

	[MenuItem(ITEM_NAME_DOWNLEVEL)]
	static void DownLevel()
	{
		var t = Selection.activeTransform;
		int sibl = t.GetSiblingIndex();

		Transform tt;

		if (t.parent != null)
			tt = t.parent.GetChild(sibl + 1);
		else tt = FindRootSibling(sibl + 1);

		if (tt == null)
			return;

		t.parent = tt;
		t.SetSiblingIndex(0);
	}


	[MenuItem(ITEM_NAME_UP, true)]
	static bool CanUp()
	{
		return Selection.activeTransform != null;
	}

	[MenuItem(ITEM_NAME_DOWN, true)]
	static bool CanDown()
	{
		return Selection.activeTransform != null;
	}

	[MenuItem(ITEM_NAME_UPLEVEL, true)]
	static bool CanUpLevel()
	{
		return Selection.activeTransform != null;
	}

	[MenuItem(ITEM_NAME_DOWNLEVEL, true)]
	static bool CanDownLevel()
	{
		return Selection.activeTransform != null;
	}

	[MenuItem("GameObject/Make first child", false, -5)]
	static void makeFirstChild(MenuCommand command)
	{
		var go = command.context as GameObject;
		if (go.transform.parent)
		{
			Undo.RegisterFullObjectHierarchyUndo(go.transform.parent, "Make first child");
			go.transform.SetAsFirstSibling();
		}
		else
		{
			go.transform.SetSiblingIndex(0);
		}
	}
}