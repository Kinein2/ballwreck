﻿using UnityEditor;
using UnityEngine;

public static class ComponentBindings
{
    private const string ITEM_NAME = "Tools/BackpackHotkeys/Invert Active &a";
    private const string ITEM_NAME_COPY = "Tools/BackpackHotkeys/Copy Transform Values &c";
    private const string ITEM_NAME_PASTE = "Tools/BackpackHotkeys/Paste Transform Values &v";

    private class Data
    {
        public Vector3 m_localPosition;
        public Quaternion m_localRotation;
        public Vector3 m_localScale;

        public Data( Vector3 localPosition, Quaternion localRotation, Vector3 localScale )
        {
            m_localPosition = localPosition;
            m_localRotation = localRotation;
            m_localScale = localScale;
        }

        public Data( Transform t ) : this( t.localPosition, t.localRotation, t.localScale )
        {
        }
    }

    
    private static Data m_data;

    [MenuItem( ITEM_NAME_COPY )]
    public static void Copy()
    {
        m_data = new Data( Selection.activeTransform );
    }

    [MenuItem( ITEM_NAME_COPY, true )]
    public static bool CanCopy()
    {
        return Selection.activeTransform != null;
    }

    [MenuItem( ITEM_NAME_PASTE )]
    public static void Paste()
    {
        foreach ( var n in Selection.gameObjects )
        {
            var t = n.transform;
            Undo.RecordObject( t, "Paste Transform Values" );
            t.localPosition = m_data.m_localPosition;
            t.localRotation = m_data.m_localRotation;
            t.localScale = m_data.m_localScale;
        }
    }

    [MenuItem( ITEM_NAME_PASTE, true )]
    public static bool CanPaste()
    {
        var gameObjects = Selection.gameObjects;
        return m_data != null && gameObjects != null && 0 < gameObjects.Length;
    }
    
    [MenuItem("CONTEXT/Component/Move to bottom")]
    static void MoveToBottom(MenuCommand command) {
        for (int i = 0; i < 100; i++) {
            UnityEditorInternal.ComponentUtility.MoveComponentDown(command.context as Component);
        }
    }
    
    [MenuItem("CONTEXT/Component/Move to top")]
    static void MoveToTop(MenuCommand command) {
        for (int i = 0; i < 100; i++) {
            UnityEditorInternal.ComponentUtility.MoveComponentUp(command.context as Component);
        }
    }
    
    [MenuItem( ITEM_NAME )]
    public static void Invert()
    {
        EditorApplication.ExecuteMenuItem( "GameObject/Toggle Active State" );
    }

    [MenuItem( ITEM_NAME, true )]
    public static bool CanInvert()
    {
        var gameObjects = Selection.gameObjects;
        return gameObjects != null && 0 < gameObjects.Length;
    }
}