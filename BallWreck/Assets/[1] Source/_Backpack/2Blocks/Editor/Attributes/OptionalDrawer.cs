﻿using UnityEngine;
using UnityEditor;
using System;
using System.Text.RegularExpressions;

[CustomPropertyDrawer (typeof (OptionalAttribute))]
class OptionalDrawer : PropertyDrawer
{

	bool hasIt;
	
    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
    {
	    
	    
	    if (hasIt == false && property.objectReferenceValue == null)
	    {
		    string text = (attribute as OptionalAttribute).prefix + label.text;
		    hasIt = EditorGUI.Toggle(position, text, hasIt);
	    }
		else
	    {
		    var type = GetPropertyObjectType(property);
		    if (type != null && (type.BaseType == typeof(TriggerExtended) || (type.BaseType != null && type.BaseType.BaseType == typeof(TriggerExtended))))
			    TriggerExtendedDrawer.OnGUIDrawer(position, property, label);
		    else EditorGUI.PropertyField(position, property);
	    }
	    
    }
	
	public static string GetPropertyType(SerializedProperty property)
	{
		var type = property.type;
		var match = Regex.Match(type, @"PPtr<\$(.*?)>");
		if (match.Success)
			type = match.Groups[1].Value;
		return type;
	}

	public static Type GetPropertyObjectType(SerializedProperty property)
	{
		return System.Type.GetType(GetPropertyType(property) + ", BackpackBlocks");                                   
	}    
}