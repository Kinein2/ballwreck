﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TriggerOnButton))]
public class TriggerOnButtonDrawer : Editor {



	public override void OnInspectorGUI()
	{
		var myTar = target as TriggerOnButton;
		
		if (myTar.buttons != null)
		for (int i = 0; i < myTar.buttons.Length; i++)
			DrawButtonTrigger(myTar, i);
		
		
		
		Rect pos = EditorGUILayout.GetControlRect();
		float r = pos.xMin + pos.xMax;
		pos.width /= 2;
		pos.x = r/2 - pos.width/2;
		if (GUI.Button(pos, "+"))
		{
			myTar.pTypes = myTar.pTypes.Increase(1);
			myTar.triggers = myTar.triggers.Increase(1);
			myTar.buttons = myTar.buttons.Increase(1);
		}
		
	}

	public void DrawButtonTrigger(TriggerOnButton myTar, int i)
	{
		Rect position = EditorGUILayout.GetControlRect();
		position = EditorGUI.PrefixLabel(position, new GUIContent("Button"));

		position.xMax -= 20;

		Rect posLeft = position;
		posLeft.xMax = (posLeft.xMax - posLeft.xMin)/2 + posLeft.xMin;
		posLeft.xMax -= 5;
		myTar.buttons[i] = (KeyCode)EditorGUI.EnumPopup(posLeft, (System.Enum)myTar.buttons[i]);

		Rect posRight = position;
		posRight.xMin = posLeft.xMax + 10;
		myTar.pTypes[i] = (TriggerOnButton.PressType)EditorGUI.EnumPopup(posRight, (System.Enum)myTar.pTypes[i]);


		Rect pos = position;
		pos.xMin = position.xMax + 4;
		pos.xMax = pos.xMin + 16;
		pos.height -= 2;

		if (GUI.Button(pos, new GUIContent("X")))
		{
			myTar.pTypes = myTar.pTypes.RemoveAt(i);
			myTar.triggers = myTar.triggers.RemoveAt(i);
			myTar.buttons = myTar.buttons.RemoveAt(i);
			
			if (i != 0) i--;
			return;
			/*
			GoodEvent.Action[] actionsTmp = new GoodEvent.Action[myTarget.actions.Length - 1];
			int n = 0;
			for (int j = 0; j < myTarget.actions.Length; j++)
			{
				if (j == i) continue;
				actionsTmp[n] = myTarget.actions[j];
				n++;
			}

			myTarget.actions = actionsTmp;
			if (i != 0) i--;
			*/
		}

		var property = serializedObject.FindProperty("triggers");
		var propI = property.GetArrayElementAtIndex(i);
		EditorGUILayout.PropertyField(propI);
	}
}

