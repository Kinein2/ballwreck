﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Backpack/Simple Trigger")]
public class SimpleTrigger : TriggerExtended
{
	
	public void Raise(MonoBehaviour author)
	{
		if (CouldBeRaised(author))		
			_Raise(author);
	}
    
    
	public override string StateToString()
	{
		return base.StateToString();
	}

#if UNITY_EDITOR
	public override void DrawTrigArgField()
	{

		base.DrawTriggerField();


		if (!Application.isPlaying)
			GUI.enabled = false;

		if (GUILayout.Button("Raise"))
			Raise(AuthorMono);

	}
#endif

}