﻿using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
#endif


[CreateAssetMenu(menuName = "Backpack/Vector2 Trigger")]
public class Vector2Trigger : SimpleTrigger
{
    public Vector2 Position { get; private set; }

    public void Raise(MonoBehaviour author, Vector2 pos)
    {
        Position = pos;
        _Raise(author);
    } 

    public override string StateToString()
    {
        return base.StateToString() + "  " + Position.ToString();
    }


#if UNITY_EDITOR
    public override void DrawTrigArgField()
    {

        base.DrawTriggerField();

        Position = EditorGUILayout.Vector2Field("Position", Position);
        
		
        if (!Application.isPlaying)
            GUI.enabled = false;
        
        if (GUILayout.Button("Raise"))
            Raise(AuthorMono);

    }
#endif 
    
}



