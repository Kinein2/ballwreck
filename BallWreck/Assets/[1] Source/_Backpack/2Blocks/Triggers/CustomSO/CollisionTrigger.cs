﻿using System;
using UnityEngine;


using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


[CreateAssetMenu(menuName = "Backpack/Collision Trigger")]
public class CollisionTrigger : SimpleTrigger
{
    public GameObject Target { get; private set; }
    public Vector3 Normal { get; private set; }

    public void Raise(MonoBehaviour author, GameObject target, Vector3 normal)
    {
        Target = target;
        Normal = normal;
        _Raise(author);
    } 

    public override string StateToString()
    {
        return base.StateToString() + "  " + Target.name + "  " + Normal.ToString();
    }


#if UNITY_EDITOR
    public override void DrawTrigArgField()
    {

        base.DrawTriggerField();

        Target = EditorGUILayout.ObjectField("Target", Target, typeof(GameObject), true) as GameObject;
        Normal = EditorGUILayout.Vector3Field("Normal", Normal);
		
        if (!Application.isPlaying)
            GUI.enabled = false;
        
        if (GUILayout.Button("Raise"))
            Raise(AuthorMono);
    }
#endif 
    
}