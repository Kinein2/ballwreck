﻿using UnityEngine;

// @Unfinished: не передают аргументы триггеров

public class TriggerToTriggers : MonoBehaviour
{
	[SerializeField]
	SimpleTrigger input;
	[SerializeField]
	SimpleTrigger[] output = new SimpleTrigger[0]; 

	void Start() 
	{
		input.AddCallback(this, Raised);	
	}

	void Raised() 
	{
		//triggerLeave.Raise(ta.Author);
		for (int i = 0; i < output.Length; i++)
			output[i].Raise(this); // поидее (input.Author)
	}

}
