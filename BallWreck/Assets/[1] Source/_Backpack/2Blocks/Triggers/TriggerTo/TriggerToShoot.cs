﻿using UnityEngine;

public class TriggerToShoot : MonoBehaviour
{

	public GameObject bullet;

	public Vector2Trigger shootTrigger;
	public SimpleTrigger simpleTrigger;

	public SimpleTrigger triggerOnShoot;
	
	public Cooldown reloadCD = new Cooldown();

	void Start () 
	{
		shootTrigger.AddCallback(this, ShootTriggered);
		simpleTrigger.AddCallback(this, ShootTriggeredSimple);
		
		SingletonPools.Preload(bullet, 1000);
	}

	void ShootTriggered() 
	{
		Shoot(shootTrigger.Position);
	}

	void ShootTriggeredSimple() 
	{
		//Debug.Log("Bang!" + name);
		Shoot((Vector2)transform.position + Vector2.right);
	}

	public GameObject Shoot(Vector2 targetPosition)
	{
		//if (reloadTimer < reloadTime) return null;
		if (reloadCD.Ready == false) return null;

		if (bullet == null)
		{
			DLog.Error("There is no bullet for " + name, DLog.Level.Verbose, this, "SHOOT (BP)");
			return null;
		}

		Vector2 delta = targetPosition - (Vector2)transform.position;

		float ang = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;
		
		//GameObject GO = Instantiate(bullet, transform.position, Helper.EulerZ(ang));
		//GameObject GO = SingletonPools.Spawn(Pool.Projectiles, bullet, transform.position, Helper.EulerZ(ang));
		
		GameObject GO = SingletonPools.Create(bullet, transform.position, Helper.EulerZ(ang));
		triggerOnShoot.Raise(this);
		
		DLog.Log(GO.name + "was shooted by " + name, DLog.Level.UltraVerbose, this, "SHOOT (BP)");
		reloadCD.Set();

		return GO;
	}
}