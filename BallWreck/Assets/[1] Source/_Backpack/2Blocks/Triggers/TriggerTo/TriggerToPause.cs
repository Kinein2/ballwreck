﻿using UnityEngine;

public class TriggerToPause : MonoBehaviour 
{
	[SerializeField]
	SimpleTrigger trigger;


	bool paused = false;
	float time0 = 1.0f;

	void Start () 
	{
		trigger.AddCallback(this, TimeSwitch);
	}

	void TimeSwitch() 
	{
		if (paused)
		{
			Time.timeScale = time0;
			time0 = 1;
		} else {
			time0 = Time.timeScale;
			Time.timeScale = 0.0f;
		}
		
		paused = !paused;
	}

}
