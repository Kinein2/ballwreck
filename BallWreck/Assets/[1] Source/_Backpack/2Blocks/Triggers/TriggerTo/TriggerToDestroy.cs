﻿using UnityEngine;

public class TriggerToDestroy : MonoBehaviour 
{

	[SerializeField]
	SimpleTrigger triggerToDestroy;
	[SerializeField]
	SimpleTrigger triggerOnDestruction;
	[SerializeField]
	FloatReference delayFromTrigger, delayFromStart;
	[SerializeField]
	GameObject target;

	[SerializeField]
	bool self, onStart;

	float destructionTimer;
	bool destructed;
	

	void Start() 
	{
		destructionTimer = 1000000000;

		if (onStart)
			DestructCommand(false);

		triggerToDestroy.AddCallback(this, OnTrigger);
	}
	
	void OnTrigger() 
	{
		DestructCommand(true);
	}


	void Update()
	{
		
		destructionTimer -= Time.deltaTime;

		if (destructionTimer < 0 && destructed == false)
			Destruct();
	
	 }

	
	void DestructCommand(bool fromTrigger) 
	{
		float newDelay = fromTrigger ? delayFromTrigger : delayFromStart;
		destructionTimer = Mathf.Min(destructionTimer, newDelay);
		
		if (destructionTimer < Time.fixedDeltaTime / 2)
			Destruct();
	}
	
	void Destruct()
	{
		triggerOnDestruction.Raise(this);
		DLog.Log("Destruction of " + name + " is triggered.", DLog.Level.Verbose, this, "DESTRUCTION (BP)");
			
		if (target != null)
			Destroy(target, 0.02f);
		else Destroy(gameObject, 0.02f);

		destructed = true;
	}
}
