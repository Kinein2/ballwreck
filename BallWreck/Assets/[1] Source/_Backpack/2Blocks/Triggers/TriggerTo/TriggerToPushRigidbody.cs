﻿using UnityEngine;

public class TriggerToPushRigidbody : MonoBehaviour 
{
	[SerializeField]
	float forcePower = 5, minAmplitude = 3;
            
	[SerializeField]
	Rigidbody targetBody;
            
	[SerializeField]
	Vector2Trigger pushTrigger;

	

	void Start () {
		pushTrigger.AddCallback(this, OnTrigger);
	}

	void OnTrigger()
	{
		float cy = 0.5f / Camera.main.aspect;
		//Vector2 d = cursorDir.Value;

		Vector2 d = pushTrigger.Position;

		Vector2 center = new Vector2(0.5f, cy);

		Vector2 pushDir = d - center;
		//Debug.Log(pushDir);


		//pushDir = pushDir.normalized * maxForce;
		float f = pushDir.magnitude;

		if (f < minAmplitude)
			pushDir = pushDir.normalized * minAmplitude;

		pushDir *= forcePower;
		Vector3 worldPushDir = new Vector3(pushDir.x, 0, pushDir.y);
			
		targetBody.AddForce(worldPushDir, ForceMode.Impulse);
		
	}

	
}
