﻿using UnityEngine;

// @Unfinished: не передают аргументы триггеров

public class TriggersToTrigger : MonoBehaviour 
{
	[SerializeField]
	SimpleTrigger[] input = new SimpleTrigger[0];
	[SerializeField]
	SimpleTrigger output; 

	void Start() 
	{
		for (int i = 0; i < input.Length; i++)
			input[i].AddCallback(this, Raised);
	}

	void Raised() 
	{
		//triggerLeave.Raise(ta.Author);
		output.Raise(this);
	}

}
