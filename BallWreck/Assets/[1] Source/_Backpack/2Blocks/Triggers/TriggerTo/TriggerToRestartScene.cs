﻿using UnityEngine;
using UnityEngine.SceneManagement;

// пока нет статических функций
public class TriggerToRestartScene : MonoBehaviour
{
	[SerializeField]
	SimpleTrigger trigger;

	void Start () 
	{
		trigger.AddCallback(this, DoRestart);
	}
	

	void DoRestart()
	{
		resetted = true;
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		DLog.Log("Scene restarted by trigger!", DLog.Level.Normal, this, "Backpack");
	}
	
	public static bool resetted = true;

}
