﻿using UnityEngine;

public class TriggerOnButton : MonoBehaviour {
	
	public enum PressType
	{
		OnPress, OnHold, OnRelease, OnNotHold
	}
	



	public PressType[] pTypes = new PressType[0];
	public SimpleTrigger[] triggers = new SimpleTrigger[0];
	public KeyCode[] buttons = new KeyCode[0];


	protected void Update ()
	{
		
		for (int i = 0; i < buttons.Length; i++)
		{
			switch (pTypes[i])
			{
				case PressType.OnPress:
					if (Input.GetKeyDown(buttons[i]))
						triggers[i].Raise(this);
				break;

				case PressType.OnRelease:
					if (Input.GetKeyUp(buttons[i]))
						triggers[i].Raise(this);
					break;

				case PressType.OnHold:
					if (Input.GetKey(buttons[i]))
						triggers[i].Raise(this);
					break;

					
				case PressType.OnNotHold:
					if (Input.GetKey(buttons[i]) == false)
						triggers[i].Raise(this);
					break;
			}
		}
	}
}
