﻿using UnityEngine;

// @Unfinished: TimedTriggerWithArgs - вариация CustomTrigger'а у него который запоминает аргументы

public class TriggerOnTimer : MonoBehaviour 
{
	[SerializeField]
	SimpleTrigger trigger;

	public bool activated = true, loop = false;
	[SerializeField]
	FloatReference time, timer;


	void Update()
	{		
		if (activated) {
			timer.Value += Time.deltaTime;

			if (timer.Value > time.Value)
			{
				trigger.Raise(this);
				if (loop)
					DLog.Log("TriggerOnTimer (" + name + ") has looped", DLog.Level.Verbose, "TIMER (BP)");
				else DLog.Log("TriggerOnTimer (" + name + ") has ended", DLog.Level.Verbose, "TIMER (BP)");
				timer.Value = 0;
				if (!loop)
					activated = false;
			}	
		}
 	}

	public void ResetTimer()
	{
		activated = true;
		timer.Value = 0;
	}

	public static TriggerOnTimer CreateNew(SimpleTrigger ST, float time, bool loop = false)
	{
		GameObject GO = new GameObject("TA_" + ST.name);

		TriggerOnTimer TT = GO.AddComponent<TriggerOnTimer>();
		TT.trigger = ST;
		TT.time.Value = time;
		TT.loop = loop;

		return TT;
	}
	

}
