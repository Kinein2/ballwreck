﻿using UnityEngine;

public class TriggerOnCollision : MonoBehaviour 
{

	[SerializeField]
	LayerMask collisionLayer = ~0; // everything
	
	[SerializeField]
	string lookForName, lookForComponent;
	
	[SerializeField]
	CollisionTrigger onEnter, onExit, onStay;

	enum PhysicsType 
	{
		Collision2D, Trigger2D, Collision3D, Trigger3D, None
	}

	PhysicsType physicsType;

	


	void Start () 
	{
		CheckType();
	}


	void CheckType()
	{
		var C = GetComponent<Collider2D>();
		if (C != null)
		{
			if (C.isTrigger)
				physicsType = PhysicsType.Trigger2D;
			else physicsType = PhysicsType.Collision2D;

			return;
		}
		var C2 = GetComponent<Collider>();
		if (C2 != null)
		{
			if (C2.isTrigger)
				physicsType = PhysicsType.Trigger3D;
			else physicsType = PhysicsType.Collision3D;

			return;
		} 

		DLog.Error("No colliders in " + name + "!", DLog.Level.Normal, this, "COLLISION (BP)");
		physicsType = PhysicsType.None;
	}

	bool CheckPrerequisites(PhysicsType pt, GameObject go)
	{
		
		if (LayerExtensions.Includes(collisionLayer, go.layer) == false) 
		{
            DLog.Log(name + " collided with " + go.name + ", but it has a wrong layer", DLog.Level.UltraVerbose, this, "COLLISION (BP)");
			return false;
		}

		if (lookForName.Length > 0 && lookForName != go.name)
		{
            DLog.Log(name + " collided with " + go.name + ", but it has a wrong name", DLog.Level.UltraVerbose, this, "COLLISION (BP)");
            return false;
		}

		if (lookForComponent.Length > 0)
		if (go.GetComponent(lookForComponent) == null)
            {
                DLog.Log(name + " collided with " + go.name + ", but it has a wrong tags", DLog.Level.UltraVerbose, this, "COLLISION (BP)");
                return false;
            }
        

		//if (physicsType != pt)
		//	DLog.Error("Wrong type (" + physicsType.ToString() + "for " + name + "! (should be " + pt.ToString() + ")", DLog.Level.Critical, this, "COLLISION (BP)");

		
		DLog.Log(name + " collided with " + go.name + "!", DLog.Level.UltraVerbose, this, "COLLISION (BP)");
		return true;

	}

	void OnCollisionEnter(Collision collision)
	{
		if (CheckPrerequisites(PhysicsType.Collision3D, collision.gameObject) == false)
			return;
		
		if (onEnter != null)
			onEnter.Raise(this, collision.gameObject, collision.impulse);
	}

	void OnCollisionStay(Collision collision)
	{
		if (CheckPrerequisites(PhysicsType.Collision3D, collision.gameObject) == false)
			return;

		if (onStay != null)
			onStay.Raise(this, collision.gameObject, collision.impulse);
	}

	void OnCollisionExit(Collision collision)
	{
		if (CheckPrerequisites(PhysicsType.Collision3D, collision.gameObject) == false)
			return;

		if (onExit != null)
			onExit.Raise(this, collision.gameObject, collision.impulse);
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (CheckPrerequisites(PhysicsType.Collision2D, collision.gameObject) == false)
			return;

		if (onEnter != null)
			onEnter.Raise(this, collision.gameObject, collision.contacts[0].normalImpulse * collision.contacts[0].normal); // @Volatile: может отличаться от чистого импульса
	}

	void OnCollisionStay2D(Collision2D collision)
	{
		if (CheckPrerequisites(PhysicsType.Collision2D, collision.gameObject) == false)
			return;

		if (onStay != null)
			onStay.Raise(this, collision.gameObject, collision.contacts[0].normalImpulse * collision.contacts[0].normal);
	}

	void OnCollisionExit2D(Collision2D collision)
	{
		if (CheckPrerequisites(PhysicsType.Collision2D, collision.gameObject) == false)
			return;

		if (onExit != null)
			onExit.Raise(this, collision.gameObject, collision.contacts[0].normalImpulse * collision.contacts[0].normal);
	}

	void OnTriggerEnter(Collider other)
	{
		if (CheckPrerequisites(PhysicsType.Trigger3D, other.gameObject) == false)
			return;

		if (onEnter != null)
			onEnter.Raise(this, other.gameObject, other.transform.position - transform.position); // @Volatile: совершенно не сходится с импульсом
	}

	void OnTriggerStay(Collider other)
	{
		if (CheckPrerequisites(PhysicsType.Trigger3D, other.gameObject) == false)
			return;

		if (onStay != null)
			onStay.Raise(this, other.gameObject, other.transform.position - transform.position);
	}

	void OnTriggerExit(Collider other)
	{
		if (CheckPrerequisites(PhysicsType.Trigger3D, other.gameObject) == false)
			return;

		if (onExit != null)
			onExit.Raise(this, other.gameObject, other.transform.position - transform.position);
	}
	

	void OnTriggerEnter2D(Collider2D other)
	{
		if (CheckPrerequisites(PhysicsType.Trigger2D, other.gameObject) == false)
			return;

		if (onEnter != null)
			onEnter.Raise(this, other.gameObject, other.transform.position - transform.position);
	}

	void OnTriggerStay2D(Collider2D other)
    {
		if (CheckPrerequisites(PhysicsType.Trigger2D, other.gameObject) == false)
			return;

		if (onStay != null)
			onStay.Raise(this, other.gameObject, other.transform.position - transform.position);
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (CheckPrerequisites(PhysicsType.Trigger2D, other.gameObject) == false)
			return;

		if (onExit != null)
			onExit.Raise(this, other.gameObject, other.transform.position - transform.position);
	}
}
