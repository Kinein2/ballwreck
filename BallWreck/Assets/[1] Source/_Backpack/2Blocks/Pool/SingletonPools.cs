﻿using System.Collections.Generic;
using UnityEngine;

public enum Pool  {
    None,
    Entities,
    Decals,
    Projectiles,
    Audio 
}


public class SingletonPools : SingletonMono<SingletonPools>
{
    // @Volatile: предполагает что не может быть двойное вложение, то есть [GENERATED]/PROJ/VASYA
    public static string projectilesPath = "[GENERATED]/Projectiles",
        entitiesPath                     = "[GENERATED]/Entities",
        decalsPath                       = "[GENERATED]/Decals",
        soundsPath                       = "[GENERATED]/Sounds",
        defaultPath                      = "[GENERATED]";


    public static bool parentTransfer = false;

    public readonly Dictionary<int, PoolWatcher> pools = new Dictionary<int, PoolWatcher>();


    static Transform projectilesParent, entitiesParent, decalsParent, soundsParent, defaultParent;

    void OnEnable()
    {
        AddPool(Pool.Projectiles);
        AddPool(Pool.Entities);
        AddPool(Pool.Decals);
        AddPool(Pool.Audio);    
        
    }

    public static Transform GetPoolDefaultParent(Pool pool)
    {
        string s = "";
        Transform parent = null;
        
        switch (pool)
        {
            case Pool.None:
                parent = defaultParent;
                s = defaultPath;
                break;
            case Pool.Projectiles:
                parent = projectilesParent;
                s = projectilesPath;
                break;
            case Pool.Entities:
                parent = entitiesParent;
                s = entitiesPath;
                break;
            case Pool.Decals:
                parent = decalsParent;
                s = decalsPath;
                break;
            case Pool.Audio:
                parent = soundsParent;
                s = soundsPath;
                break;
        }

        if (parent)
            return parent;

        GameObject go;
        
        if (pool == Pool.None)
        {
            go = GameObject.Find(defaultPath) ?? new GameObject(defaultPath);
            defaultParent = go.transform;
            return defaultParent;
        }
        
        
        string parentPath;
        Transform tParent = Helper.GetParent(s, out parentPath);
        
        
        if (tParent == null)
        {
            var goParent = GameObject.Find(parentPath) ?? new GameObject(parentPath);
            tParent = goParent.transform;
                
            switch (pool)
            {
                case Pool.Projectiles:
                    projectilesParent = tParent;
                    break;
                case Pool.Entities:
                    entitiesParent = tParent;
                    break;
                case Pool.Decals:
                    decalsParent = tParent;
                    break;
                case Pool.Audio:
                    soundsParent = tParent;
                    break;
            }
        }

        s = s.Substring(s.LastIndexOf('/') + 1);
        

        go = GameObject.Find(s) ?? new GameObject(s);
        go.transform.parent = tParent;

        return go.transform;
    }

    public void AddPool(Pool id, bool reparent = true)
    {
        if (reparent)
        {
            var defaultFolder = GetPoolDefaultParent(Pool.None);
            Transform poolTransform;
            if (parentTransfer)
            {
                poolTransform = new GameObject("Pool_" + id).transform;
                poolTransform.SetParent(defaultFolder);    
            } else
            {
                poolTransform = GetPoolDefaultParent(id);
            }
            
            
            var PW = poolTransform.gameObject.AddComponent<PoolWatcher>();
            PW.pool = id;

            PW.parentForDead = PW.transform;
            pools.Add((int) id, PW);
            //pool.SetPoolParent(poolGO.transform);
        }
    }

    public static GameObject CreateNew(string name)
    {
        var go = new GameObject(name);
        go.transform.parent = GetPoolDefaultParent(Pool.None);
        return go;
    }
    
    public static GameObject Create(GameObject prototype, Vector3 position, Quaternion rotation)
    {
        return Create(prototype, null, position, rotation);
    }

    public static GameObject Create(GameObject prototype, GameObject owner, Vector3 position, Quaternion rotation)
    {
        if (prototype == null) return null;

        var c = prototype.GetComponent<CacheMe>();
        if (c == null)
            return Instantiate(prototype, position, rotation, GetPoolDefaultParent(Pool.None));


        GameObject go = null;

        if (c.pool == Pool.None)
            go = Instantiate(prototype, position, rotation, GetPoolDefaultParent(Pool.None));
            

            


        PoolWatcher watcher;

        if (Instance.pools.TryGetValue((int) c.pool, out watcher))
        {
            if (c.enabled == false)
                go = Instantiate(prototype, position, rotation, watcher.GetParentForAlive());
            else go = watcher.Spawn(prototype, position, rotation);
        }

        if (go != null)
        {
            var cm = go.GetComponent<CacheMe>();
            cm.owner = owner;
            cm.prototype = prototype;
            return go;
        }
        

        return null;
    }

    public static void Preload(GameObject prototype, int amount)
    {
        if (prototype == null) return;

        var c = prototype.GetComponent<CacheMe>();

        if (c == null || c.pool == Pool.None || c.enabled == false)
            return;

        PoolWatcher watcher;

        if (Instance.pools.TryGetValue((int) c.pool, out watcher))
        {
            for (int i = 0; i < amount; i++)
                watcher.Preload(prototype);
        }
    }

    public static void Delete(GameObject go)
    {
        var c = go.GetComponent<CacheMe>();
        if (c == null)
        {
            GameObject.Destroy(go);
            return;
        }


        if (c.pool == Pool.None || c.enabled == false)
        {
            GameObject.Destroy(go);
            return;
        }


        PoolWatcher watcher;

        if (Instance.pools.TryGetValue((int) c.pool, out watcher))
        {
            watcher.Despawn(go);
            return;
        }
    }
}

/*
 
    public static GameObject Create(GameObject prototype, Vector3 position, Quaternion rotation, Transform parentTransfrom)
    {
        var c = prototype.GetComponent<CacheMe>();
        if (c == null)
            return Instantiate(prototype, position, rotation, parentTransfrom);
        
        if (c.pool == Pool.None || c.enabled == false)
            return Instantiate(prototype, position, rotation, parentTransfrom);
        
        
        int id = (int) c.pool;
        PoolWatcher watcher = null;
        if (Instance.pools.TryGetValue(id, out watcher))
            return watcher.Spawn(prototype, position, rotation, parentTransfrom);

        return null;
    }
    
 */

/*

    GameObject _Spawn(Pool pool, GameObject prototype, Vector3 position, Quaternion rotation, Transform parentTransfrom)
    {
        int id = (int) pool;
        PoolWatcher watcher = null;

        if (pool == Pool.None)
        {
            // @Unfinished...
            
        }
        
        // ищем нужный пул, передаем задачу ему
        if (pools.TryGetValue(id, out watcher))
            return watcher.Spawn(prototype, position, rotation, parentTransfrom);

        return null;
    }

    void _Despawn(Pool pool, GameObject go)
    {
        int id = (int) pool;
        PoolWatcher watcher = null;

        // ищем нужный пул, передаем задачу ему
        if (pools.TryGetValue(id, out watcher))
        {
            watcher.Despawn(go);
        }
    }


    

    public static GameObject Spawn(CacheMe prototypeCache, Vector3 position, Quaternion rotation)
    {
        return Spawn(prototypeCache.pool, prototypeCache.gameObject, position, rotation);
    }
    
    public static GameObject Spawn(Pool pool, GameObject prototype, Vector3 position, Quaternion rotation)
    {
        int id = (int) pool;
        PoolWatcher watcher;
        
        if (Instance.pools.TryGetValue(id, out watcher))
            return watcher.Spawn(prototype, position, rotation, Instance.pools[id].defaultParentForAlive);

        return null;
    }
      

    public static GameObject Spawn(Pool pool, GameObject prototype, Vector3 position, Quaternion rotation, Transform parentTransfrom)
    {
        return Instance._Spawn(pool, prototype, position, rotation, parentTransfrom);
    }

    public static void Despawn(CacheMe mc)
    {
        Instance._Despawn(mc.pool, mc.gameObject);
    }
    
    public static void Despawn(Pool pool, GameObject go)
    {
        Instance._Despawn(pool, go);
    }
    
    */