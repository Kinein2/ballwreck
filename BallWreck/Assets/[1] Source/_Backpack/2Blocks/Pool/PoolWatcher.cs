﻿using System.Collections.Generic;
using UnityEngine;

public class PoolWatcher : MonoBehaviour
{
    public Pool      pool;
    public Transform defaultParentForAlive, parentForDead;

    // массив кэш объектов на один прототип
    //public Dictionary<GameObject, CacheMe[]> aliveCache = new Dictionary<GameObject, CacheMe[]>();
    public Dictionary<GameObject, Stack<CacheMe>> asleepCache = new Dictionary<GameObject, Stack<CacheMe>>();


    public Transform GetParentForAlive()
    {
        if (defaultParentForAlive == null)
            defaultParentForAlive = SingletonPools.GetPoolDefaultParent(pool);

        return defaultParentForAlive;
    }
    


    public GameObject Spawn(GameObject prototype, Vector3 position, Quaternion rotation)
    {

        CacheMe spawning = null; 
        Stack<CacheMe> arr;

        if (asleepCache.TryGetValue(prototype, out arr))
        {
            if (arr.Count > 0)
            {
                spawning = arr.Pop();
                asleepCache[prototype] = arr;    
            }
        }

        if (spawning == null)
        {
            var go = Instantiate(prototype, position, rotation, GetParentForAlive());
            spawning = go.GetComponent<CacheMe>();
            spawning.prototype = prototype;
        }
        else
        {
            spawning.transform.position = position;
            spawning.transform.rotation = rotation;
            spawning.gameObject.SetActive(true);
            
            if (SingletonPools.parentTransfer)
                spawning.transform.parent = GetParentForAlive();
        }
        
        
        
        // Вытаскиваем спящего
        // Вставляем в неспящих
        // А что если неспящим не нужен массив вовсе? записав их прототип в кэшми
        
        
        spawning.OnSpawn();
        
        return spawning.gameObject;
    }

    public void Preload(GameObject prototype)
    {
        GameObject instance = Instantiate(prototype, parentForDead);
        var cm = instance.GetComponent<CacheMe>();
        if (cm == null)
        {
            Destroy(instance);
            DLog.Error("You preloading an object without CacheMe!", DLog.Level.Normal, prototype);
            return;
        }
        cm.prototype = prototype;
        Despawn(cm);
    }

    /*
    public void Preload(GameObject prototype)
    {
        Debug.Log("PRELAD");
        GameObject instance = Instantiate(prototype, parentForDead);
        AddToCachelist(instance, prototype, false);
        instance.SetActive(false);
    }
    */

    public void Despawn(CacheMe cacheComp, bool despawnCall = true)
    {
        if (despawnCall)
            cacheComp.OnDespawn();

        cacheComp.gameObject.SetActive(false);

        Stack<CacheMe> arr;

        if (asleepCache.TryGetValue(cacheComp.prototype, out arr) == false)
        {
            arr = new Stack<CacheMe>();    
            asleepCache.Add(cacheComp.prototype, arr);    
        }
            
        if (asleepCache.TryGetValue(cacheComp.prototype, out arr))
        {
            arr.Push(cacheComp);
            asleepCache[cacheComp.prototype] = arr;
        }
        else
        {
            Destroy(cacheComp.gameObject);
            return;
        }
        
        if (SingletonPools.parentTransfer)
            cacheComp.transform.parent = parentForDead;
    }
    
    public void Despawn(GameObject go)
    {
        // @Unfinished: нужен таймер для потери...
        //int id = GetIDofGO(go, true);
        
        Despawn(go.GetComponent<CacheMe>());
    }
}