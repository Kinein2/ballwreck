﻿using System.Collections.Generic;
using UnityEngine;

public interface IPoolable
{
    void OnSpawn();
    void OnDespawn();
}


public class CacheMe : MonoBehaviour {

    public Pool pool;

    [HideInInspector]
    public GameObject prototype;

    public GameObject owner;
    public List<IPoolable> poolables = new List<IPoolable>(); 
    
    
    // @Volatile: если в чужом авейке мы не успеем сделать связь а он запустит OnSpawn я хз
    void Awake()
    {
        SetAllReferences();
    }
    
    void Update()
    {
        
    }

    void AddPoolables(GameObject go)
    {
        IPoolable[] pls = go.GetComponents<IPoolable>();
        
        for (int i = 0; i < pls.Length; i++)
            poolables.Add(pls[i]);
    }
    
    public void SetAllReferences()
    {
        AddPoolables(gameObject);
        
        Helper.RunActionToAllChildren(transform, t =>
        {
            AddPoolables(t.gameObject); 
        });
    }

    public void OnSpawn()
    {
        DLog.Log("Spawned " + name, DLog.Level.UltraVerbose, this, "POOL (BP)");
        foreach (var p in poolables)
            p.OnSpawn();

    }
    
    public void OnDespawn()
    {
        DLog.Log("Despawned " + name, DLog.Level.UltraVerbose, this, "POOL (BP)");
        foreach (var p in poolables)
            p.OnDespawn();
    }
}
