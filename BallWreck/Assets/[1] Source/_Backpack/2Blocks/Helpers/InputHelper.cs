﻿using UnityEngine;
using System.Collections.Generic;



public class InputHelper : MonoBehaviour
{




	private static TouchCreator lastFakeTouch;

	public static List<Touch>  GetTouches()
	{
		List<Touch> touches = new List<Touch>();
		
		
		if (DLog.currentLevel == DLog.Level.UltraVerbose)
		if (Input.touches != null)
		{
			int n = 0;
			for (int i = 0; i < Input.touches.Length; i++)
			{
				if (Input.touches[i].phase == TouchPhase.Began)
					n++;
			}
			
				
			
			if (n > 0)
				DLog.Log(n + " taps detected", DLog.Level.UltraVerbose, "INPUT (BP)");
		}
				
		
		
		touches.AddRange(Input.touches);
		
		if (lastFakeTouch == null) lastFakeTouch = new TouchCreator();

		if (Input.touches != null && Input.touches.Length > 0)
			return touches;
		
		if (Input.GetMouseButtonDown(0))
		{
			
			DLog.Log("Mouse pressed", DLog.Level.UltraVerbose, "INPUT (BP)");
			lastFakeTouch.phase = TouchPhase.Began;
			lastFakeTouch.deltaPosition = new Vector2(0, 0);
			lastFakeTouch.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			lastFakeTouch.fingerId = 0;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			DLog.Log("Mouse unpressed", DLog.Level.UltraVerbose, "INPUT (BP)");
			lastFakeTouch.phase = TouchPhase.Ended;
			Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			lastFakeTouch.deltaPosition = newPosition - lastFakeTouch.position;
			lastFakeTouch.position = newPosition;
			lastFakeTouch.fingerId = 0;
		}
		else if (Input.GetMouseButton(0))
		{
			lastFakeTouch.phase = TouchPhase.Moved;
			Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			lastFakeTouch.deltaPosition = newPosition - lastFakeTouch.position;
			lastFakeTouch.position = newPosition;
			lastFakeTouch.fingerId = 0;
		}
		else
		{
			lastFakeTouch = null;
		}
		if (lastFakeTouch != null) touches.Add(lastFakeTouch.Create());
//#endif


		return touches;
	}

}

/*
 * 
 * Please fix your Stationary bug:

                 Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                 lastFakeTouch.deltaPosition = newPosition - lastFakeTouch.position;
                 lastFakeTouch.phase = lastFakeTouch.deltaPosition.magnitude == 0 ? TouchPhase.Stationary : TouchPhase.Moved;
				 */
