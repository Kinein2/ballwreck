﻿	using UnityEngine;
using System;

public static class Helper
{
	
	// поворот вектора v на угол ang в градусах
	public static Vector2 RotateVector(Vector2 v, float ang)
	{
		return new Vector2(v.x * Mathf.Cos(ang * Mathf.Deg2Rad) - v.y * Mathf.Sin(ang * Mathf.Deg2Rad), v.y * Mathf.Cos(ang * Mathf.Deg2Rad) + v.x * Mathf.Sin(ang * Mathf.Deg2Rad));
	}

	// создать вектор по углу в градусах, размером scale
	public static Vector2 AngleVec(float ang, float scale = 1)
	{
		return new Vector2(scale * Mathf.Cos(ang * Mathf.Deg2Rad), scale * Mathf.Sin(ang * Mathf.Deg2Rad));
	}

	// получить перпендикулярный вектор от заданного (перпендикулярный вправо)
	public static Vector2 Ortho(Vector2 original)
	{
		return new Vector2(-original.y, original.x);
	}

	// векторное произведение двумерных векторов (хорошо для расчета скорости вращения и угловой скорости)
	public static float Cross(Vector2 a, Vector2 b)
	{
		return a.x * b.y - a.y * b.x;
	}

	// проигрыш выбранного звука с наложением звуков
	public static GameObject PlaySound(AudioClip AC, float Volume)
	{
		GameObject Cam = GameObject.Find("MainCamera");
		GameObject GO = new GameObject("snd");
		GameObject.Destroy(GO, AC.length + 0.1f);
		AudioSource AS = GO.AddComponent<AudioSource>();
		AS.clip = AC;
		AS.Play();
		AS.volume = Volume;
		GO.transform.parent = Cam.transform;
		GO.transform.localPosition = Vector3.zero;
		return GO;
	}

	// остановка выбранного звука
	public static void StopSound(GameObject GO)
	{
		AudioSource AS = GO.GetComponent<AudioSource>();
		AS.Stop();
	}

	// повернуть объект в сторону вектора v со сдвигом на доп угол shift в градусах
	public static Quaternion RotateTo(Vector2 v, float shift = 0)
	{
		float a = Mathf.Atan2(v.y, v.x) * 180 / Mathf.PI;
		Quaternion q = Quaternion.Euler(new Vector3(0, 0, a + shift));
		return q;
	}
	
	public static Quaternion RotateTo(Vector2z v, float shift = 0, int k = 1)
	{
		float a = Mathf.Atan2(v.z, v.x) * 180 / Mathf.PI;
		Quaternion q = Quaternion.Euler(new Vector3(0, k*(a + shift), 0));
		return q;
	}

	// случайное число от -0.5 до 0.5
	public static float crnd() { return UnityEngine.Random.value - 0.5f; }

	// случайный единичный вектор
	public static Vector2 rndVec() { return new Vector2(crnd(), crnd()).normalized; }

    public static Quaternion EulerZ(float z)
    {
        return Quaternion.Euler(new Vector3(0, 0, z));
    }

    // плавно поворачивает объект под углом ang к углу tar, используя размер шага delta
    // delta по сути это угловая скорость * на промежуток времени между вызовами функций (типа Time.fixedDeltaTime)
    public static void RotToTar(ref float ang, float tar, float delta)
	{

		int k = 1;

		if (ang < 0)
			ang = ang + 360;
		if (ang > 360)
			ang = ang - 360;
		if (tar < 0)
			tar = tar + 360;

		if (tar > ang)
			k = 1;
		else k = -1;


		if (Mathf.Abs(tar - ang) > 180)
			k = -k;
		

		if (Mathf.Abs(ang - tar) < delta)
		{
			ang = tar;
		}
		else ang += k * delta;
	}

	public delegate void void_transformfloat(Transform T, float f_info);
	
	public static void DoFunctionForAllChilds(Transform T, float f, void_transformfloat func)
	{
		for (int i = 0; i < T.childCount; i++)
		{
			var t = T.GetChild(i);
			func(t, f);
			if (t.childCount > 0)
				DoFunctionForAllChilds(t, f, func); // йеп йеп рекурсия!
		}
	}

	public static void SetTransformToLineZ(Transform lineT, Vector3 start, Vector3 end, bool global = true)
	{
		Vector3 pos = start + end;
		pos *= 0.5f;

		Vector3 dv = end - start;

		if (global)
			lineT.position = pos;
		else lineT.localPosition = pos;

		lineT.rotation = Helper.RotateTo(dv);

		lineT.localScale = new Vector3((dv).magnitude, lineT.localScale.y, lineT.localScale.y); // @Volatile: если будут масштабы в родителях то все навернется
	}

	public static bool isDirectParent(GameObject probParent, GameObject probChild) 
	{
		Transform T = probChild.transform;
		while (T != null) 
		{
			if (T.gameObject == probParent)
				return true;

			T = T.parent;
		}

		return false;
	}
	
	public static bool isThereInheritance(System.Type t, string typeName)
	{
		if (t.ToString() == typeName) return true;

		return _isThereParent(t, typeName, 0);
	}

	static bool _isThereParent(System.Type t, string typeName, int deepness)
	{
		string baseStr = t.BaseType.ToString();
		if (baseStr.IndexOf("System.") != -1)
			return false;

		if (baseStr == typeName) 
			return true;

		if (t.BaseType == null)
			return false;
		 
		if (deepness > 5)
		{
			//DLog.Log("UNEXPECTED RESULT", DLog.Level.Critical, "Backpack");
			return false;
		}
			
		return _isThereParent(t.BaseType, typeName, deepness+1);
	}
	
	public static Transform GetParent(string path, out string parentPath)
	{
		parentPath = path.Substring(0, path.LastIndexOf('/'));
		GameObject go = GameObject.Find(parentPath);
		if (go == null) return null;
		return go.transform;
	}

	public static void RunActionToAllChildren(Transform parent, Action<Transform> action)
	{
		for (int i = 0; i < parent.childCount; i++)
		{
			Transform T = parent.GetChild(i);
			action(T);
			RunActionToAllChildren(T, action);
		}
	} 
	
	public static void RunActionToAllParents(Transform startPoint, Action<Transform> action)
	{
		Transform T = startPoint.parent;
		while (T != null)
		{
			action(T);
			T = T.parent;
		}
	}

	public static void DrawPointZ(Vector3 pos, Color c, float time = 0)
	{
		Debug.DrawLine(pos, pos + Vector3.forward * 100, c, time);
	}
	
	public static void DrawPointY(Vector3 pos, Color c, float time = 0)
	{
		Debug.DrawLine(pos, pos + Vector3.up * 100, c, time);
	}
}
