﻿using UnityEngine;

public class OptionalAttribute : PropertyAttribute
{
    public readonly string prefix;
    
    
    public OptionalAttribute(string prefix = "has ")
    {
        this.prefix = prefix;
    }
    
}