﻿using System;

using UnityEngine;

[Serializable]
public class Cooldown
{
	public float time;

	MonoBehaviour _owner;

	public SingletonTimer.Timer _timer;
	[SerializeField]
	bool _ready;

	public bool Ready
	{
		get { return _ready; }
	}

	public Cooldown()
	{
		_ready = true;
		_timer = null;
	}

    public Cooldown(MonoBehaviour owner)
    {
        _ready = true;
        _timer = null;
        _owner = owner;
    }


    public Cooldown(float time, MonoBehaviour owner = null, bool ready = true)
	{
		this.time = time;
		_owner = owner;
		_timer = null;

		if (ready == false)
			Set();
		else _ready = true;
	}

	public void Set()
	{
		_ready = false;

		

		if (_timer != null)
		{
			// он существует но ещё не закончился
            if (_timer.callBackAction != null)
			if (SingletonTimer.FindTimer(_timer))
			{
				_timer.time = time;
				_timer.Reset();
				return;
			}
			
			
		}


		_timer = SingletonTimer.AddTimer(time, () => {
				_ready = true;
				_timer = null;
			}, false, 0, _owner );
	}

	public void Cancel() // @Refactor: убрать в жопу этот метод
	{
		if (_timer != null)
		{
			_timer.enabled = false;
			_timer.finished = true;
			_timer = null;
		}

		_ready = false;
	}

    public void Stop()
    {
        if (_timer != null)
        {
            _timer.enabled = false;
        }
    }

    public void Unstop()
    {
        if (_timer != null)
        {
            _timer.enabled = true;
        }
    }

    public bool IsSet() // @Refactor: убрать в жопу этот метод
    {
        if (_timer == null)
            return false;

        if (_timer.enabled == false)
            return false;

        if (_timer.time > 0)
            return true;


        return false;
    }

    

	public float GetReadiness()
	{
		if (_timer == null) return 1;
		return _timer.timer / _timer.time; 
	}
}


public class SingletonTimer : SingletonMono<SingletonTimer> {

	[System.Serializable]
	public class Timer
	{
		public MonoBehaviour owner;
		public string ownerName;
		public Action callBackAction;
		
		public bool enabled, loop;
		public float timer, time;

		[HideInInspector]
		public bool finished;

		public static bool showName = true;

		public void Update(float delta)
		{
			if (!enabled || finished) return;
			
			timer += delta;
			if (timer > time)
			{
				if (!loop)
				{
					DLog.Log("Timer of " + ownerName + " has ended.", DLog.Level.UltraVerbose, owner, "TIMER (BP)");
					finished = true;
				}
				else DLog.Log("Timer of " + ownerName + " has looped.", DLog.Level.UltraVerbose, owner, "TIMER (BP)");
				
				//if (owner)
				if (callBackAction != null)
				if (callBackAction.Target != null)
					callBackAction();
				timer = 0;
			}
		}
        
		public void Reset()
		{
			timer = 0;
			enabled = true;
			finished = false;
		}

	}

	[SerializeField]
	public Timer[] timers = new Timer[0];
	
	void Update ()
	{
		float delta = Time.deltaTime;

		for (int i = 0; i < timers.Length; i++)
			timers[i].Update(delta);
	
	}

	public static Timer AddTimer(float timeDelay, Action action, bool looped = false,
		float startTimer = 0, MonoBehaviour owner = null)
	{
		return Instance._AddTimer(timeDelay, action, looped, startTimer, owner);
	}

    public static void FinishMyTimers(MonoBehaviour mb)
    {
        Instance._FinishMyTimers(mb);
    }

	public static bool FindTimer(SingletonTimer.Timer timer)
	{
		return Instance.timers.IndexOf(timer) != -1;
	}


    public void _FinishMyTimers(MonoBehaviour mb)
    {
		if (mb == null) return;

        for (int i = 0; i < timers.Length; i++)
            if (timers[i].owner == mb)
                timers[i].finished = true;
    }

	public Timer _AddTimer(float timeDelay, Action action, bool looped = false, float startTimer = 0, MonoBehaviour owner = null)
	{
		var timer = new Timer();
		timer.enabled = true;
		timer.loop = looped;
		timer.time = timeDelay;
		timer.timer = startTimer;
		timer.callBackAction = action;
		if (owner == null)
			timer.owner = action.Target as MonoBehaviour;
		else timer.owner = owner;

		if (timer.owner != null)
		{
			timer.ownerName = timer.owner.GetType().ToString();
			DLog.Log(timer.ownerName + " has added a timer!", DLog.Level.UltraVerbose, owner, "TIMER (BP)");
		}
		else
		{
			timer.ownerName = action.Target.ToString();
			DLog.Log("Unknown (" + timer.ownerName + ") has added a timer!", DLog.Level.UltraVerbose, owner, "TIMER (BP)");
		}
					

		for (int i = 0; i < timers.Length;i++)
			if (timers[i].finished)
			{
				timers[i] = timer;
				return timer;
			}
				
		
		
		timers = timers.Add(timer);

		return timer;
	}
}
