using UnityEngine;
using System.Collections;

public class LayerExtensions
{

    public static void Activate(ref LayerMask layerMask, int layer)
    {
        layerMask |= layer;
    }

    public static void Activate(ref LayerMask layerMask, string layer)
    {
        Activate(ref layerMask, 1 << LayerMask.NameToLayer(layer));
    }

    public static void Deactivate(ref LayerMask layerMask, int layer)
    {
        layerMask &= ~layer;
    }

    public static void Deactivate(ref LayerMask layerMask, string layer)
    {
        Deactivate(ref layerMask, 1 << LayerMask.NameToLayer(layer));
    }

    public static void Toggle(ref LayerMask layerMask, int layer)
    {
        //layerMask ^= layer;
		layerMask ^= layer;
    }

    public static void Toggle(ref LayerMask layerMask, string layer)
    {
        Toggle(ref layerMask, 1 << LayerMask.NameToLayer(layer));
    }

    public static bool Includes(LayerMask layerMask, int layer)
    {
		int lay = (layerMask & (1 << layer));
        return lay > 0;
    }

    public static bool Includes(LayerMask layerMask, string layer)
    {
        return Includes(layerMask, 1 << LayerMask.NameToLayer(layer));
    }

    /*
    public static void LayerCullingToggle(this Camera cam, int layerMask, bool isOn)
    {
        bool included = LayerCullingIncludes(cam, layerMask);
        if (isOn && !included)
        {
            LayerCullingShow(cam, layerMask);
        }
        else if (!isOn && included)
        {
            LayerCullingHide(cam, layerMask);
        }
    }

    public static void LayerCullingToggle(this Camera cam, string layer, bool isOn)
    {
        LayerCullingToggle(cam, 1 << LayerMask.NameToLayer(layer), isOn);
    }
    */
}