﻿using UnityEngine;

public class SizeIncrease : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        var pb = other.GetComponent<PlayerBall>();
        if (pb == null)
            return;
        
        pb.SizeIncrease();
        Destroy(gameObject);
    }
}