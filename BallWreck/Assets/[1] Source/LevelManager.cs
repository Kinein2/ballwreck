﻿using UnityEngine;

public class LevelManager : SingletonMono<LevelManager>
{
    PlayerBall playerBall;
    
    public GameObject endScreen;
    [SerializeField] public GameObject perfectText, percentageText, progressBar;

    public float z0, z1;
    [SerializeField] FloatVariable progressPercentage;

    void Awake()
    {
        playerBall = FindObjectOfType<PlayerBall>();
        
        
    }

    void Update()
    {
        float z = playerBall.transform.position.z - z0;
        z /= (z1 - z0);
        progressPercentage.Value = z;
    }
    
    
    public void SetToLoose()
    {
        playerBall.enabled = false;
        endScreen.SetActive(true);
        
    }
    
}