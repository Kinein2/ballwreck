﻿using UnityEngine;

public class BrickScript : MonoBehaviour
{
    
    public float rayShiftX, brickHeight;
    
    public LayerMask brickMask;

    [HideInInspector]
    public bool awaken = false;
    
    public int depth;
    
    public BrickScript fBrick, bBrick;
    public BrickScript fBrickBelow, bBrickBelow;

    public Rigidbody rb;

    bool fallen;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }


    public void SettleConnections()
    {
        fBrick = null;
        bBrick = null;
        
        var origin = transform.position - rayShiftX*transform.TransformDirection(Vector3.right);
        RaycastHit[] cols;
        cols = Physics.RaycastAll(origin, Vector3.up, brickHeight, brickMask);
        Debug.DrawLine(origin, origin + transform.TransformDirection(Vector3.up)*brickHeight, cols.Length > 1 ? Color.red : Color.grey, 0.1f);
        
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].collider.gameObject == gameObject) continue;

            var bs = cols[i].collider.gameObject.GetComponent<BrickScript>();
            bBrick = bs;
            
            break;
        }

        origin = transform.position + rayShiftX*transform.TransformDirection(Vector3.right);
        cols = Physics.RaycastAll(origin, Vector3.up, brickHeight, brickMask);
        Debug.DrawLine(origin, origin + transform.TransformDirection(Vector3.up)*brickHeight, cols.Length > 1 ? Color.red : Color.grey, 0.1f);
        
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].collider.gameObject == gameObject) continue;

            var bs = cols[i].collider.gameObject.GetComponent<BrickScript>();
            fBrick = bs;
            
            break;
        }

    }

    void Update()
    {
        return;
        if (fBrickBelow)
            Debug.DrawLine(transform.position, fBrickBelow.transform.position, Color.cyan);
        if (bBrickBelow)
            Debug.DrawLine(transform.position, bBrickBelow.transform.position, Color.yellow);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Ground"))
        {
            var bs = other.gameObject.GetComponent<BrickScript>();
            if (bs == null)
                return;
            
            if (bs.fallen == false)
                return;
        }
            

        if (fallen == true)
            return;

        fallen = true;
        //DLog.Log("I'm fallen! " + gameObject.name);
        BuildingsManager.Instance.bricksFallen++;

    }
}