﻿using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

public class PlayerBall : MonoBehaviour
{


    public float distance;
    public float speedForward, sensitivity;

    public float sizeVolumeIncrease, sizeMassIncrease, scaleTime, finishAccelerate, finishAccelerateTime, finishCameraFollowTime;

    public IntegerReference size;
    public int setStartSize = 0;
    
    Camera _camera;
    Rigidbody _rb;
    
    Vector2 oldviewportV;
    float finishAccelerateTimer;
    

    bool pressed;

    bool finished;
    [SerializeField] float kAcc;

    public void SizeIncrease(bool instant = false)
    {
        size.Value++;


        float R = transform.localScale.x/2.0f;
        float V = 4.0f / 3.0f * Mathf.PI * R * R * R;
        V += sizeVolumeIncrease;
        R = Mathf.Pow(V * (3.0f / 4.0f) / Mathf.PI, 1 / 3.0f);

        if (instant == false)
            transform.DOScale(2 * R * Vector3.one, scaleTime);
        else transform.localScale = 2 * R * Vector3.one;
        
        
        //Vector3 tarSz = transform.localScale.IncLength(sizeScaleIncrease);
        //transform.DOScale(tarSz, scaleTime);
        _rb.mass *= sizeMassIncrease;
    }

    public void GoFinish()
    {
        finished = true;
        _rb.constraints = RigidbodyConstraints.None;
        _rb.velocity = speedForward * Vector3.forward;
        LevelManager.Instance.progressBar.SetActive(false);
        
        

    }
    
    void Start()
    {
        _camera = Camera.main;
        _rb = GetComponent<Rigidbody>();
        
        for (int i = 0; i < setStartSize; i++)
            SizeIncrease(true);
    }

    
    Vector2 GetWorldPos(Vector2 pixelPos)
    {
        var p = _camera.ScreenToWorldPoint((Vector3)pixelPos + distance * Vector3.forward);
        //p.y *= 1.0f / _camera.aspect;
        Debug.DrawLine(_camera.transform.position, p, Color.green, 0.1f);
        return p;
    }

    bool wordsInitted = false;

    void Update()
    {
        float delta = Time.deltaTime;
        if (finished)
        {
            if (finishAccelerateTimer < finishAccelerateTime)
            {
                //_rb.velocity += finishAccelerate * Vector3.forward * delta * (0.5f + 0.5f * 200.0f / _rb.mass / _rb.mass);
                float f = _rb.mass / 250.0f;
                f = 1 - f;
                if (f < 0) f = 0;
                if (f > 1.0f) f = 1.0f;
                _rb.velocity += finishAccelerate * Vector3.forward * delta * (kAcc + f*(1-kAcc));

            }
            
            if (finishAccelerateTimer < finishCameraFollowTime)
                _camera.transform.position += Vector3.forward * speedForward * delta;

            if (finishAccelerateTimer > finishCameraFollowTime * 0.7f && wordsInitted == false)
            {
                LevelManager.Instance.perfectText.SetActive(true);
                LevelManager.Instance.percentageText.SetActive(true);
                
                wordsInitted = true;
            }
            
            finishAccelerateTimer += delta;
            return;
        }
        
        _camera.transform.position += Vector3.forward * speedForward * delta;
        transform.position += Vector3.forward * speedForward * delta;

        var ts = InputHelper.GetTouches();
        Vector3 dv = Vector3.zero;

        if (ts.Count > 0)
        {
            var viewportV = GetWorldPos(ts[0].position);

            if (pressed)
            {
                dv.x = viewportV.x - oldviewportV.x;
            }

            oldviewportV = viewportV;
            pressed = true;
        }
        else pressed = false;
        
         
        transform.position += sensitivity * dv.x * Vector3.right;

    }



}