﻿using UnityEngine;

public class Shooter : MonoBehaviour
{
    public float ballSpeed;

    public GameObject ballPrefab;
    
    
    public Vector2Variable cursord;

    public SimpleTrigger clickDown;
    public SimpleTrigger clickUp;

    void Awake()
    {
        clickDown.AddCallback(this, () =>
        {
            var ball = SingletonPools.Create(ballPrefab, transform.position, Quaternion.identity);
            var rb = ball.GetComponent<Rigidbody>();

            var dir = (Vector3)((Vector2z) cursord.Value) - transform.position;
            rb.velocity = dir.normalized * ballSpeed;

        });
    }


    void Update()
    {
        
    }

}