﻿using UnityEngine;

public class DeadlyTrap : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerBall>() == null)
            return;

        LevelManager.Instance.SetToLoose();
    }
}