﻿using UnityEngine;

public class BallScript : MonoBehaviour
{
    public float rayLength, potentVelocity;
    public LayerMask brickLayer;


    Rigidbody _rb;

    
    void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }
    
    
    void Update()
    {

        if (_rb.velocity.magnitude > potentVelocity)
        {
            RaycastHit hitInfo;
            var hits = Physics.SphereCastAll(transform.position, transform.localScale.x, _rb.velocity.normalized, rayLength, brickLayer);
            Debug.DrawLine(transform.position, _rb.velocity.normalized*rayLength);
            for (int i = 0; i < hits.Length; i++)
            {
                var bs = hits[i].collider.GetComponent<BrickScript>();

                if (bs != null)
                    BuildingsManager.Instance.DoHit(this, bs);
            }    
        }
        

    }
    
    
    void OnCollisionEnter(Collision collision)
    {
        
        var bs = collision.gameObject.GetComponent<BrickScript>();

        //if (bs != null)
        //    _rb.velocity *= (0.95f);

        //BuildingsManager.Instance.DoHit(this, bs, collision);


    }
}