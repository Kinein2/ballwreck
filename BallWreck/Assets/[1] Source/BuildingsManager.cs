﻿using UnityEngine;

public class BuildingsManager : SingletonMono<BuildingsManager>
{
    public BrickScript[] bricks;
    public BrickScript[] floorBricks;
    [SerializeField] public int bricksFallen;
    public int bricksAmount;

    public FloatVariable destructionPercentage;

    public string[] perfectPhrases;
    public float[] percentageForPerfect;
    [SerializeField] StringReference perfectString;


    void Awake()
    {
            
        
        bricks = FindObjectsOfType<BrickScript>();
        bricksAmount = bricks.Length;

        
        for (int i = 0; i < bricks.Length; i++)
        {
            bricks[i].SettleConnections();
        }

        for (int i = 0; i < bricks.Length; i++)
        {
            if (bricks[i].fBrick)
                bricks[i].fBrick.bBrickBelow = bricks[i];
            if (bricks[i].bBrick)
                bricks[i].bBrick.fBrickBelow = bricks[i];
        }

        // ставим всем глубину и создаем массив себе днищевых
        for (int i = 0; i < bricks.Length; i++)
        {
            if (bricks[i].fBrickBelow == null && bricks[i].bBrickBelow == null)
            {
                bricks[i].depth = 0;
                floorBricks = floorBricks.Add(bricks[i]);
            }
            else
            {
                bricks[i].depth = FindDepths(bricks[i]);
            }
        }
        

    }


    void Update()
    {
        if (bricksAmount > 0)
        {
            destructionPercentage.Value = (float)bricksFallen / bricksAmount * 100.0f;

            float f = destructionPercentage.Value;
            int nPerfect = -1;

            for (int i = 0; i < percentageForPerfect.Length; i++)
            {
                if (f < percentageForPerfect[i])
                {
                    break;
                }
                
                nPerfect = i;
            }

            if (nPerfect == -1)
                perfectString.Value = "";
            else perfectString.Value = perfectPhrases[nPerfect]; 
        }
            
    }
    

    // @Volatile: допущение - если кирпич не имеет нижних значит он надежно стоит
    // @Volatile: допущение - не важно в какую сторону идти вниз
    int FindDepths(BrickScript brickTar)
    {
        var brick = brickTar;
        int n = 0;
        
        while (brick != null)
        {
            if (brick.bBrickBelow != null)
                brick = brick.bBrickBelow;
            else brick = brick.fBrickBelow;
            
            if (brick != null)
                n++;
        }

        return n;
    }


    public void DoHit(BallScript ballScript, BrickScript bs)
    {
        
        //Wake(bs);
        //bs.rb.AddForceAtPosition(-collision.impulse, collision.GetContact(0).point, ForceMode.Impulse);
        //bs.rb.AddForce(-collision.impulse, ForceMode.Impulse);

        WakeAllUpper(bs);
    }


    void WakeAllUpper(BrickScript bs)
    {
        if (bs.awaken) return;
        
        Wake(bs);

        if (Random.value > 0.05f)
        if (bs.fBrick != null)
            WakeAllUpper(bs.fBrick);
        
        if (Random.value > 0.05f)
        if (bs.bBrick != null)
            WakeAllUpper(bs.bBrick);
    }
    
    void Wake(BrickScript bs)
    {
        //if (bs.awaken) return;
        
        bs.awaken = true;
        
        bs.rb.constraints = RigidbodyConstraints.None;
        bs.rb.isKinematic = false;
    }
}