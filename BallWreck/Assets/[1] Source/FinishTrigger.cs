﻿using UnityEngine;

public class FinishTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        var pb = other.GetComponent<PlayerBall>();
        if (pb == null)
            return;

        pb.GoFinish();
        Destroy(gameObject);
    }
}